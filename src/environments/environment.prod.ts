export const environment = {
	production: true,
	apiUri: 'https://us-central1-shopping-699f9.cloudfunctions.net/api',
	appVersion: require('../../package.json').version,
	firebaseConfig: {
		apiKey: 'AIzaSyDIvgapoHcOWebQgH_wpNGSwoLzWVRif9A',
		authDomain: 'shopping-699f9.firebaseapp.com',
		databaseURL: 'https://shopping-699f9.firebaseio.com',
		messagingSenderId: '548742150148',
		projectId: 'shopping-699f9',
		rules: {
			'.read': 'auth != null',
			'.write': 'auth != null',
		},
		storageBucket: 'shopping-699f9.appspot.com',
		appId: '1:548742150148:web:fa181c91e5587ed8',
	},
	stripe: {
		publishable: 'pk_live_QjRUGrhxS0TbN1fah2DcFXOU',
	},
	messagingPublicVapidKey: 'BO5b3IEiGO9LwVSVll2JqSZEWpa3Y4sD7niJ9ZeKCc9ma50fQ9sE2cVr5XgqqrzjNySgYLJN6VBqIcGJYdCya0E',
};
