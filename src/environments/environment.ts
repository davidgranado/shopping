// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
	production: false,
	apiUri: 'https://us-central1-shopping-699f9.cloudfunctions.net/api',
	appVersion: require('../../package.json').version,
	firebaseConfig: {
		apiKey: 'AIzaSyDIvgapoHcOWebQgH_wpNGSwoLzWVRif9A',
		authDomain: 'shopping-699f9.firebaseapp.com',
		databaseURL: 'https://shopping-699f9.firebaseio.com',
		messagingSenderId: '548742150148',
		projectId: 'shopping-699f9',
		storageBucket: 'shopping-699f9.appspot.com',
		appId: '1:548742150148:web:fa181c91e5587ed8',
	},
	stripe: {
		publishable: 'pk_test_oBvlyp1hpoM0ToCZSChrHdEV',
	},
	messagingPublicVapidKey: 'BO5b3IEiGO9LwVSVll2JqSZEWpa3Y4sD7niJ9ZeKCc9ma50fQ9sE2cVr5XgqqrzjNySgYLJN6VBqIcGJYdCya0E',
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
