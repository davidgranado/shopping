import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';
import { dynamicallyLoadScript } from './app/utils';

if (environment.production) {
	enableProdMode();
	setTimeout(() => {
		dynamicallyLoadScript('https://www.googletagmanager.com/gtag/js?id=UA-116286258-1');
	}, 7000);
}

platformBrowserDynamic().bootstrapModule(AppModule)
	.then(() => {
		// Temp solution for bug
		// https://github.com/angular/angular-cli/issues/13351
		if ('serviceWorker' in navigator && environment.production) {
			navigator.serviceWorker.register('/combined-sw.js');
		}
	})
	.catch(err => console.log(err));
