import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { IonicModule } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule, FirebaseOptionsToken } from '@angular/fire';
import { AngularFirePerformanceModule } from '@angular/fire/performance';
import { AngularFirestore, AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ToastService } from './services/toast.service';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { CommonComponentsModule } from './components/common-components.module';
import { IonicGestureConfig } from '../ionic-gesture-config';
import { InstallBannerService } from './services/install-banner.service';
import { CoreModule } from './core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { ItemEditPage } from './pages/item-edit/item-edit.page';
import { ShoppingListItemEditFormComponent } from './components/shopping-list-item-edit-form/shopping-list-item-edit-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ShoppingListService } from './services/shopping-list.service';
import { UserService } from './services/user.service';

@NgModule({
	declarations: [
		AppComponent,
		ItemEditPage,
		ShoppingListItemEditFormComponent,
	],
	entryComponents: [
		ItemEditPage,
		ShoppingListItemEditFormComponent,
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(),
		AngularFirestoreModule.enablePersistence(),
		AppRoutingModule,
		AngularFireAuthModule,
		AngularFireModule,
		AngularFirestoreModule,
		environment.production ? AngularFirePerformanceModule : [],
		AngularFireModule.initializeApp(environment.firebaseConfig),
		CoreModule,
		HttpClientModule,
		// Disabled and hacked around in main.ts
		ServiceWorkerModule.register('combined-sw.js', { enabled: environment.production }),
		CommonComponentsModule,
		FormsModule,
		ReactiveFormsModule,
	],
	providers: [
		AngularFirestore,
		// NotificationService,
		InstallBannerService,
		ToastService,
		UserService,
		ShoppingListService,
		UserService,
		{
			provide: FirebaseOptionsToken,
			useValue: environment.firebaseConfig,
		},
		// Value temporarily needed until the following bug is addressed
		// https://github.com/angular/angularfire2/issues/1993
		{
			provide: FirestoreSettingsToken,
			useValue: {},
		},
		// Needed as a workaround for IOS. See IonicGestureConfig notes
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: IonicGestureConfig,
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {
	constructor() { }

	// TODO Review this
	// async init() {
	// 	if (environment.production) {
	// 		const user = await this.userService.user.;

	// 		if(!user || user.isAnonymous) {
	// 			return;
	// 		}

	// 		this.db.doc(`last-update-users-viewed/${user.uid}`).set({
	// 			user: user.email,
	// 			id: user.uid,
	// 			date: new Date(),
	// 			version: environment.appVersion,
	// 		});
	// 	}
	// }
}
