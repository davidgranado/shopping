import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class InstallBannerService {
	// @ts-ignore
	private promptEvent: BeforeInstallPromptEvent | null = null;

	constructor() {
		// @ts-ignore
		addEventListener('beforeinstallprompt', (e: BeforeInstallPromptEvent) => {
			this.promptEvent = e;
			e.preventDefault();
		});
	}

	get hasInstallPrompt() {
		return !!this.promptEvent;
	}

	async install() {
		if(this.promptEvent) {
			await this.promptEvent.prompt();

			this.promptEvent = null;
		}
	}
}


// TEMP
// SEE https://stackoverflow.com/questions/51503754/typescript-type-beforeinstallpromptevent
/**
 * The BeforeInstallPromptEvent is fired at the Window.onbeforeinstallprompt handler
 * before a user is prompted to "install" a web site to a home screen on mobile.
 *
 * @deprecated Only supported on Chrome and Android Webview.
 */
interface BeforeInstallPromptEvent extends Event {

	/**
	 * Returns an array of DOMString items containing the platforms on which the event was dispatched.
	 * This is provided for user agents that want to present a choice of versions to the user such as,
	 * for example, "web" or "play" which would allow the user to chose between a web version or
	 * an Android version.
	 */
	readonly platforms: Array<string>;

	/**
	 * Returns a Promise that resolves to a DOMString containing either "accepted" or "dismissed".
	 */
	readonly userChoice: Promise<{
		outcome: 'accepted' | 'dismissed',
		platform: string;
	}>;

	/**
	 * Allows a developer to show the install prompt at a time of their own choosing.
	 * This method returns a Promise.
	 */
	prompt(): Promise<void>;

}
