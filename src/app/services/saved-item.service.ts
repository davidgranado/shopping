import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { environment } from '../../environments/environment';
import { tap, switchMap, shareReplay, map } from 'rxjs/operators';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';
import { SavedItem, ShoppingListItem } from '../interfaces';

const SAVED_ITEMS_DB = `${environment.production ? '' : ''}saved-items`;

@Injectable({
	providedIn: 'root',
})
export
class SavedItemService {
	// TODO refactor methods to consume this as an observable
	private itemCol = new BehaviorSubject<AngularFirestoreCollection<SavedItem>| null>(null);
	private items$: Observable<SavedItem[]>;
	private hasItems$: Observable<boolean>;
	private count$: Observable<number>;
	private privateCount$: Observable<number>;

	get items() {
		return this.items$;
	}

	get hasItems() {
		return this.hasItems$;
	}

	get count() {
		return this.count$;
	}

	get privateCount() {
		return this.privateCount$;
	}

	constructor(
		private db: AngularFirestore,
		private userService: UserService,
	) {

		const user = userService.user;

		user.pipe(
			tap(u =>
				this.itemCol.next(
					u ?
						db.collection<SavedItem>(
							SAVED_ITEMS_DB,
							ref => ref.where('ownerId', '==', u.uid),
						) :
						null,
				),
			),
		).subscribe();

		this.items$ = this.itemCol.pipe(
			switchMap((itemCol) => {
				if(itemCol) {
					return itemCol.valueChanges();
				} else {
					return of([]);
				}
			}),
			shareReplay(1),
		);

		this.count$ = this.items$.pipe(
			map(items => items.length),
		);

		this.privateCount$ = this.items$.pipe(
			map(
				items => items.filter(item => item.private).length,
			),
		);

		this.hasItems$ = this.count$.pipe(
			map(count => !!count),
		);
	}

	searchTerm(term: string) {
		return this.items.pipe(
			map(items => items.filter(i => i.name.toLowerCase().includes(term))),
		);
	}

	getItem(id: string) {
		return this.items$.pipe(
			map(
				items => items.find(
					item => item.id === id,
				) || null,
			),
			switchMap(
				i => {
					if(i) {
						return of(i);
					}

					const col = this.itemCol.getValue();

					if(!col) {
						return of(null);
					}

					return col.doc<SavedItem>(id).valueChanges()
						.pipe(
							map(x => x || null),
						);
				},
			),
		);
	}

	toSavedItemContent(item: ShoppingListItem) {
		const {
			id,
			parentId,
			quantity,
			selectedPriceId,
			...newItem } = item;
		return {
			...newItem,
			storePrices: newItem.storePrices ? [
				...newItem.storePrices,
			] : [],
		} as SavedItem;
	}

	async save(partial: Partial<SavedItem>, forceCreate = false) {
		if(!this.userService.isLoggedIn) {
			await this.userService.loginAnonymously();
		}

		const userDoc = await this.userService.userDocPromise;
		const itemCol = this.itemCol.getValue();

		if(!(userDoc && itemCol)) {
			return null;
		}

		const model = {
			ownerId: this.userService.userId,
			private: true,
			created: new Date(),
			...partial,
		} as SavedItem;

		if(!model.id) {
			model.id = this.createId();
			await itemCol.doc<SavedItem>(model.id).set(model);
		} else if(forceCreate) {
			await itemCol.doc<SavedItem>(model.id).set(model);
		} else {
			await itemCol.doc<SavedItem>(model.id).update(model);
		}

		return model;
	}

	async delete(id: string) {
		const itemCol = this.itemCol.getValue();
		itemCol && await itemCol.doc(id).delete();
	}

	async duplicate(item: SavedItem) {
		if(!(item.id)) {
			return;
		}

		return await this.save({
			...item,
			name: `Copy of ${item.name}`,
		});
	}

	createId() {
		return this.db.createId();
	}

	createSavedItem(): SavedItem {
		return {
			aisle: '',
			brand: '',
			name: '',
			notes: '',
			ownerId: this.userService.userId,
			price: 0,
			private: false,
		};
	}
}
