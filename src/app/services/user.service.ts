import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, of, from } from 'rxjs';
import { auth } from 'firebase/app';
import { shareReplay, switchMap, filter, take, map, tap } from 'rxjs/operators';

import { UserDoc } from '../core/user';
import { LoadingController, NavController } from '@ionic/angular';

@Injectable({
	providedIn: 'root',
})
export
class UserService {
	private userDoc$: Observable<UserDoc | null>;

	get userId() {
		return this.afAuth.auth.currentUser && this.afAuth.auth.currentUser.uid || '';
	}

	get isLoggedIn() {
		return !!this.afAuth.auth.currentUser;
	}

	get userDoc() {
		return this.userDoc$;
	}

	get user() {
		return this.afAuth.authState;
	}

	get userDocPromise() {
		return this.userDoc$.pipe(
			take(1),
		).toPromise();
	}

	get needsEmailVerification() {
		const currentUser = this
			.afAuth
			.auth
			.currentUser;

		return !!currentUser && !currentUser.emailVerified;
	}

	constructor(
		private afAuth: AngularFireAuth,
		private loadingCtrl: LoadingController,
		private navCtrl: NavController,
		private db: AngularFirestore,
	) {
		this.userDoc$ = afAuth.authState.pipe(
				switchMap(u =>
					u ?
						db.doc<UserDoc>(`users/${u.uid}`)
						.valueChanges().pipe(
							map(userDoc => userDoc || null),
						) :
						of(null),
				),
				shareReplay(1),
			);
	}

	async setPersistence() {
		await this.afAuth.auth.setPersistence(auth.Auth.Persistence.LOCAL);
	}

	async loginAnonymously() {
		const loader = await this.loadingCtrl.create({
			message: 'Running initial setup...',
		});
		loader.present();
		await this.setPersistence();
		await this.afAuth.auth.signInAnonymously();
		await this
			.userDoc$.pipe(
				filter(u => !!(u && u.username)),
				take(1),
			)
			.toPromise();
		loader.dismiss();
	}

	get idToken() {
		return this.afAuth.auth.currentUser && this.afAuth.auth.currentUser.getIdToken();
	}

	async register(email: string, password: string) {
		await this.setPersistence();

		if(this.afAuth.auth.currentUser) {
			const authCred = auth.EmailAuthProvider.credential(email, password);
			const { user } = await this.afAuth.auth.currentUser.linkWithCredential(authCred);
			// @ts-ignore
			this.db.doc(`users/${user.uid}`).update({
				email,
			});
		} else {
			await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
		}

		// @ts-ignore
		environment.production && await this.afAuth.auth.currentUser.sendEmailVerification();
	}

	sendPasswordResetEmail(email: string) {
		return this.afAuth.auth.sendPasswordResetEmail(email);
	}

	async login(email: string, password: string) {
		await this.setPersistence();
		await this.afAuth.auth.signInWithEmailAndPassword(email, password);
		await this.navCtrl.navigateRoot('/tabs/home');
	}

	async logout() {
		await this.afAuth.auth.signOut();
		await this.navCtrl.navigateRoot('/login');
	}

	async delete() {
		const { currentUser } = this.afAuth.auth;
		currentUser && await currentUser.delete();
	}

	async sendEmailVerification() {
		this.user.pipe(
			tap(u => u && u.sendEmailVerification()),
			take(1),
		).toPromise();
	}

	getUserIdToken(): Observable<string | null> {
		if(!this.afAuth.auth.currentUser) {
			return of(null);
		}

		return from(this.afAuth.auth.currentUser.getIdToken(true));
	}

	async updateUsername(newUsername: string) {
		const userDoc = await this.getUserDoc(newUsername);
		if(userDoc.exists) {
			return false;
		}

		await this.db.doc(`users/${this.userId}`).update({
			username: newUsername,
		});

		return true;
	}

	getUserDoc(username: string) {
		return this.db.doc(`usernames/${username.toLowerCase()}`).ref.get();
	}

	async getUser(username: string) {
		const doc = await this.getUserDoc(username);
		return doc.data();
	}
}
