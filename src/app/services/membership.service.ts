import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { UserService } from './user.service';
import { SubscriptionDoc, Plan } from '../interfaces';

const SUBSCRIPTION_TABLE = 'subscriptions';

@Injectable({
	providedIn: 'root',
})
export class MembershipService {
	private subscriptionDoc$: Observable<SubscriptionDoc | null>;
	private plan$: Observable<Plan>;
	private isBasic$: Observable<boolean>;
	private isPro$: Observable<boolean>;

	get plan() {
		return this.plan$;
	}

	get isBasic() {
		return this.isBasic$;
	}

	get isPro() {
		return this.isPro$;
	}

	get subscriptionDoc() {
		return this.subscriptionDoc$;
	}

	constructor(
		private db: AngularFirestore,
		private userService: UserService,
	) {
		this.subscriptionDoc$ = this.userService.user.pipe(
			switchMap(
				(user) => {
					if(!user) {
						return of(null);
					}

					return this
						.db
						.doc<SubscriptionDoc>(`${SUBSCRIPTION_TABLE}/${user.uid}`)
						.valueChanges()
						.pipe(
							map(sub => sub || null),
						);
			}),
		);
		this.plan$ = this.subscriptionDoc$.pipe(
			map(sub => sub ? sub.plan : 'basic'),
		);
		this.isBasic$ = this.plan$.pipe(
			map(plan => plan === 'basic'),
		);
		this.isPro$ = this.plan$.pipe(
			map(plan => plan !== 'basic'),
		);
	}
}
