import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { environment } from '../../environments/environment';
import { map, shareReplay, switchMap, take, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { UserService } from './user.service';
import { uuid } from '../utils';
import { ShoppingListItem, SavedList, ShoppingList } from '../interfaces';

const SAVED_LISTS_DB = `${environment.production ? '' : ''}saved-lists`;

@Injectable({
	providedIn: 'root',
})
export
class SavedListService {
	private listCol: AngularFirestoreCollection<SavedList> | null = null;
	private lists$:  Observable<SavedList[]>;
	private hasLists$: Observable<boolean>;
	private privateCount$: Observable<number>;

	get lists() {
		return this.lists$;
	}

	get hasLists() {
		return this.hasLists$;
	}

	get privateCount() {
		return this.privateCount$;
	}

	constructor(
		private db: AngularFirestore,
		private userService: UserService,
	) {
		const userDoc = userService.userDoc;

		userDoc.pipe(
			tap(u => {
				this.listCol = u ?
						db.collection<SavedList>(
							SAVED_LISTS_DB,
							ref => ref.where('ownerId', '==', u.id),
						) :
						null;
			}),
		).subscribe();

		this.lists$ = userDoc.pipe(
				switchMap(u => {
					if( u && this.listCol) {
						return this.listCol.valueChanges();
					} else {
						return of([]);
					}
				}),
				shareReplay(1),
			);

		this.hasLists$ = this.lists$.pipe(
			map(l => !!l.length),
		);

		this.privateCount$ = this.lists$.pipe(
			map(
				lists => lists.filter(list => list.private).length,
			),
		);
	}

	async save(partial: Partial<SavedList>) {
		if(!this.userService.isLoggedIn) {
			await this.userService.loginAnonymously();
		}

		const userDoc = await this.userService.userDocPromise;

		if(!userDoc) {
			return;
		}

		const model = {
			ownerUsername: userDoc.username,
			ownerId: await userDoc.id,
			created: new Date(),
			items: [],
			itemOrder: [],
			...partial,
		} as SavedList;

		if(model.id) {
			this.listCol && await this.listCol.doc<SavedList>(model.id).update(model);
		} else {
			model.id = this.db.createId();
			this.listCol && await this.listCol.doc<SavedList>(model.id).set(model);
		}

		return model;
	}

	async saveItem(model: ShoppingListItem, listId: string) {
		model.price = +model.price;
		model.quantity = +model.quantity;

		const list = await this.getListPromise(listId);

		if(!list) {
			return;
		}

		const item = list.items.find(i => i.id === model.id);

		if(item) {
			Object
				.keys(model)
				// @ts-ignore
				.forEach(key => item[key] = model[key]);
		} else {
			model.id = uuid();
			list.items.splice(0, 0, model);
			list.itemOrder.push(model.id);
		}

		await this.save(list);
	}

	async duplicate(list: SavedList) {
		const ownerId = this.userService.userId;

		if(!(list.id && ownerId)) {
			return;
		}

		return await this.save({
			// duplicate items array without maintaining references
			name: `Copy of ${list.name}`,
			ownerId,
			items: list.items.map(i => ({...i})),
			itemOrder: list.itemOrder.slice(),
		});
	}

	async delete(id: string) {
		this.listCol && await this.listCol.doc(id).delete();
	}

	async getListPromise(id: string) {
		return (
			this.listCol &&
			await this.listCol
				.doc<SavedList>(id)
				.valueChanges()
				.pipe(
					map(l => l || null),
					take(1),
				)
				.toPromise()
		);
	}

	getList(id: string) {
		return this.lists$.pipe(
			map(
				lists => lists.find(
					list => list.id === id,
				) || null,
			),
		);
	}

	getLists(ids: string[]) {
		return this.lists$.pipe(
			map(lists => lists.filter(
				l => ids.includes(l.id || ''),
			)),
		);
	}

	getListsPromise(ids: string[]) {
		return this.getLists(ids).pipe(
			take(1),
		).toPromise();
	}

	getItem(itemId: string, listId: string) {
		return this.getList(listId).pipe(
			map(
				list => list && list.items.find(
					item => item.id === itemId,
				) || null,
			),
		);
	}

	// Keeping distinct for now in case I end up putting items into collectionn
	async getItems(id: string): Promise<ShoppingListItem[]> {
		const list = await this.getListPromise(id);

		if(!(list && list.items)) {
			return [];
		}

		return list
			.itemOrder
			.map(
				io => {
					const item = list
						.items
						.find(i => io === i.id);
					return {...item} as ShoppingListItem;
				},
			);
	}

	canEdit(list: SavedList) {
		// If it's a new list
		if(!list.ownerId) {
			return true;
		}

		if(!(this.userService.isLoggedIn)) {
			return false;
		}

		// If user is the owner
		const userId = this.userService.userId;

		return !!userId && userId === list.ownerId;
	}

	async createFromShoppingList(shoppingList: ShoppingList) {
		await this.save({
			name: shoppingList.name,
			items: shoppingList.items.map(item => ({...item})),
			itemOrder: shoppingList.itemOrder.slice(),
		});
	}

	async deleteItem(id: string, listId: string) {
		const list = await this.getListPromise(listId);

		if(!list) {
			return;
		}

		list.items = list
			.items
			.filter(i => i.id !== id)
			.map(i => ({...i}));

		list.itemOrder = list
			.itemOrder
			.filter(itemId => itemId !== id);

		this.save(list);
	}

	createList(): SavedList {
		return {
			ownerUsername: '',
			ownerId: '',
			name: '',
			notes: '',
			items: [],
			itemOrder: [],
		};
	}

	createItem(): ShoppingListItem {
		return {
			aisle: '',
			brand: '',
			name: '',
			notes: '',
			price: 0,
			quantity: 1,
		};
	}
}
