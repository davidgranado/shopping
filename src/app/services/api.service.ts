import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

const API_URI = environment.apiUri;

interface Payload {
	[key: string]: any;
}

export
interface PaymentParams {
	email?: string;
	plan: string; // TODO Update all this to static values and dynamically handle
	source: string;
}

@Injectable({
	providedIn: 'root',
})
export class ApiService {
	constructor(
		private http: HttpClient,
	) { }

	payment(paymentParams: PaymentParams) {
		return this.http.post<any>(`${API_URI}/membership/pro`, paymentParams).toPromise();
	}

	unsubscribe() {
		return this.http.post<any>(`${API_URI}/membership/unsubscribe`, {}).toPromise();
	}

	feedback(feedback: string) {
		return this.http.post<any>(`${API_URI}/feedback`, {feedback}).toPromise();
	}

	fooGet() {
		return this.http.get<any>(`${API_URI}/foo`);
	}

	fooPost(payload: Payload) {
		return this.http.post<any>(`${API_URI}/foo`, payload);
	}
}
