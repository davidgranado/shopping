import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from './user.service';
import { map, tap, take } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class LoggedInWithAccountGuardService implements CanActivate {
	constructor(private userService: UserService, private router: Router) {}

	canActivate() {
		return this.userService.user.pipe(
			map(u => !!u && !u.isAnonymous),
			tap(loggedInWithAccount =>
				loggedInWithAccount && this.router.navigate([ '/tabs/home' ]),
			),
			take(1),
		);
	}
}
