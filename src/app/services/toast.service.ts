import { ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export
class ToastService {
	constructor(private toastCtrl: ToastController) {}

	async show(message: string, duration: number = 3000) {
		const toast = await this.toastCtrl
			.create({
				duration,
				message,
			});
		await toast.present();
	}
}
