import { Injectable } from '@angular/core';
import { ApiService, PaymentParams } from './api.service';
import { environment } from '../../environments/environment';
import { dynamicallyLoadScript } from '../utils';
import { BehaviorSubject } from 'rxjs';
import { ProPlan, ProPlanCost } from '../interfaces';

const stripeConfig = environment.stripe;

@Injectable({
	providedIn: 'root',
})
export class PaymentService {
	public stripe: any;
	private loaded$ = new BehaviorSubject(false);

	constructor(
		private apiService: ApiService,
	) {
		dynamicallyLoadScript('https://js.stripe.com/v3/', () =>
			dynamicallyLoadScript('https://checkout.stripe.com/checkout.js', () => {
				this.stripe = (window as any).Stripe(stripeConfig.publishable);
				this.loaded$.next(true);
			}),
		);
	}

	get loaded() {
		return this.loaded$;
	}

	get elementsInstance() {
		return this.stripe.elements();
	}

	async getPaymentRequest(plan: ProPlan) {
		return this.stripe.paymentRequest({
			country: 'US',
			currency: 'usd',
			total: {
				label: `ShopLystr Pro - ${plan === 'pro-annual' ? 'Annual' : 'Monthly'}`,
				amount: ProPlanCost[plan],
			},
		});
	}

	payment(data: PaymentParams) {
		return this.apiService.payment(data);
	}

	unsubscribe() {
		return this.apiService.unsubscribe();
	}

	async getSource(card: any) {
		const {source} = await this.stripe.createSource(card);
		return source;
	}
}
