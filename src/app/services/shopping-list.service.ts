import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { environment } from '../../environments/environment';
import { map, shareReplay, switchMap, tap, take } from 'rxjs/operators';
import { combineLatest, Observable, of } from 'rxjs';
import { UserService } from './user.service';
import { ShoppingListItem, ShoppingList, EditableBy } from '../interfaces';
import { uuid, flat } from '../utils';
import { SavedListService } from './saved-list.service';

const SHOPPING_LISTS_DB = `${environment.production ? '' : ''}shopping-lists`;

@Injectable({
	providedIn: 'root',
})
export
class ShoppingListService {
	private ownedCol: AngularFirestoreCollection<ShoppingList> | null = null;
	private sharedCol: AngularFirestoreCollection<ShoppingList> | null = null;
	private ownedLists$: Observable<ShoppingList[]>;
	private sharedLists$: Observable<ShoppingList[]>;
	private lists$:  Observable<ShoppingList[]>;
	private hasLists$: Observable<boolean>;

	get lists() {
		return this.lists$;
	}

	get hasLists() {
		return this.hasLists$;
	}

	constructor(
		private db: AngularFirestore,
		private userService: UserService,
		private savedListService: SavedListService,
	) {
		const userDoc = userService.userDoc;

		userDoc.pipe(
			tap(u => {
				if(u) {
					this.ownedCol =
						db.collection<ShoppingList>(
							SHOPPING_LISTS_DB,
							ref => ref.where('ownerId', '==', u.id),
						);
					this.sharedCol = db.collection<ShoppingList>(
							SHOPPING_LISTS_DB,
							ref => ref.where(`shared.${u.id}.username`, '==', u.username),
						);
				} else {
					this.ownedCol = null;
					this.sharedCol = null;
				}
			}),
		).subscribe();

		this.ownedLists$ = userDoc.pipe(
				switchMap(u => {
					if(u && this.ownedCol) {
						return this.ownedCol.valueChanges();
					} else {
						return of([]);
					}
				}),
				shareReplay(1),
			);

		this.sharedLists$ = userDoc.pipe(
				switchMap(u => {
					if(u && this.sharedCol) {
						return this.sharedCol.valueChanges();
					} else {
						return of([]);
					}
				}),
				shareReplay(1),
			);

		this.lists$ = combineLatest([
			this.ownedLists$,
			this.sharedLists$,
		]).pipe(
			map(([
				owned,
				shared,
			]) => owned.concat(shared)),
			shareReplay(1),
		);

		this.hasLists$ = this.lists$.pipe(
			map(l => !!l.length),
		);
	}

	async save(partial: Partial<ShoppingList>) {
		if(!this.userService.isLoggedIn) {
			await this.userService.loginAnonymously();
		}

		const userDoc = await this.userService.userDocPromise;

		if(!userDoc) {
			return;
		}

		const model = {
			ownerUsername: userDoc.username,
			ownerId: await userDoc.id,
			created: new Date(),
			shared: {},
			items: [],
			itemOrder: [],
			finishedItems: {},
			editableBy: EditableBy.SpecifiedUsers,
			...partial,
		} as ShoppingList;

		if(model.id) {
			this.db.doc(`${SHOPPING_LISTS_DB}/${model.id}`).update(model);
		} else {
			model.id = this.db.createId();
			this.db.doc(`${SHOPPING_LISTS_DB}/${model.id}`).set(model);
		}

		return model;
	}

	async saveItem(model: ShoppingListItem, listId: string) {
		model.price = +model.price;
		model.quantity = +model.quantity;

		const list = await this.getListPromise(listId);

		if(!list) {
			return;
		}

		const item = list.items.find(i => i.id === model.id);

		if(item) {
			Object
				.keys(model)
				// @ts-ignore
				.forEach(key => item[key] = model[key]);
		} else {
			model.id = uuid();
			list.items.push(model);
			list.itemOrder.unshift(model.id);
		}

		await this.save(list);
	}

	async getListPromise(id: string) {
		return this.getList(id)
			.pipe(
				take(1),
			)
			.toPromise();
	}

	getList(id: string) {
		return this.db
			.doc<ShoppingList>(`${SHOPPING_LISTS_DB}/${id}`)
			.valueChanges()
			.pipe(
				map(list => list || null),
			);
	}

	getItem(itemId: string, listId: string) {
		return this.getList(listId).pipe(
			map(
				list => list && list.items.find(
					item => item.id === itemId,
				) || null,
			),
		);
	}

	async duplicate(list: ShoppingList) {
		const ownerId = this.userService.userId;

		if(!(list.id && ownerId)) {
			return;
		}

		return await this.save({
			// duplicate items array without maintaining references
			name: `Copy of ${list.name}`,
			ownerId,
			shared: {},
			items: list.items.map(i => ({...i})),
			itemOrder: list.itemOrder.slice(),
		});
	}

	delete(id: string) {
		return this.db.doc(`${SHOPPING_LISTS_DB}/${id}`).delete();
	}

	async deleteItem(id: string, listId: string) {
		const list = await this.getListPromise(listId);

		if(!list) {
			return;
		}

		list.items = list
			.items
			.filter(i => i.id !== id)
			.map(i => ({...i}));

		list.itemOrder = list
			.itemOrder
			.filter(itemId => itemId !== id);

		this.save(list);
	}

	async saveNewList(name: string, savedListIds: string[]) {
		const items = flat(
			await Promise.all(
				savedListIds.map(
					(id) => this.savedListService.getItems(id)),
				),
			)
			.map(item => {
				item.id = uuid();
				return item;
			})
			.sort((a, b) => {
				return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
			});

		const dedupedItems: ShoppingListItem[] = [];

		items.forEach(item => {
			const existingItem = dedupedItems.find(i =>
				i.name.toLocaleLowerCase() === item.name.toLocaleLowerCase(),
			);

			if(existingItem) {
				existingItem.quantity = +item.quantity + +existingItem.quantity;
			} else {
				dedupedItems.push(item);
			}
		});

		const notes = (await this.savedListService.getListsPromise(savedListIds))
			.map(i =>
				i.notes && `"${i.name}" Notes:\n\n${i.notes}` || '',
			)
			.join('\n\n');

		this.save({
			notes,
			name: name,
			items,
			itemOrder: items.map(item => item.id as string),
		});
	}

	createItem(): ShoppingListItem {
		return {
			aisle: '',
			brand: '',
			name: '',
			notes: '',
			price: 0,
			quantity: 1,
			selectedPriceId: '',
			storePrices: [],
		};
	}
}
