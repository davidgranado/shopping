import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay, switchMap } from 'rxjs/operators';

export
class DelayedPreloadStrategy implements PreloadingStrategy {
	preload(route: Route, load: Function): Observable<any> {
		return of(null).pipe(
			delay(15000),
			switchMap(() => load()),
		);
	}
}
