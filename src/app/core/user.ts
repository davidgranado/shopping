import { firestore } from 'firebase';

export
interface UserDoc {
	id: string;
	created: firestore.Timestamp;
	email: string;
	username: string;
}
