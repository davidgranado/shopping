import { Injectable, Injector } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
} from '@angular/common/http';

import { switchMap } from 'rxjs/operators';

import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

	auth: UserService | null = null;

	constructor(private inj: Injector) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		if (request.url.indexOf('oauthCallback') > -1 ) {
			return next.handle(request);
		}

		this.auth = this.inj.get<UserService>(UserService);

		return this.auth.getUserIdToken().pipe(
			switchMap(token => {
				request = request.clone({
					setHeaders: {
						Authorization: `Bearer ${token}`,
					},
				});

				return next.handle(request);
			}),
		);
	}
}
