import { FormControl } from '@angular/forms';
import { ShoppingListItem, SavedItem, StorePrice } from './interfaces';

export
function getPrice(item: ShoppingListItem | SavedItem, selectedPriceId = ''): StorePrice {
	if(!selectedPriceId) {
		selectedPriceId = (item as ShoppingListItem).selectedPriceId || '';
	}

	const storePrice = !!selectedPriceId &&
		!!item.storePrices &&
		item.storePrices.find(
			i => i.id === selectedPriceId,
		) || false;

	return storePrice || {
			id: '',
			storeName: 'General',
			price: item.price,
		};
}

export
function uuid() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
		// tslint:disable-next-line
		const r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	});
}

export
function move(array: any[], from: number, to: number) {
	array
		.splice(
			to,
			0,
			array
				.splice(
					from,
					1,
				)[0],
		);
}

type GetIdentifier<T> = (value: T) => any;
const defaultGetIdentifier: GetIdentifier<any> = (val) => val;

export
function dedupe<T>(
	collection: T[],
	getIdentifier: GetIdentifier<T> = defaultGetIdentifier,
) {
	const identifierState: { [identifier: string]: boolean } = {};

	return collection.filter((value) => {
		const identifier = String(getIdentifier(value));

		if (identifierState[identifier]) {
			return false;
		}

		identifierState[identifier] = true;

		return true;
	});
}

declare global {
	const gtag: (config: string, id: string, data: any) => void;
	const datalayer: any[];
}

export
function recordPageView(title: string, path: string) {
	if(!gtag) {
		return;
	}

	gtag('config', 'UA-116286258-1', {
		'page_title' : title,
		'page_path': path,
	});
}

export
function dynamicallyLoadScript(url: string, onLoad?: () => any) {
	const script = document.createElement('script');
	script.src = url;
	script.async = true;

	if(onLoad) {
		script.onload = onLoad;
	}

	document.body && document.body.appendChild(script);
}

export
function flat<T>(arr: T[][]) {
	return ([] as T[]).concat.apply([], arr);
}

export
function pick<T extends object, U extends keyof T>(obj: T, paths: Array<U>): Partial<T> {
	return paths.reduce((o, k) => {
		o[k] = obj[k];
		return o;
	}, Object.create(null));
}

export
function defer(fn: (...args: any[]) => void) {
	setTimeout(fn, 0);
}

export
function minTrimmedLengthValidator(minLength: number) {
	return (control: FormControl) => {
		return (control.value || '').trim().length >= minLength ?
			null :
			{ 'whitespace': true };
	};
}
