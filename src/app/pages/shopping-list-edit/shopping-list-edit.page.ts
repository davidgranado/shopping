import { Component } from '@angular/core';
import { ToastService } from '../../services/toast.service';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { recordPageView } from '../../utils';
import { UserShareModalPage } from '../user-share-modal/user-share-modal.page';
import { Observable } from 'rxjs';
import { ItemEditPage } from '../item-edit/item-edit.page';
import { ShoppingListService } from '../../services/shopping-list.service';
import { map, tap } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { ShoppingList, EditableBy } from '../../interfaces';

@Component({
	selector: 'app-shopping-list-edit',
	styleUrls: ['./shopping-list-edit.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-back-button defaultHref="/tabs/home"></ion-back-button>
				</ion-buttons>
				<ng-container *ngIf="model$ | async as model">
					<ion-title>{{model.name}}</ion-title>
					<ion-buttons slot="end">
						<ion-button (click)="editSharing(model.id)" *ngIf="canEdit(model)">
							Share<ion-icon slot="end" name="share-social-outline"></ion-icon>
						</ion-button>
					</ion-buttons>
				</ng-container>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding" *ngIf="doesNotExist">
			<p>Sorry. But that list does not seem to exist any more or was removed by the creator.</p>
			<p>Try creating a <a [routerLink]="['/tabs/home']" routerDirection="back">new shopping list</a>!</p>
		</ion-content>

		<ng-container *ngIf="(model$ | async) as model">
			<ion-content *ngIf="!doesNotExist && !loading">
				<ion-grid fixed class="full-height-grid ion-no-padding">
					<ion-row>
						<ion-col class="ion-no-padding">
							<app-shopping-list-edit-form
								[shopping-list]="model"
								[canEdit]="canEdit(model)"
								(save)="save($event)"
							></app-shopping-list-edit-form>
						</ion-col>
					</ion-row>
				</ion-grid>
				<ion-fab *ngIf="canEdit" vertical="bottom" horizontal="end" slot="fixed">
					<ion-fab-button
						mini
						(click)="routeToCreateItem(model.id)"
						color="secondary"
					>
						<ion-icon name="add"></ion-icon>
					</ion-fab-button>
				</ion-fab>
			</ion-content>
		</ng-container>
	`,
})
export class ShoppingListEditPage {
	public model$: Observable<ShoppingList | null>;
	public doesNotExist = false;
	public loading = false;

	get userId() {
		return this.userService.userId;
	}

	constructor(
		private toastService: ToastService,
		private shoppingListService: ShoppingListService,
		private loadingController: LoadingController,
		private modalCtrl: ModalController,
		private userService: UserService,
		private navCtrl: NavController,
		route: ActivatedRoute,
	) {
		recordPageView('Shopping List Edit', '/shopping-lists/edit');

		this.model$ = this.shoppingListService.getList(route.snapshot.params.id);

		route.paramMap.pipe(
			tap(params => {
				params.has('itemType') ?
					this.openCreateItem(route.paramMap) :
					this.closeCreateItem();
			}),
		).subscribe();
	}

	async save(model: ShoppingList) {
		const loader = await this.loadingController.create({});
		await loader.present();
		await this.shoppingListService.save(model);
		await loader.dismiss();
		this.toastService.show(`"${model.name}" saved`);
	}

	// TODO rework the below in favor of observables
	canEdit(model: ShoppingList) {
		if(!(model && model.shared)) {
			return false;
		}

		return (
			model.ownerId === this.userId ||
			model.editableBy === EditableBy.Anyone ||
			(
				model.editableBy === EditableBy.SpecifiedUsers &&
				!!model.shared[this.userId]
			)
		);
	}

	async routeToCreateItem(listId: string) {
		this.navCtrl.navigateForward(`/shopping-list/${listId}/item/shop/`, {
			animated: false,
		});
	}

	async openCreateItem(params: Observable<ParamMap>) {
		const oldModal = await this.modalCtrl.getTop();

		oldModal && oldModal.dismiss();

		const modal = await this.modalCtrl.create({
			backdropDismiss: false,
			component: ItemEditPage,
			componentProps: {
				params: params.pipe(
					map((paramMap: ParamMap) => ({
						id: paramMap.get('id'),
						itemId: paramMap.get('itemId'),
						itemType: paramMap.get('itemType'),
					})),
				),
			},
		});

		await modal.present();
	}

	async closeCreateItem() {
		const modal = await this.modalCtrl.getTop();

		modal && modal.dismiss();
	}

	async editSharing(listId: string) {
		const modal = await this.modalCtrl
			.create({
				component: UserShareModalPage,
				componentProps: {
					listId,
				},
			});
		await modal.present();
	}
}
