import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ShoppingListEditPage } from './shopping-list-edit.page';
import { ShoppingListEditFormComponent } from '../../components/shopping-list-edit-form/shopping-list-edit-form.component';
import { ShoppingItemListComponent } from '../../components/shopping-item-list/shopping-item-list.component';
import { CommonComponentsModule } from '../../components/common-components.module';

const routes: Routes = [
	{
		path: '',
		component: ShoppingListEditPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		CommonComponentsModule,
	],
	declarations: [
		ShoppingListEditPage,
		ShoppingListEditFormComponent,
		ShoppingItemListComponent,
	],
	exports: [
		ShoppingListEditFormComponent,
		ShoppingItemListComponent,
	],
})
export class ShoppingListEditPageModule {}
