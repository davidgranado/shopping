import { Component, OnInit, OnDestroy } from '@angular/core';
import { recordPageView } from '../../utils';
import { UserService } from '../../services/user.service';

interface PageLink {
	url: string;
	label: string;
}

@Component({
	selector: 'app-settings',
	styleUrls: ['./settings.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-title>
					<div class="image-logo-icon"></div> Settings
				</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content>
			<div class="foo"></div>
			<ion-list>
				<ion-item
					detail
					routerLink="{{pageLink.url}}"
					routerDirection="forward"
					*ngFor="let pageLink of pageLinks; trackBy: trackData"
				>
					<ion-label>{{pageLink.label}}</ion-label>
				</ion-item>
			</ion-list>
		</ion-content>
	`,
})
export class SettingsPage implements OnInit, OnDestroy {
	sub: any;
	pageLinks: PageLink[] = [{
		url: '/about',
		label: 'About',
	// Disable membership until launch
	// }, {
	// 	url: '/membership',
	// 	label: 'Membership',
	}];

	constructor(
		private userService: UserService,
	) {
		recordPageView('Settings', '/tabs/settings');
	}

	async ngOnInit() {
		this.sub = this.userService.user.subscribe(u => {
			this.pageLinks = [{
				url: '/about',
				label: 'About',
			}];

			if(u) {
				this.pageLinks = [
					{
						url: '/user',
						label: 'User Management',
					}, {
						url: '/membership',
						label: 'Membership',
					},
					...this.pageLinks,
				];
			}
		});
	}

	ngOnDestroy() {
		this.sub && this.sub.unsubscribe();
	}

	// Perf optimization
	trackData(_: number, pageLink: PageLink): any {
		return pageLink.url;
	}

}
