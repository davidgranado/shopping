import { Component, ViewChild } from '@angular/core';
import { ActionSheetController, ModalController, NavController, AlertController, IonSelect } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { ShoppingListItem, ShoppingList, EditableBy } from '../../interfaces';
import { ActionSheetButton } from '@ionic/core';
import { CreateListModalPage } from '../create-list-modal/create-list-modal.page';
import { UserShareModalPage } from '../user-share-modal/user-share-modal.page';
import { recordPageView, getPrice } from '../../utils';
import { ShoppingListService } from '../../services/shopping-list.service';
import { UserService } from '../../services/user.service';
import { SavedListService } from '../../services/saved-list.service';
import { map, startWith } from 'rxjs/operators';
import { Observable, combineLatest } from 'rxjs';
import { FormControl } from '@angular/forms';

const SortFns: any = {
	dateAsc(a: ShoppingList, b: ShoppingList) {
		// tslint:disable-next-line: no-non-null-assertion
		return a.created!.toDate().getTime() - b.created!.toDate().getTime();
	},
	dateDsc(a: ShoppingList, b: ShoppingList) {
		// tslint:disable-next-line: no-non-null-assertion
		return b.created!.toDate().getTime() - a.created!.toDate().getTime();
	},
	nameAsc(a: ShoppingList, b: ShoppingList) {
		return (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1;
	},
	nameDsc(a: ShoppingList, b: ShoppingList) {
		return (a.name.toLowerCase() < b.name.toLowerCase()) ? 1 : -1;
	},
};

@Component({
	selector: 'app-shopping-lists',
	styleUrls: ['./shopping-lists.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-title>
					<div class="image-logo-icon"></div> Shopping Lists
				</ion-title>
				<ion-buttons slot="end">
					<ion-button (click)="openSortSelect()">
						<ion-icon name="funnel"></ion-icon>
					</ion-button>
				</ion-buttons>
				<ion-item hidden>
					<ion-label>
						Sort
					</ion-label>
					<ion-select
						#sortOption
						okText="Okay"
						[formControl]="selectControl"
					>
						<ion-select-option value="dateDsc">Date (New-Old)</ion-select-option>
						<ion-select-option value="dateAsc">Date (Old-New)</ion-select-option>
						<ion-select-option value="nameAsc">Name (A-Z)</ion-select-option>
						<ion-select-option value="nameDsc">Name (Z-A)</ion-select-option>
					</ion-select>
				</ion-item>
			</ion-toolbar>
		</ion-header>

		<!--double check this conditional.  I'd expect opposite result-->
		<ion-content [ngClass]="{'ion-padding': !(hasLists | async)}">
			<div class="foo"></div>
			<p class="empty-list-message" *ngIf="!(hasLists | async)">
				<em>Ready to plan a your next shopping trip?  Need to save your gift ideas?<br>
				Create a new list!</em>
			</p>
			<ion-grid fixed>
				<ion-row *ngIf="shoppingLists | async as lists">
					<ion-col
						size-xs="12"
						size-sm="6"
						size-md="4"
						size-lg="3"
						*ngFor="let shoppingList of lists; trackBy: trackData"
					>
						<ion-card
							class="list-card ion-activatable hammerjs-press-scroll-hack"
							(click)="editShoppingList(shoppingList.id)"
							(press)="openActionSheet(shoppingList)"
						>
							<ion-ripple-effect></ion-ripple-effect>
							<ion-button
								class="list-delete"
								fill="clear"
								size="small"
								(click)="handleDelete(shoppingList, $event)"
							>
								<ion-icon slot="icon-only" name="close"></ion-icon>
							</ion-button>
							<ion-card-header>
								<ion-card-title *ngIf="shoppingList.finishedBy">
									<s>
										{{ shoppingList.name }}
									</s>
								</ion-card-title>
								<ion-card-title *ngIf="!shoppingList.finishedBy">
									{{ shoppingList.name }}
								</ion-card-title>
								<ion-card-subtitle *ngIf="shoppingList.items">
									{{finishedItemCount(shoppingList)}} / {{shoppingList.items.length}} items purchased
								</ion-card-subtitle>
							</ion-card-header>
							<ion-item lines="none">
								<ion-label>
									<p *ngIf="shoppingList.items">
										{{doneCost(shoppingList) | currency}} / {{totalCost(shoppingList.items) | currency}}
									</p>
									<ng-container *ngIf="amOwner(shoppingList.ownerId)">
										<p *ngIf="showSharedUserList(shoppingList)">
											Sharing with {{sharedUsers(shoppingList)}}
										</p>
										<p *ngIf="shoppingList.editableBy && !showSharedUserList(shoppingList)">
											Sharing with no one
										</p>
										<p *ngIf="shoppingList.editableBy">
											{{editableByMsg(shoppingList)}}
										</p>
									</ng-container>
									<p *ngIf="!amOwner(shoppingList.ownerId)">
										{{shoppingList.ownerUsername}} owns this list.
									</p>
									<p>
										Created: {{shoppingList.created.toDate() | date}}
									</p>
								</ion-label>
							</ion-item>
							<ion-button
								expand="full"
								class="list-actions"
								(click)="openActionSheet(shoppingList, $event)"
							>
								More <ion-icon name="ellipsis-vertical-outline"></ion-icon>
							</ion-button>
						</ion-card>
					</ion-col>
				</ion-row>
			</ion-grid>
			<ion-fab vertical="bottom" horizontal="end" slot="fixed">
				<ion-fab-button
					mini
					(click)="openCreateList()"
					color="secondary"
				>
					<ion-icon name="add"></ion-icon>
				</ion-fab-button>
			</ion-fab>
		</ion-content>
	`,
})
export class ShoppingListsPage {
	@ViewChild('sortOption', {static: false}) sortOption: IonSelect | null = null;
	selectControl = new FormControl('dateDsc');
	shoppingLists: Observable<ShoppingList[]>;

	constructor(
		private alertCtrl: AlertController,
		private actionSheetCtrl: ActionSheetController,
		private modalCtrl: ModalController,
		private navCtrl: NavController,
		private toastService: ToastService,
		private savedListService: SavedListService,
		private shoppingListService: ShoppingListService,
		private bar: UserService,
	) {
		recordPageView('Shopping Lists', '/tabs/home');
		this.shoppingLists = combineLatest([
			this.shoppingListService.lists,
			this.selectControl.valueChanges.pipe(
				// ???: Not sure if misunderstanding or bug. But initial
				// value doesn't flow through. Only detects subsequent
				// changes
				startWith(this.selectControl.value),
			),
		]).pipe(
			map(
				([lists, sortValue]) =>
					lists.sort(SortFns[sortValue]),
			),
		);
	}

	get hasLists() {
		return this.shoppingListService.hasLists;
	}

	openSortSelect() {
		this.sortOption && this.sortOption.open();
	}

	editableByMsg(list: ShoppingList) {
		if(list.editableBy === EditableBy.Anyone) {
			return 'Editable by anyone';
		} else if(list.editableBy === EditableBy.SpecifiedUsers) {
			return 'Editable by shared users';
		}
	}

	finishedItemCount(shoppingList: ShoppingList) {
		return shoppingList
			.items
			.filter(
				item => item.id && shoppingList.finishedItems[item.id],
			)
			.length;
	}

	amOwner(userId: string) {
		return this.bar.userId === userId;
	}

	showSharedUserList(shoppingList: ShoppingList) {
		return !!(this.amOwner(shoppingList.ownerId) && this.sharedUsers(shoppingList).length);
	}

	sharedUsers(shoppingList: ShoppingList) {
		const shared = shoppingList.shared;

		if(!shared) {
			return '';
		}

		return Object
			.keys(shared)
			.map(id => shared[id].username)
			.join(', ');
	}
	// Perf optimization
	trackData(_: number, item: ShoppingList): any {
		return item.id;
	}

	doneCost(shoppingList: ShoppingList) {
		return shoppingList
			.items
			.filter(item => item.id && shoppingList.finishedItems[item.id])
			.reduce(
				(total, item) => total + getPrice(item).price,
				0,
			) / 100;
	}

	totalCost(items: ShoppingListItem[]) {
		return items
			.reduce((total, item) => total + getPrice(item).price,
			0,
		) / 100;
	}

	async delete(shoppingList: ShoppingList) {
		if(!shoppingList.id) {
			return;
		}

		await this.shoppingListService.delete(shoppingList.id);
		this.toastService.show(`"${shoppingList.name}" deleted`);
	}

	handleDelete(shoppingList: ShoppingList, e: Event) {
		e.stopPropagation();
		this.confirmDelete(shoppingList);
	}

	async confirmDelete(shoppingList: ShoppingList) {
		const alert = await this.alertCtrl.create({
			message: `Are you sure you want to delete ${shoppingList.name}?`,
			header: `Delete ${shoppingList.name}?`,
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
				}, {
					text: 'Yes',
					cssClass: 'primary',
					handler: () => this.delete(shoppingList),
				},
			],
		});
		alert.present();
	}

	async editSharing(shoppingList: ShoppingList) {
		const modal = await this.modalCtrl
			.create({
				component: UserShareModalPage,
				componentProps: {
					listId: shoppingList.id,
				},
			});
		await modal.present();
	}

	editShoppingList(id: string) {
		this.navCtrl.navigateForward(`/shopping-list/${id}`);
	}

	async toggleFinishList(list: ShoppingList) {
		if(!this.bar.isLoggedIn) {
			await this.bar.loginAnonymously();
		}
		const userDoc = await this.bar.userDocPromise;

		if(!userDoc) {
			return;
		}

		const username = userDoc.username;

		if(!list.finishedBy && username) {
			list.finishedBy = username;
		} else {
			list.finishedBy = '';
		}

		this.shoppingListService.save(list);
	}

	async openActionSheet(shoppingList: ShoppingList, e?: Event) {
		e && e.stopPropagation();
		const buttons: ActionSheetButton[] = [{
			handler: () => shoppingList.id && this.editShoppingList(shoppingList.id),
			icon: 'create',
			text: `Open`,
		}, {
			handler: () => this.savedListService.createFromShoppingList(shoppingList),
			icon: 'bookmark',
			role: '',
			text: 'Save',
		}, {
			handler: () => this.toggleFinishList(shoppingList),
			icon: shoppingList.finishedBy ? 'arrow-undo-outline' : 'checkmark',
			text: shoppingList.finishedBy ? 'Unmark as Done' : 'Mark Done',
		}, {
			handler: () => this.editSharing(shoppingList),
			icon: 'person-add',
			text: 'Share',
		}, {
			handler: () => this.duplicate(shoppingList),
			icon: 'copy',
			text: 'Duplicate',
		}, {
			handler: () => this.delete(shoppingList),
			icon: 'trash',
			role: 'destructive',
			text: 'Delete',
		}, {
			handler() {},
			icon: 'close',
			role: 'cancel',
			text: 'Cancel',
		}] as any;

		const actionSheet = await this.actionSheetCtrl.create({
			buttons,
			header: `'${shoppingList.name}' Actions`,
		});
		await actionSheet.present();
	}

	async duplicate(shoppingList: ShoppingList) {
		if(!shoppingList.id) {
			return;
		}

		this.shoppingListService.duplicate(shoppingList);
	}

	async openCreateList() {
		const modal = await this.modalCtrl
			.create({
				component: CreateListModalPage,
			});
		await modal.present();
	}
}
