import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ShoppingListsPage } from './shopping-lists.page';
import { CreateListModalPage } from '../create-list-modal/create-list-modal.page';

const routes: Routes = [
	{
		path: '',
		component: ShoppingListsPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
	],
	declarations: [
		ShoppingListsPage,
		CreateListModalPage,
	],
	entryComponents: [
		CreateListModalPage,
	],
})
export class ShoppingListsPageModule {}
