import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { recordPageView } from '../../utils';

@Component({
	selector: 'app-registration',
	styleUrls: ['./registration.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-button (click)="goBack()">
						<ion-icon name="close"></ion-icon>
						<span class="ios-only">Close</span>
					</ion-button>
				</ion-buttons>
				<ion-title>
					ShopLystr - Register
				</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding">
			<p>
				<img class="main-logo" src="/assets/imgs/logo.png">
			</p>
			<app-registration-form (registered)="handleRegistration()"></app-registration-form>
		</ion-content>
	`,
})
export class RegistrationPage {

	constructor(
		private navCtrl: NavController,
	) {
		recordPageView('Register', '/register');
	}

	async handleRegistration() {
		await this.navCtrl.navigateRoot('/tabs/home');
	}

	goBack() {
		this.navCtrl.navigateBack('/tabs/home');
	}

}
