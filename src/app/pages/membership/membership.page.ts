import { Component } from '@angular/core';
import { NavController, ModalController, AlertController, LoadingController } from '@ionic/angular';
import { recordPageView } from '../../utils';
import { CheckoutModalPage } from '../modals/checkout-modal/checkout-modal.page';
import { MembershipService } from '../../services/membership.service';
import { PaymentService } from '../../services/payment.service';
import { ToastService } from '../../services/toast.service';
import { firestore } from 'firebase';
import { SubscriptionDoc } from '../../interfaces';

@Component({
	selector: 'app-membership',
	styleUrls: ['./membership.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-button (click)="goBack()">
						<ion-icon name="arrow-back"></ion-icon>
						<span class="ios-only">Back</span>
					</ion-button>
				</ion-buttons>
				<ion-title>
					Membership
				</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding" *ngIf="subscriptionDoc | async as doc">
			<ion-card color="{{(isPro | async) && 'light'}}" >
				<ion-card-header>
					<ion-card-title>
						Pro
					</ion-card-title>
					<ion-card-subtitle *ngIf="isBasic | async">
						$.99/month or $8.99/year
					</ion-card-subtitle>
					<ion-card-subtitle *ngIf="isPro | async">
						{{isCancelled(doc) ? 'Expires' : 'Renews'}}
						{{getDate(doc.periodEnd) | date: 'longDate'}}
					</ion-card-subtitle>
				</ion-card-header>
				<ion-card-content>
					<ul>
						<li>
							All Basic Plan Features
						</li>
						<li>
							<strong>Unlimited</strong> Saved Lists
						</li>
						<li>
							<strong>Unlimited</strong> Private Saved Items
						</li>
					</ul>
				</ion-card-content>
				<ion-card-content>
					<ion-button
						*ngIf="isPro | async"
						color="secondary"
						expand="block"
					>
						<ion-icon name="checkmark-circle-outline"></ion-icon> Subscribed
					</ion-button>
					<ion-button
						*ngIf="!(isPro | async)"
						color="primary"
						expand="block"
						(click)="openCheckout()"
					>
						<ion-ripple-effect></ion-ripple-effect>
						Go Pro
					</ion-button>
				</ion-card-content>
			</ion-card>
			<ion-card color="{{(isBasic | async) && 'light'}}" >
				<ion-card-header>
					<ion-card-title>
						Basic
					</ion-card-title>
					<ion-card-subtitle *ngIf="isBasic | async">
						Free
					</ion-card-subtitle>
				</ion-card-header>
				<ion-card-content>
					<ul>
						<li>
							<strong>Unlimited</strong> Shopping Lists
						</li>
						<li>
							<strong>Unlimited</strong> Public Saved Items
						</li>
						<li>
							<strong>Unlimited</strong> Public Saved Lists
						</li>
						<li>
							Access Lists From Mobile or Desktop
						</li>
						<li>
							Share Lists with Others
						</li>
						<li>
							Real-time List Updates
						</li>
						<li>
							Up to 10 Private Saved Items
						</li>
						<li>
							Up to 5 Private Saved Lists
						</li>
					</ul>
				</ion-card-content>
				<ion-card-content>
					<ion-button
						*ngIf="isBasic | async"
						color="secondary"
						expand="block"
					>
						<ion-icon name="checkmark-circle-outline"></ion-icon>
						Subscribed
					</ion-button>
					<ion-button
						*ngIf="(isPro | async) && !isCancelled"
						color="primary"
						expand="block"
						(click)="unsubscribe()"
					>
						<ion-ripple-effect></ion-ripple-effect>
						Downgrade
					</ion-button>
				</ion-card-content>
			</ion-card>
		</ion-content>
	`,
})
export class MembershipPage {
	constructor(
		public navCtrl: NavController,
		private modalCtrl: ModalController,
		private membershipService: MembershipService,
		private alertCtl: AlertController,
		private paymentService: PaymentService,
		private loadingCtrl: LoadingController,
		private toastService: ToastService,
	) {
		recordPageView('Membership', '/membership');
	}

	get isBasic() {
		return this.membershipService.isBasic;
	}

	get isPro() {
		return this.membershipService.isPro;
	}

	get subscriptionDoc() {
		return this.membershipService.subscriptionDoc;
	}

	getDate(time: firestore.Timestamp) {
		return time.toDate();
	}

	isCancelled(subscriptionDoc: SubscriptionDoc) {
		return subscriptionDoc.plan !== 'basic' && !!subscriptionDoc.cancellationDate;
	}

	async openCheckout() {
		const modal = await this.modalCtrl
			.create({
				component: CheckoutModalPage,
				componentProps: {},
			});
		await modal.present();
	}

	async unsubscribe() {
		const alert = await this.alertCtl.create({
			header: 'Unsubscribe from Pro',
			backdropDismiss: false,
			message: 'Are you sure you want to unsubscribe?',
			keyboardClose: true,
			buttons: [
				{
					text: 'Continue Unsubscribe',
					handler: async () => {
						const loading = await this.loadingCtrl.create({});
						await loading.present();

						try {
							await this.paymentService.unsubscribe();
							this.toastService.show('Unsubscribed! ShopLystr Pro will expire at the end of the period.');
						} catch {
							this.showErrorMsg();
						}

						await loading.dismiss();
					},
				}, {
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
				},
			],
		});
		await alert.present();
	}

	async showErrorMsg() {
		const alert = await this.alertCtl.create({
			header: 'There was a problem',
			message: `
				There was a problem unsubscribing. Please try again later.
				If problems persist, please email <a href="mailto:contact@shoplystr.com">contact@shoplystr.com</a>.
			`,
			keyboardClose: true,
			buttons: ['Ok'],
		});
		await alert.present();
	}

	goBack() {
		this.navCtrl.back();
	}
}
