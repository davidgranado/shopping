import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MembershipPage } from './membership.page';
import { CheckoutModalPage } from '../modals/checkout-modal/checkout-modal.page';
import { CheckoutFormComponent } from '../../components/checkout-form/checkout-form.component';
import { CommonComponentsModule } from '../../components/common-components.module';

const routes: Routes = [
	{
		path: '',
		component: MembershipPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		CommonComponentsModule,
	],
	declarations: [
		CheckoutModalPage,
		MembershipPage,
		CheckoutFormComponent,
	],
	entryComponents: [
		CheckoutModalPage,
	],
})
export class MembershipPageModule {}
