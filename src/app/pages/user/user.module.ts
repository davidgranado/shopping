import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserPage } from './user.page';
import { UsernameEditModalPage } from '../username-edit-modal/username-edit-modal.page';

const routes: Routes = [
	{
		path: '',
		component: UserPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
	],
	declarations: [
		UserPage,
		UsernameEditModalPage,
	],
	entryComponents: [
		UsernameEditModalPage,
	],
})
export class UserPageModule {}
