import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavController, LoadingController } from '@ionic/angular';
import { recordPageView } from '../../utils';
import { ToastService } from '../../services/toast.service';
import { UsernameEditModalPage } from '../username-edit-modal/username-edit-modal.page';
import { filter, first } from 'rxjs/operators';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-user',
	styleUrls: ['./user.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-back-button defaultHref="/tabs/home"></ion-back-button>
				</ion-buttons>
				<ion-title>
					User Management
				</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding" *ngIf="user | async as u">
			<ng-container *ngIf="!u.email">
				<h2>You do not have an account</h2>
				<p>
					Sign up now to save
					your lists and access them accross your devices!
				</p>
				<ion-button expand="full" routerLink="/register">
					Create a free account now
				</ion-button>
				<p>
					Already have an account?
				</p>
				<ion-button expand="full" routerLink="/login">
					Sign in with your existing account
				</ion-button>
			</ng-container>
			<ng-container *ngIf="u.username">
				<ion-list>
					<ion-item (click)="openEditUsername()" button>
						<ion-label>
							Signed in as <strong>{{u.username}}</strong>
						</ion-label>
						<ion-icon name="create" slot="end"></ion-icon>
					</ion-item>
					<ion-item>
						<p>
							Email:
							<ng-container *ngIf="!u.email">
								<em>Not Registered</em>
							</ng-container>
							<ng-container *ngIf="u.email">
								<strong>{{u.email}}</strong>
								<ng-container *ngIf="needsVerification">
									<br/>
									<small>
										<em>Unverified</em>
									</small>
									<ion-button
										expand="full"
										(click)="resendVerification()"
									>
										Resend verification email
									</ion-button>
								</ng-container>
							</ng-container>
						</p>
					</ion-item>
					<ion-item *ngIf="u.email" button (click)="logout()">
						<ion-label>
							Logout
						</ion-label>
						<ion-icon name="log-out" slot="end"></ion-icon>
					</ion-item>
					<ion-item button (click)="promptUserDelete()">
						<ion-label>
							Delete Account
						</ion-label>
						<ion-icon name="alert" color="danger" slot="end"></ion-icon>
					</ion-item>
				</ion-list>
			</ng-container>
		</ion-content>
	`,
})
export class UserPage implements OnInit {
	constructor(
		private modalCtrl: ModalController,
		private userService: UserService,
		private toastCtrl: ToastService,
		private alertCtrl: AlertController,
		private navCtrl: NavController,
		private loadingCtrl: LoadingController,
	) {
		recordPageView('User Management', '/user');
	}

	get user() {
		return this.userService.userDoc;
	}

	get needsVerification() {
		return this.userService.needsEmailVerification;
	}

	async ngOnInit() { }

	async logout() {
		try {
			await this.prepNavOnLogout();
			await this.userService.logout();
		} catch(e) {
			console.error(e);
			this.toastCtrl.show(e.message);
		}
	}

	async promptUserDelete() {
		const userDoc = await this.userService.userDocPromise;

		if(!userDoc) {
			return;
		}

		const username = userDoc.username;
		const alert = await this.alertCtrl.create({
			cssClass: 'big-alert',
			buttons: [
				{
					role: 'close',
					text: 'Close',
				}, {
					text: 'DELETE',
					handler: () => this.delete(),
				},
			],
			message: `Are your suer  you want to delete ${username}? This will permanently delete your data!`,
			header: `Delete "${username}"`,
		});
		alert.present();
	}

	async delete() {
		let prep: any = {};

		try {
			prep = await this.prepNavOnLogout();
			await this.userService.delete();
		} catch(e) {
			console.error(e);
			prep.sub && prep.sub.unsubscribe();
			prep.loader && prep.loader.dismiss();
			await this.toastCtrl.show(e.message);
		}
	}

	async prepNavOnLogout() {
		const loader = await this.loadingCtrl.create({
			message: 'Submitting account for deletion...',
		});
		await loader.present();

		const sub = this.userService
			.userDoc
			.pipe(filter(u => !u))
			.pipe(first())
			.subscribe(async () => {
				await this.navCtrl.navigateRoot('/tabs/home');
				await loader.dismiss();
			});

		return {
			sub,
			loader,
		};
	}

	async openEditUsername() {
		const modal = await this.modalCtrl.create({
			component: UsernameEditModalPage,
		});
		modal.present();
	}

	async resendVerification() {
		this.userService.sendEmailVerification();
	}

}
