import { Component } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from '../../services/toast.service';
import { recordPageView } from '../../utils';
import { ItemEditPage } from '../item-edit/item-edit.page';
import { SavedListService } from '../../services/saved-list.service';
import { Observable, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { SavedList } from '../../interfaces';

@Component({
	selector: 'app-saved-list-edit',
	styleUrls: ['./saved-list-edit.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-back-button defaultHref="/tabs/saved-lists"></ion-back-button>
				</ion-buttons>
				<ng-container *ngIf="model | async as list">
					<ion-title *ngIf="list.name">
						{{list.name}}
					</ion-title>
					<ion-buttons slot="end">
						<ion-button *ngIf="!list.private" (click)="share(list)">
							Share<ion-icon slot="end" name="share-social-outline"></ion-icon>
						</ion-button>
					</ion-buttons>
				</ng-container>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding" *ngIf="!(model | async)">
			<p>Sorry. But that list does not seem to exist any more or was removed by the creator.</p>
			<p>Try creating a <a [routerLink]="['/tabs/home']" routerDirection="back">new shopping list</a>!</p>
		</ion-content>

		<ion-content *ngIf="model | async as list">
			<ion-grid fixed class="full-height-grid ion-no-padding">
				<ion-row>
					<ion-col class="ion-no-padding">
						<app-saved-lists-edit-form
							[saved-list]="list"
							(save)="save($event)"
							(updateStores)="handleUpdateStores($event, list)"
						></app-saved-lists-edit-form>
					</ion-col>
				</ion-row>
			</ion-grid>
			<ion-fab
				*ngIf="canEdit"
				vertical="bottom"
				horizontal="end"
				slot="fixed"
			>
				<ion-fab-button
					mini
					(click)="routeToCreateItem(list.id)"
					color="secondary"
				>
					<ion-icon name="add"></ion-icon>
				</ion-fab-button>
			</ion-fab>
		</ion-content>
	`,
})
export class SavedListEditPage {
	public model: Observable<SavedList | null>;
	public canEdit: Observable<boolean>;
	public modelId = '';

	constructor(
		private savedListService: SavedListService,
		private modalCtrl: ModalController,
		private toastService: ToastService,
		private navCtrl: NavController,
		route: ActivatedRoute,
	) {
		recordPageView('Saved List Edit', '/saved-list/');

		this.model = route.paramMap.pipe(
			map(paramMap => paramMap.get('id')),
			switchMap(
				id =>
					id ?
						savedListService.getList(id) :
						of(null),
			),
		);

		this.canEdit = this.model.pipe(
			map(list =>
				list ?
					this.savedListService.canEdit(list) :
					false,
			),
		);
	}

	async save(model: SavedList) {
		await this.savedListService.save(model);
		this.toastService.show(`"${model.name}" saved`);
	}

	async routeToCreateItem(listId: string) {
		this.navCtrl.navigateForward(`/saved-list/${listId}/item/saved/`, {
			animated: false,
		});
	}

	handleUpdateStores(storeName: string, list: SavedList) {
		list.items.forEach(i => {
			if(!(i.storePrices && i.storePrices.length)) {
				return;
			}

			const price = i.storePrices.find(
					p => p.storeName.toLowerCase() === storeName,
				);

			i.selectedPriceId = price ?
				price.id :
				'';
		});

		this.save(list);
	}

	onCreate(name: string) {
		if(!name.trim()) {
			return false;
		}

		this.savedListService.saveItem({
			aisle: '',
			brand: '',
			name,
			notes: '',
			price: 0,
			quantity: 1,
		}, this.modelId);

		return true;
	}

	async share(list: SavedList) {
		const {
			navigator,
		}: any = window;

		if(!navigator.share) {
			await navigator.clipboard.writeText(`${location.origin}/saved-list/${list.id}`);
			await this.toastService.show(`Copied url to "${list.name}"!`);
		} else {
			await navigator.share({
				title: `ShopLystr List: '${list.name}'`,
				text: 'Check out this shopping list!',
				url: location.href,
			});

			await this.toastService.show(`'${list.name}' shared.`);
		}
	}

	async openCreateItem(list: SavedList) {
		const modal = await this.modalCtrl.create({
			component: ItemEditPage,
			componentProps: {
				listType: 'saved-list',
				listId: list.id,
			},
		});

		await modal.present();
	}
}
