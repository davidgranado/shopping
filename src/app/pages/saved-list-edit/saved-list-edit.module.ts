import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SavedListEditPage } from './saved-list-edit.page';
import { SavedListsEditFormComponent } from '../../components/saved-lists-edit-form/saved-lists-edit-form.component';
import { CommonComponentsModule } from '../../components/common-components.module';

const routes: Routes = [
	{
		path: '',
		component: SavedListEditPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		CommonComponentsModule,
	],
	declarations: [
		SavedListEditPage,
		SavedListsEditFormComponent,
	],
	exports: [
		SavedListsEditFormComponent,
	],
})
export class SavedListEditPageModule {}
