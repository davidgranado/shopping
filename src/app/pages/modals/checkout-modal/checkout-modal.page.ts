import { Component } from '@angular/core';
import { recordPageView } from '../../../utils';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-checkout-modal',
	styleUrls: ['./checkout-modal.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-button (click)="closeModal()">
						<ion-icon name="close"></ion-icon>
						<span class="ios-only">Close</span>
					</ion-button>
				</ion-buttons>
				<ion-title>Checkout</ion-title>
			</ion-toolbar>
		</ion-header>
		<ion-content class="ion-padding">
			<app-checkout-form></app-checkout-form>
		</ion-content>
	`,
})
export class CheckoutModalPage {
	constructor(
		private modalCtrl: ModalController,
	) {
		recordPageView('Checkout', '/modal/checkout');
	}

	async closeModal() {
		const modal = await this.modalCtrl.getTop();
		modal && modal.dismiss();
	}
}
