import { Component, ViewChild } from '@angular/core';
import { SavedItem } from '../../interfaces';
import { ToastService } from '../../services/toast.service';
import { AlertController, ActionSheetController, NavController, IonSelect } from '@ionic/angular';
import { SavedItemService } from '../../services/saved-item.service';
import { FormControl } from '@angular/forms';
import { recordPageView } from '../../utils';
import { Observable, combineLatest } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MembershipService } from '../../services/membership.service';

const SortFns: any = {
	dateAsc(a: SavedItem, b: SavedItem) {
		// tslint:disable-next-line: no-non-null-assertion
		return a.created!.toDate().getTime() - b.created!.toDate().getTime();
	},
	dateDsc(a: SavedItem, b: SavedItem) {
		// tslint:disable-next-line: no-non-null-assertion
		return b.created!.toDate().getTime() - a.created!.toDate().getTime();
	},
	nameAsc(a: SavedItem, b: SavedItem) {
		return (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1;
	},
	nameDsc(a: SavedItem, b: SavedItem) {
		return (a.name.toLowerCase() < b.name.toLowerCase()) ? 1 : -1;
	},
};

@Component({
	selector: 'app-saved-items',
	template: `
	<ion-header>
		<ion-toolbar color="primary">
			<ion-title>
				<div class="image-logo-icon"></div> Saved Items
			</ion-title>
			<ion-buttons slot="end">
				<ng-container *ngIf="isBasic">
					<ion-button [routerLink]="['/membership']">
						<em><small>{{privateListCount | async}}/10 Private Lists</small></em>
						<ion-icon size="small" color="light" name="help-circle-outline"></ion-icon>
					</ion-button>
				</ng-container>
				<ng-container *ngIf="!isBasic">
					<em><small>Pro</small></em>
					<ion-icon size="small" color="secondary" name="checkmark-circle"></ion-icon>
				</ng-container>
				<ion-button (click)="openSortSelect()">
					<ion-icon name="funnel"></ion-icon>
				</ion-button>
			</ion-buttons>
			<ion-item hidden>
				<ion-label>
					Sort
				</ion-label>
				<ion-select
					#sortOption
					okText="Okay"
					[formControl]="selectControl"
				>
					<ion-select-option value="dateAsc">Oldest-Newest</ion-select-option>
					<ion-select-option value="dateDsc">Newest-Oldest</ion-select-option>
					<ion-select-option value="nameAsc">Name (A-Z)</ion-select-option>
					<ion-select-option value="nameDsc">Name (Z-A)</ion-select-option>
				</ion-select>
			</ion-item>
		</ion-toolbar>
	</ion-header>

	<ion-content [ngClass]="{'ion-padding': !(hasItems | async)}">
		<div class="foo"></div>
		<ion-grid fixed>
			<ion-row *ngIf="savedItems | async as items">
				<ion-col
					size-xs="12"
					size-sm="6"
					size-md="4"
					size-lg="3"
					*ngFor="let savedItem of items; trackBy: trackData"
				>
					<ion-card
						class="list-card ion-activatable hammerjs-press-scroll-hack"
						(click)="editSavedItem(savedItem.id)"
						(press)="openActionSheet(savedItem)"
					>
						<ion-ripple-effect></ion-ripple-effect>
						<ion-button
							class="list-delete"
							fill="clear"
							size="small"
							(click)="handleDelete(savedItem, $event)"
						>
							<ion-icon slot="icon-only" name="close"></ion-icon>
						</ion-button>
						<ion-card-header>
							<ion-card-title>
								{{ savedItem.name }}
							</ion-card-title>
						</ion-card-header>
						<ion-item lines="none">
							<ion-label>
								<p *ngIf="savedItem.price">
									{{'$'}}{{savedItem.price}}
								</p>
								<p *ngIf="savedItem.notes">
									{{savedItem.notes}}
								</p>
								<p *ngIf="savedItem.private">
									<ion-icon name="eye-off"></ion-icon>
								</p>
								<p *ngIf="savedItem.created">
									Created: {{(savedItem.created.toDate() | date)}}
								</p>
							</ion-label>
						</ion-item>
						<ion-button
							expand="full"
							class="list-actions"
							(click)="openActionSheet(savedItem, $event)"
						>
							More <ion-icon name="ellipsis-vertical-outline"></ion-icon>
						</ion-button>
					</ion-card>
				</ion-col>
			</ion-row>
		</ion-grid>
		<ion-fab vertical="bottom" horizontal="end" slot="fixed">
			<ion-fab-button
				mini
				color="secondary"
				(click)="editSavedItem()"
			>
				<ion-icon name="add"></ion-icon>
			</ion-fab-button>
		</ion-fab>
	</ion-content>

	`,
	styleUrls: ['./saved-items.page.scss'],
})
export class SavedItemsPage {
	@ViewChild('sortOption', {static: false}) sortOption: IonSelect | null = null;
	selectControl = new FormControl('nameAsc');
	savedItems: Observable<SavedItem[]>;

	constructor(
		private alertCtrl: AlertController,
		private actionSheetCtrl: ActionSheetController,
		private savedItemsService: SavedItemService,
		private toastService: ToastService,
		private navCtrl: NavController,
		private membershipService: MembershipService,
	) {
		recordPageView('Saved Items', '/tabs/saved-items');
		this.savedItems = combineLatest([
			this.savedItemsService.items,
			this.selectControl.valueChanges.pipe(
				// ???: Not sure if misunderstanding or bug. But initial
				// value doesn't flow through. Only detects subsequent
				// changes
				startWith(this.selectControl.value),
			),
		]).pipe(
			map(
				([lists, sortValue]) =>
					lists.sort(SortFns[sortValue]),
			),
		);
	}

	get hasItems() {
		return this.savedItemsService.hasItems;
	}

	get isBasic() {
		return this.membershipService.isBasic;
	}

	get privateListCount() {
		return this.savedItemsService.privateCount;
	}

	openSortSelect() {
		this.sortOption && this.sortOption.open();
	}

	// Perf optimization
	trackData(_: number, item: SavedItem): any {
		return item.id;
	}

	editSavedItem(id = '') {
		this.navCtrl.navigateForward(`/item/${id}`);
	}

	async delete(savedItem: SavedItem) {
		if(!savedItem.id) {
			return;
		}

		await this.savedItemsService.delete(savedItem.id);
		this.toastService.show(`"${savedItem.name}" deleted`);
	}

	handleDelete(savedItem: SavedItem, e: Event) {
		e.stopPropagation();
		this.confirmDelete(savedItem);
	}

	async openActionSheet(savedItem: SavedItem, e?: Event) {
		e && e.stopPropagation();
		const buttons = [{
			handler: () => this.editSavedItem(savedItem.id),
			icon: 'create',
			text: 'Edit',
		}, {
			handler: () => this.delete(savedItem),
			icon: 'trash',
			role: 'destructive',
			text: 'Delete',
		}, {
			handler() {},
			icon: 'close',
			role: 'cancel',
			text: 'Cancel',
		}];

		const actionSheet = await this.actionSheetCtrl.create({
			buttons,
			header: `'${savedItem.name}' Actions`,
		});
		await actionSheet.present();
	}

	async confirmDelete(savedItem: SavedItem) {
		const alert = await this.alertCtrl.create({
			message: `Are you sure you want to delete ${savedItem.name}?`,
			header: `Delete ${savedItem.name}?`,
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
				}, {
					text: 'Yes',
					cssClass: 'primary',
					handler: () => this.delete(savedItem),
				},
			],
		});
		alert.present();
	}
}
