import { Component } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { recordPageView } from '../../utils';
import { ToastService } from '../../services/toast.service';
import { UserService } from '../../services/user.service';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { UserDoc } from '../../core/user';
import { FormControl, Validators } from '@angular/forms';
import { tap, take, map } from 'rxjs/operators';

const ALPHA_NUM_REG = /^[a-z][a-z0-9]+$/i;

@Component({
	selector: 'app-username-edit-modal',
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-button
						(click)="closeModal()"
					>
						<ion-icon name="close"></ion-icon>
						<span class="ios-only">Close</span>
					</ion-button>
				</ion-buttons>
				<ion-title>
					Edit Username
				</ion-title>
			</ion-toolbar>
		</ion-header>
		<ion-content class="ion-padding" *ngIf="user | async as u">
			<ion-item>
				<ion-label position="floating">Username</ion-label>
				<ion-input
					type="text"
					[formControl]="newName"
				></ion-input>
			</ion-item>
			<ion-grid>
				<ion-row>
					<ion-col>
						<small>
							<em>
								Must:
								<ul>
									<li>
										Contain numbers and/or letters (uppor or lower case)
									</li>
									<li>
										Be at least 4 characters
									</li>
								</ul>
							</em>
						</small>
					</ion-col>
				</ion-row>
				<ion-row>
					<ion-col>
						<p>
							<ion-button
								expand="block"
								color="secondary"
								(click)="closeModal()"
							>
								Cancel
							</ion-button>
						</p>
					</ion-col>
					<ion-col>
						<p>
							<ion-button
								[disabled]="!(canSave | async)"
								expand="block"
								color="primary"
								(click)="save()"
							>
								Save
							</ion-button>
						</p>
					</ion-col>
				</ion-row>
			</ion-grid>
		</ion-content>
	`,
})
export class UsernameEditModalPage {
	saving = new BehaviorSubject(false);
	canSave: Observable<boolean>;
	user: Observable<UserDoc | null>;
	newName: FormControl;

	constructor(
		private modalCtrl: ModalController,
		private userService: UserService,
		private toastService: ToastService,
		private loadingCtrl: LoadingController,
	) {
		recordPageView('Username Edit', '/modal/username-edit');
		this.user = this.userService.userDoc;
		this.newName = new FormControl('', [
			Validators.required,
			Validators.minLength(2),
			Validators.pattern(ALPHA_NUM_REG),
		]);
		const isValid = this.newName.statusChanges
			.pipe(
				map(status => status === 'VALID'),
			);
		const isChanged = combineLatest([
			this.user,
			this.newName.valueChanges,
		]).pipe(
			map(
				([
					user,
					newName,
				]) => !!user && (user.username !== newName),
			),
		);
		this.canSave = combineLatest([
			this.saving,
			isValid,
			isChanged,
		]).pipe(
			map(
				([
					saving,
					valid,
					changed,
				]) => !saving && valid && changed,
			),
		);
		this.user.pipe(
			tap(u => u && this.newName.setValue(u.username)),
			take(1),
		).subscribe();
	}

	async closeModal() {
		const modal = await this.modalCtrl.getTop();
		modal && modal.dismiss();
	}

	async save() {
		this.saving.next(true);
		const loader = await this.loadingCtrl.create();
		loader.present();
		const username = this.newName.value;

		try {
			const updated = await this.userService.updateUsername(username);
			const message = updated ?
				`Username updated to "${username}"` :
				`"${username}" is already taken.`;

			this.toastService.show(message);

			if(updated) {
				this.closeModal();
			}
		} catch {
			this.toastService.show('There was a problem changing your username');
		}
		this.saving.next(false);
		loader.remove();
	}
}
