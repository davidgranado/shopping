import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { recordPageView } from '../../utils';

@Component({
	selector: 'app-login',
	styleUrls: ['./login.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-button (click)="goBack()">
						<ion-icon name="close"></ion-icon>
						<span class="ios-only">Close</span>
					</ion-button>
				</ion-buttons>
				<ion-title>
					ShopLystr - Login
				</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding">
			<p>
				<img class="main-logo" src="/assets/imgs/logo.png">
			</p>
			<app-login-form></app-login-form>
		</ion-content>
	`,
})
export class LoginPage implements OnInit {

	constructor(
		private navCtrl: NavController,
	) {
		recordPageView('Login', '/login');
	}

	ngOnInit() {
	}

	goBack() {
		this.navCtrl.navigateBack('/tabs/home');
	}

}
