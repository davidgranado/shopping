import { Component } from '@angular/core';
import { ShoppingListItem } from '../../interfaces';
import { NavController } from '@ionic/angular';
import { recordPageView } from '../../utils';
import { ShoppingListService } from '../../services/shopping-list.service';
import { SavedListService } from '../../services/saved-list.service';
import { of, Observable, combineLatest } from 'rxjs';
import { switchMap, map, tap, take, filter } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { ToastService } from '../../services/toast.service';
import { ActivatedRoute } from '@angular/router';
import { SavedItemService } from '../../services/saved-item.service';

type ItemType = 'shop' | 'saved' | '';

interface ItemParams {
	id: string;
	itemType: ItemType;
	itemId: string;
}

@Component({
	selector: 'app-item-edit',
	styleUrls: ['./item-edit.page.scss'],
	template: `
		<ng-container *ngIf="params | async as p">
			<ion-header>
				<ion-toolbar color="primary">
					<ion-buttons slot="start">
						<ion-button (click)="goBack()">
							<ion-icon name="close"></ion-icon>
							<span class="ios-only">Close</span>
						</ion-button>
					</ion-buttons>
					<ion-title>
						<ng-container *ngIf="isEdit | async else new">
							Edit {{(item | async)?.name}}
						</ng-container>
						<ng-template #new>
							Create Item
						</ng-template>
					</ion-title>
				</ion-toolbar>
			</ion-header>

			<ng-template #dne>
				<ion-content class="ion-padding">
					<p>Sorry. But that item does not seem to exist any more or was removed by the creator.</p>
				</ion-content>
			</ng-template>

			<ion-content *ngIf="item | async as m else dne">
				<app-shopping-list-item-edit-form
					[item]="m"
					(save)="save($event, p.itemType, p.id, false)"
					(saveAndFinish)="save($event, p.itemType, p.id, true)"
					(close)="goBack()"
					[isSavedItem]="!p.itemType"
					[disableAutocomplete]="!p.itemType"
					[showCreateParentLink]="showCreateParentLink | async"
					[isOwner]="isOwner | async"
				></app-shopping-list-item-edit-form>
			</ion-content>
		</ng-container>
	`,
})
export class ItemEditPage {
	public params: Observable<ItemParams>;
	public item: Observable<ShoppingListItem | null>;
	public showCreateParentLink: Observable<boolean>;
	public isEdit: Observable<boolean>;
	public isOwner: Observable<boolean>;

	constructor(
		private shoppingListService: ShoppingListService,
		private savedListService: SavedListService,
		private savedItemService: SavedItemService,
		private userService: UserService,
		private toastService: ToastService,
		private navCtrl: NavController,
		private route: ActivatedRoute,
	) {
		this.params = this.route.params.pipe(
			switchMap(p => (
				(!Object.keys(p).length && route.firstChild) ?
				route.firstChild.params :
				route.params
			)as Observable<ItemParams>),
		);
		this.item = this.params.pipe(
			switchMap(
				params => {
					if(params.itemType === 'shop') {
						if(params.itemId) {
							return this.shoppingListService.getItem(params.itemId, params.id || '');
						}
					} else {
						if(params.itemId) {
							return this.savedListService.getItem(params.itemId, params.id || '');
						}
					}

					return of(this.shoppingListService.createItem());
				},
			),
		);
		this.isOwner = combineLatest([
				this.userService.user,
				this.params,
			]).pipe(
				filter(([user, params]) => !!(user && params)),
				switchMap(
					([u, params]) => {
						if(params.itemType === 'shop') {
							if(params.itemId) {
								return this.shoppingListService.getList(params.id);
							}
						} else {
							if(params.itemId) {
								return this.savedListService.getList(params.id);
							}
						}

						return of(null);
					},
				),
				map(list => !!list && list.ownerId === this.userService.userId),
			);

		this.showCreateParentLink = combineLatest([
			this.params,
			this.item,
		]).pipe(
			switchMap(([params, item]) => {
				if(!(item && item.parentId)) {
					if(params.itemType === 'shop') {
						return this.shoppingListService.getList(params.id || '');
					} else {
						return this.savedListService.getList(params.id || '');
					}
				}

				return of(null);
			}),
			map(list => !!list && list.ownerId === this.userService.userId),
		);

		this.isEdit = this.params.pipe(
			map(params => !!params.id),
		);

		this.params.pipe(
			tap(({itemType}) => {
				if(itemType === 'shop') {
					recordPageView('Item edit', `/${itemType}/item`);
				} else {
					recordPageView('Item edit', `/${itemType}/item`);
				}
			}),
			take(1),
		).subscribe();
	}

	async save(model: ShoppingListItem, itemType: 'shop' | 'saved' | '' , listId: string, goBack: boolean) {
		let saveReq: Promise<any>;

		if(itemType) {
			if(itemType === 'shop') {
				saveReq = this.shoppingListService.saveItem(model, listId);
			} else {
				saveReq = this.savedListService.saveItem(model, listId);
			}
		} else {
			saveReq = this.savedItemService.save(model);
		}

		if(goBack) {
			this.goBack();
		} else if(!model.id) {
			this.item = of(this.shoppingListService.createItem());
		}


		await saveReq;
		this.toastService.show(`"${model.name}" saved`);
	}

	async goBack() {
		this.params && this.params.pipe(
			tap(params => {
				this.navCtrl.back({
					animated: !params.itemType,
				});
			}),
			take(1),
		).subscribe();
	}
}
