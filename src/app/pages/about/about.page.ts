import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { recordPageView } from '../../utils';
import { ApiService } from '../../services/api.service';

@Component({
	selector: 'app-about',
	styleUrls: ['./about.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-back-button defaultHref="/tabs/home"></ion-back-button>
				</ion-buttons>
				<ion-title>
					About
				</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding">
			<img class="logo" src="assets/imgs/logo.png"/>
			<p>
				<a href="https://shoplystr.com/privacy-policy.html" target="__blank">
					Privacy Policy
				</a>
			</p>
			<p>
				<a href="https://shoplystr.com/tos.html" target="__blank">
					Terms of Service
				</a>
			</p>
			<p>
				Did you find an issue?  Do you have some questions or comments?
			</p>
			<p>
				That's great! Just email <a href="mailto:contact@shoplystr.com">contact@shoplystr.com</a> or
				submit them below!
			</p>
			<p>
				<ion-button
					expand="full"
					*ngIf="!feedbackSubmitted"
					(click)="openFeedbackForm()"
				>
					Submit Feedback
				</ion-button>
			</p>
			<p class="feedback-thank-you" *ngIf="feedbackSubmitted">
				Thank you for your feedback!
			</p>
			<p>
				{{version}}
			</p>
			<p>
				What's new:
			</p>
			<ul>
				<li>
					Beta release of ShopLystr!
				</li>
			</ul>
		</ion-content>
	`,
})
export class AboutPage implements OnInit {
	public feedbackSubmitted = false;
	public version = environment.appVersion;

	constructor(
		private alertCtrl: AlertController,
		public navCtrl: NavController,
		private apiService: ApiService,
	) {
		recordPageView('About', '/about');
	}

	async ngOnInit() { }

	async submitFeedback(feedback: string) {
		const trimmedFeedback = feedback.trim();

		if(trimmedFeedback) {
			await this.apiService.feedback(trimmedFeedback);
			this.feedbackSubmitted = true;
		}
	}

	async openFeedbackForm() {
		const alert = await this.alertCtrl.create({
			buttons: [
				{
					role: 'cancel',
					text: 'Cancel',
				}, {
					handler: data => {
						this.submitFeedback(data.feedback);
					},
					text: 'Submit',
				},
			],
			inputs: [
				{
					name: 'feedback',
					placeholder: 'Enter your feedback and/or bug',
				},
			],
			header: 'Feedback',
		});
		await alert.present();
	}
}
