import { Component, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SavedItem, StorePrice } from '../../interfaces';
import { NavController, AlertController } from '@ionic/angular';
import { recordPageView, pick, minTrimmedLengthValidator, uuid, defer, getPrice } from '../../utils';
import { Observable, of, BehaviorSubject, Subject, combineLatest } from 'rxjs';
import { switchMap, map, tap, filter, takeUntil } from 'rxjs/operators';
import { ToastService } from '../../services/toast.service';
import { ActivatedRoute } from '@angular/router';
import { MembershipService } from '../../services/membership.service';
import { SavedItemService } from '../../services/saved-item.service';
import { Key } from 'ts-key-enum';
import { FormGroup, FormControl, Validators } from '@angular/forms';

interface ItemParams {
	id?: string;
}

@Component({
	selector: 'app-saved-item-edit',
	styleUrls: ['./saved-item-edit.page.scss'],
	template: `
		<ng-container>
			<ion-header>
				<ion-toolbar color="primary">
					<ion-buttons slot="start">
						<ion-button (click)="goBack()">
							<ion-icon name="close"></ion-icon>
							<span class="ios-only">Close</span>
						</ion-button>
					</ion-buttons>
					<ion-title>
						<ng-container *ngIf="isEdit | async else new">
							Edit {{(item | async)?.name}}
						</ng-container>
						<ng-template #new>
							Create Item
						</ng-template>
					</ion-title>
				</ion-toolbar>
			</ion-header>

			<ng-template #dne>
				<ion-content class="ion-padding">
					<p>Sorry. But that item does not seem to exist any more or was removed by the creator.</p>
				</ion-content>
			</ng-template>
			<ion-content *ngIf="item | async as m else dne">
				<ion-grid fixed>
					<ion-row>
						<ion-col size-md="6" push-md="3">
							<div [formGroup]="itemFG">
								<ion-item>
									<ion-label position="stacked">Name</ion-label>
									<ion-input
										#nameInput
										formControlName="name"
										type="text"
										(keyup)="handleKey($event)"
									></ion-input>
								</ion-item>

								<ion-item>
									<ion-label position="stacked">Brand</ion-label>
									<ion-input
										#nameInput
										formControlName="brand"
										type="text"
										(keyup)="handleKey($event)"
									></ion-input>
								</ion-item>
								<ion-item
									[class.ion-item-inline-control-wide-empty]="!(hasStorePrices | async)"
									class="ion-item-inline-control-wide ion-item-inline-control"
								>
									<ion-button
										slot="start"
										*ngIf="selectedPriceId | async"
										(click)="removeStorePrice()"
									>
										<ion-icon slot="icon-only" name="remove"></ion-icon>
									</ion-button>
									<ion-label hidden>Store Prices</ion-label>
									<ng-container *ngIf="item | async as i">
										<ion-select
											*ngIf="hasStorePrices | async"
											interface="popover"
											[selectedText]="(price | async)?.storeName"
											[value]="selectedPriceId | async"
											(ionChange)="setSelectedPriceId($event.detail.value)"
										>
											<ion-select-option [value]="''">
												{{'$'}}{{i.price}} - General
											</ion-select-option>
											<ion-select-option [value]="price.id" *ngFor="let price of i.storePrices">
												{{'$'}}{{price.price}} - {{price.storeName}}
											</ion-select-option>
										</ion-select>
									</ng-container>
									<ion-button slot="end" (click)="addStorePrice()">
										<ion-icon slot="icon-only" name="add"></ion-icon>
									</ion-button>
								</ion-item>
								<ion-item *ngIf="price | async as p">
									<ion-label position="stacked">Price</ion-label>
									<ion-input
										class="money"
										type="number"
										[value]="p.price"
										(ionChange)="updatePrice($event.detail.value)"
										(keyup)="handleKey($event)"
									></ion-input>
								</ion-item>
								<app-number-input
									*ngIf="price | async as p"
									[precision]="2"
									[amount]="p.price.toString()"
									(amountEntered)="updatePrice($event)"
									(keyup)="handleKey($event)"
								></app-number-input>


								<ion-item>
									<ion-label position="stacked">Aisle</ion-label>
									<ion-input
										formControlName="aisle"
										type="text"
										(keyup)="handleKey($event)"
									></ion-input>
								</ion-item>

								<ion-item>
									<ion-toggle formControlName="private" name="private" slot="start"></ion-toggle>
									<ion-label for="private">
										Private
										<ng-container *ngIf="isBasic | async">
											{{privateListCount | async}}/10
										</ng-container>
									</ion-label>
								</ion-item>

								<ion-item>
									<ion-label position="stacked">Notes</ion-label>
									<ion-textarea
										auto-grow
										spellcheck
										formControlName="notes"
									></ion-textarea>
								</ion-item>
							</div>
						</ion-col>
					</ion-row>
				</ion-grid>
				<ion-fab vertical="bottom" horizontal="end" slot="fixed">
					<ion-fab-button
						color="secondary"
						[disabled]="itemFG.invalid || itemFG.pristine"
						(click)="save(m)"
					>
						<ion-icon name="checkmark"></ion-icon>
					</ion-fab-button>
				</ion-fab>
			</ion-content>
		</ng-container>
	`,
})
export class SavedItemEditPage implements OnDestroy {
	public itemFG: FormGroup;
	public params: Observable<ItemParams>;
	public item: BehaviorSubject<SavedItem | null>;
	public isEdit: Observable<boolean>;
	public canTogglePrivate: Observable<boolean>;
	public hasStorePrices: Observable<boolean>;
	public price: Observable<StorePrice>;
	public selectedPriceId = new BehaviorSubject('');
	private destroy = new Subject();
	@ViewChild('nameInput', {static: true, read: ElementRef}) nameInput: ElementRef<HTMLIonInputElement> | null = null;

	constructor(
		private savedItemService: SavedItemService,
		private toastService: ToastService,
		private navCtrl: NavController,
		private route: ActivatedRoute,
		private alertCtrl: AlertController,
		private membershipService: MembershipService,
	) {
		recordPageView('Item edit', `/saved-items`);
		this.itemFG = new FormGroup({
			private: new FormControl(false),
			name: new FormControl('', [
				Validators.required,
				minTrimmedLengthValidator(1),
			]),
			brand: new FormControl('', [
				Validators.maxLength(50),
			]),
			aisle: new FormControl('', [
				Validators.maxLength(10),
			]),
			notes: new FormControl('', [
				Validators.maxLength(1000),
			]),
		});

		this.params = this.route.params.pipe(
			switchMap(p => (
				(!Object.keys(p).length && route.firstChild) ?
				route.firstChild.params :
				route.params
			) as Observable<ItemParams>),
		);
		this.item = new BehaviorSubject<SavedItem | null>(null);
		this.params.pipe(
			switchMap(
				params => {
					if(params.id) {
						return this.savedItemService.getItem(params.id);
					} else {
						return of(this.savedItemService.createSavedItem());
					}
				},
			),
			filter(item => !!item),
			tap((item) => {
				if(!item) {
					return;
				}

				this.item.next({
					...item,
					storePrices: item.storePrices ? [...item.storePrices] : [],
				});
				this.setFG(item);
			}),
			takeUntil(this.destroy),
		).subscribe();
		this.hasStorePrices = this.item.pipe(
			map(i => !!(i && i.storePrices && i.storePrices.length)),
		);
		this.isEdit = this.params.pipe(
			map(params => !!(params.id)),
		);
		this.canTogglePrivate = membershipService.subscriptionDoc.pipe(
			switchMap(doc => {
				if(!doc) {
					return of(false);
				}

				return doc.plan === 'basic' ?
					combineLatest([
						this.item,
						savedItemService.privateCount,
					]).pipe(
						map(([list, count]) =>
							count < 10 ||
							!!list.private,
						),
					) :
					of(true);
			}),
		);
		this.price = combineLatest([
			this.item,
			this.selectedPriceId,
			this.itemFG.valueChanges,
		]).pipe(
			map(([
				i,
				selectedPriceId,
			]) => {
				const item = i as SavedItem;
				const defaultPrice = {
					id: '',
					price: item.price,
					storeName: 'General',
				};

				if(!item.storePrices) {
					return defaultPrice;
				}

				const price = getPrice(item, selectedPriceId);

				return price || defaultPrice;
			}),
			takeUntil(this.destroy),
		);
		this.price.subscribe();
		this.canTogglePrivate.pipe(
			tap(canTogglePrivate => {
				const privateControl = this.itemFG.get('private');

				if(!privateControl) {
					return;
				}

				canTogglePrivate ?
					privateControl.enable() :
					privateControl.disable();
			}),
		).subscribe();
	}

	ngOnDestroy() {
		this.destroy.complete();
	}

	get isBasic() {
		return this.membershipService.isBasic;
	}

	get privateListCount() {
		return this.savedItemService.privateCount;
	}

	setSelectedPriceId(newId: string) {
		this.selectedPriceId.next(newId);
	}

	setFG(item: SavedItem) {
		defer(() => {
			this.itemFG.setValue(
				pick(item, [
					'private',
					'name',
					'brand',
					'aisle',
					'notes',
				]),
			);
		});
	}

	updatePrice(price: string) {
		const item = this.item.getValue();

		if(!item) {
			return;
		}

		const {
			storePrices = [],
		} = item;
		const {
			selectedPriceId = '',
		} = this.itemFG.value;
		const updatePrice = getPrice(item, selectedPriceId);

		if(updatePrice && updatePrice.id) {
			updatePrice.price = +price;
			this.item.next({
				...item,
				storePrices,
			});
		} else {
			this.item.next({
				...item,
				price: +price,
			});
		}
		this.itemFG.markAsDirty();
	}

	async handleKey(e: any) {
		if(e.key === Key.Enter) {
			const item = this.item.getValue();

			if(!item) {
				return item;
			}

			await this.save(item);

			this.nameInput && this.nameInput.nativeElement.setFocus();
		} else if(e.key === Key.Escape) {
			this.goBack();
		}
	}

	async save(item: SavedItem) {
		if(!this.itemFG.valid) {
			return;
		}

		const newItem = this.itemFG.getRawValue();

		const saveReq = this.savedItemService.save({
			...item,
			...newItem,
		});

		this.goBack();

		await saveReq;
		this.toastService.show(`"${newItem.name}" saved`);
	}

	async goBack() {
		this.navCtrl.back();
	}


	public removeStorePrice() {
		const item = this.item.getValue();

		if(!item) {
			return;
		}

		const { storePrices = [] } = item;

		if(!storePrices.length) {
			return;
		}

		const selectedPriceId = this.selectedPriceId.getValue();

		this.itemFG.patchValue({
			selectedPriceId: '',
		});
		this.itemFG.markAsDirty();
		this.item.next({
			...item,
			storePrices: storePrices.filter(p => p.id !== selectedPriceId),
		});
	}

	public async addStorePrice() {
		const alert = await this.alertCtrl.create({
			header: 'Add Store Price',
			inputs: [
				{
					name: 'price',
					type: 'number',
					placeholder: 'Price',
				}, {
					name: 'storeName',
					placeholder: 'Name',
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: () => {
						console.log('Cancel clicked');
					},
				},
				{
					text: 'Save',
					handler: data => {
						const newPrice: StorePrice = {
							id: uuid(),
							...data,
						};

						const item = this.item.getValue();

						if(!item) {
							return;
						}
						this.selectedPriceId.next(newPrice.id);
						this.item.next({
							...item,
							storePrices: [
								...item.storePrices || [],
								newPrice,
							],
						});
					},
				},
			],
		});
		alert.present();
	}
}
