import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SavedItemEditPage } from './saved-item-edit.page';
import { CommonComponentsModule } from '../../components/common-components.module';

const routes: Routes = [
	{
		path: '',
		component: SavedItemEditPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		CommonComponentsModule,
	],
	declarations: [
		SavedItemEditPage,
	],
})
export class SavedItemEditPageModule {}
