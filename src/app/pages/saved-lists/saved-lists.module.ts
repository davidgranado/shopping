import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SavedListsPage } from './saved-lists.page';

const routes: Routes = [
	{
		path: '',
		component: SavedListsPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
	],
	exports: [
	],
	declarations: [
		SavedListsPage,
	],
})
export class SavedListsPageModule {}
