import { Component, ViewChild } from '@angular/core';
import { AlertController, ActionSheetController, LoadingController, NavController, IonSelect } from '@ionic/angular';
import { ShoppingListItem, SavedList } from '../../interfaces';
import { ToastService } from '../../services/toast.service';
import { recordPageView, getPrice } from '../../utils';
import { MembershipService } from '../../services/membership.service';
import { SavedListService } from '../../services/saved-list.service';
import { map, startWith } from 'rxjs/operators';
import { Observable, combineLatest } from 'rxjs';
import { FormControl } from '@angular/forms';

const SortFns: any = {
	dateAsc(a: SavedList, b: SavedList) {
		// tslint:disable-next-line: no-non-null-assertion
		return a.created!.toDate().getTime() - b.created!.toDate().getTime();
	},
	dateDsc(a: SavedList, b: SavedList) {
		// tslint:disable-next-line: no-non-null-assertion
		return b.created!.toDate().getTime() - a.created!.toDate().getTime();
	},
	nameAsc(a: SavedList, b: SavedList) {
		return (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1;
	},
	nameDsc(a: SavedList, b: SavedList) {
		return (a.name.toLowerCase() < b.name.toLowerCase()) ? 1 : -1;
	},
};

@Component({
	selector: 'app-saved-lists',
	styleUrls: ['./saved-lists.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-title>
					<div class="image-logo-icon"></div> Saved Lists
				</ion-title>
				<ion-buttons slot="end">
					<ng-container *ngIf="isBasic">
						<ion-button [routerLink]="['/membership']">
							<em><small>{{privateListCount | async}}/5 Private Lists</small></em>
							<ion-icon size="small" color="light" name="help-circle-outline"></ion-icon>
						</ion-button>
					</ng-container>
					<ng-container *ngIf="!isBasic">
						<em><small>Pro</small></em>
						<ion-icon size="small" color="secondary" name="checkmark-circle"></ion-icon>
					</ng-container>
					<ion-button (click)="openSortSelect()">
						<ion-icon name="funnel"></ion-icon>
					</ion-button>
				</ion-buttons>
				<ion-item hidden>
					<ion-label>
						Sort
					</ion-label>
					<ion-select
						#sortOption
						okText="Okay"
						[formControl]="selectControl"
					>
						<ion-select-option value="dateDsc">Date (New-Old)</ion-select-option>
						<ion-select-option value="dateAsc">Date (Old-New)</ion-select-option>
						<ion-select-option value="nameAsc">Name (A-Z)</ion-select-option>
						<ion-select-option value="nameDsc">Name (Z-A)</ion-select-option>
					</ion-select>
				</ion-item>
			</ion-toolbar>
		</ion-header>

		<ion-content [ngClass]="{'ion-padding': !(hasLists | async)}">
			<div class="foo"></div>
			<p class="empty-list-message" *ngIf="!(hasLists | async)">
				<em>Create saved lists for those items you frequently
				purchase, such as weekly groceries, your favorite recipes, or
				even your travel item checklist.</em>
			</p>
			<ion-grid fixed>
				<ion-row *ngIf="savedLists | async as lists">
					<ion-col
						size-xs="12"
						size-sm="6"
						size-md="4"
						size-lg="3"
						*ngFor="let savedList of lists; trackBy: trackData"
					>
						<ion-card
							class="list-card ion-activatable hammerjs-press-scroll-hack"
							(click)="editSavedList(savedList.id)"
							(press)="openActionSheet(savedList)"
						>
							<ion-ripple-effect></ion-ripple-effect>
							<ion-button
								class="list-delete"
								fill="clear"
								size="small"
								(click)="handleDelete(savedList, $event)"
							>
								<ion-icon slot="icon-only" name="close"></ion-icon>
							</ion-button>
							<ion-card-header>
								<ion-card-title>
									{{ savedList.name }}
								</ion-card-title>
							</ion-card-header>
							<ion-item lines="none">
								<ion-label>
									<p *ngIf="savedList.items?.length">
										{{ savedList.items.length }} items @ {{ totalCost(savedList.items) | currency }}
									</p>
									<p *ngIf="savedList.private">
										<ion-icon name="eye-off"></ion-icon>
									</p>
									<p>
										Created: {{savedList.created.toDate() | date}}
									</p>
								</ion-label>
							</ion-item>
							<ion-button
								expand="full"
								class="list-actions"
								(click)="openActionSheet(savedList, $event)"
							>
								More <ion-icon name="ellipsis-vertical-outline"></ion-icon>
							</ion-button>
						</ion-card>
					</ion-col>
				</ion-row>
			</ion-grid>
			<ion-fab vertical="bottom" horizontal="end" slot="fixed">
				<ion-fab-button
					mini
					(click)="openCreateList()"
					color="secondary"
				>
					<ion-icon name="add"></ion-icon>
				</ion-fab-button>
			</ion-fab>
		</ion-content>
	`,
})
export class SavedListsPage {
	@ViewChild('sortOption', {static: false}) sortOption: IonSelect | null = null;
	selectControl = new FormControl('dateDsc');
	savedLists: Observable<SavedList[]>;

	constructor(
		private alertCtrl: AlertController,
		private actionSheetCtrl: ActionSheetController,
		private savedListService: SavedListService,
		private toastService: ToastService,
		private loadingCtrl: LoadingController,
		private navCtrl: NavController,
		private membershipService: MembershipService,
	) {
		recordPageView('Saved Lists', '/tabs/saved-lists');

		this.savedLists = combineLatest([
				this.savedListService.lists,
				this.selectControl.valueChanges.pipe(
					// ???: Not sure if misunderstanding or bug. But initial
					// value doesn't flow through. Only detects subsequent
					// changes
					startWith(this.selectControl.value),
				),
			]).pipe(
				map(
					([lists, sortValue]) =>
						lists.sort(SortFns[sortValue]),
				),
			);
	}

	get isBasic() {
		return this.membershipService.isBasic;
	}

	get privateListCount() {
		return this.savedListService.privateCount;
	}

	editSavedList(id: string) {
		this.navCtrl.navigateForward(`/saved-list/${id}`);
	}

	totalCost(items: ShoppingListItem[]) {
		return items
			.reduce((total, item) => total + getPrice(item).price,
			0,
		) / 100;
	}

	get hasLists() {
		return this.savedListService.hasLists;
	}

	openSortSelect() {
		this.sortOption && this.sortOption.open();
	}

	async onCreate(name: string) {
		const loader = await this.loadingCtrl.create({});
		await loader.present();
		await this.savedListService.save({
			name,
			items: [],
			itemOrder: [],
		});
		await loader.dismiss();
	}

	delete(shoppingList: SavedList) {
		if(!shoppingList.id) {
			return;
		}

		this.savedListService.delete(shoppingList.id);
		this.toastService.show(`"${shoppingList.name}" deleted`);
	}

	handleDelete(savedList: SavedList, e: Event) {
		e.stopPropagation();
		this.confirmDelete(savedList);
	}

	async confirmDelete(savedList: SavedList) {
		const alert = await this.alertCtrl.create({
			message: `Are you sure you want to delete ${savedList.name}?`,
			header: `Delete ${savedList.name}?`,
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
				}, {
					text: 'Yes',
					cssClass: 'primary',
					handler: () => this.delete(savedList),
				},
			],
		});
		alert.present();
	}

	async openActionSheet(savedList: SavedList, e?: Event) {
		e && e.stopPropagation();
		const buttons = [{
			handler: () => {
				savedList.id && this.editSavedList(savedList.id);
			},
			icon: 'create',
			text: 'Edit',
		}, {
			handler: () => this.duplicate(savedList),
			icon: 'copy',
			role: '',
			text: 'Duplicate',
		}, {
			handler: () => this.delete(savedList),
			icon: 'trash',
			role: 'destructive',
			text: 'Delete',
		}, {
			handler() {},
			icon: 'close',
			role: 'cancel',
			text: 'Cancel',
		}];

		const actionSheet = await this.actionSheetCtrl.create({
			buttons,
			header: `'${savedList.name}' Actions`,
		});
		await actionSheet.present();
	}

	async duplicate(shoppingList: SavedList) {
		if(!shoppingList.id) {
			return;
		}

		this.savedListService.duplicate(shoppingList);
	}

		// Perf optimization
		trackData(_: number, item: SavedList): any {
			return item.id;
		}

	async openCreateList() {
		const alert = await this.alertCtrl.create({
			buttons: [
				{
					role: 'cancel',
					text: 'Cancel',
				},
				{
					handler: data => this.onCreate(data.name),
					text: 'Create',
				},
			],
			inputs: [
				{
					name: 'name',
					placeholder: 'New List Name',
				},
			],
			header: 'Create Saved List',
		});
		await alert.present();

		alert.getElementsByTagName('input')[0].focus();
	}
}
