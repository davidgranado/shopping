import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { recordPageView } from '../../utils';
import { ShoppingListService as ShoppingListService } from '../../services/shopping-list.service';
import { SavedListService as SavedListService } from '../../services/saved-list.service';

@Component({
	selector: 'app-create-list-modal',
	styleUrls: ['./create-list-modal.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-button (click)="closeModal()">
						<ion-icon name="close"></ion-icon>
						<span class="ios-only">Close</span>
					</ion-button>
				</ion-buttons>
				<ion-title>Create Shopping List</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content class="ion-padding" *ngIf="savedLists | async as lists">
			<ion-item>
				<ion-input
					floating
					autofocus
					type="text"
					placeholder="Name"
					[(ngModel)]="name"
				></ion-input>
			</ion-item>
			<ion-button
				expand="full"
				[disabled]="!isValidName"
				(click)="createList()"
			>
				Create
			</ion-button>
			<p>
				Add saved lists
			</p>
			<p class="empty-list-message" *ngIf="!lists.length">
				<em>
					You don't have any saved lists.<br>
					<br>
					Create new saved lists to quickly add whole recipes, your usual grocery items, and more.
				</em>
			</p>
			<ion-list>
				<ion-item button *ngFor="let savedList of lists">
					<ion-label>{{savedList.name}}</ion-label>
						<ion-checkbox
							color="dark"
							[(ngModel)]="savedListIds[savedList.id]"
						></ion-checkbox>
				</ion-item>
			</ion-list>
		</ion-content>

	`,
})
export class CreateListModalPage {
	public name = '';
	public savedListIds: {[id: string]: boolean} = {};

	constructor(
		private savedListService: SavedListService,
		private shoppingListService: ShoppingListService,
		private modalCtrl: ModalController,
	) {
		recordPageView('Create List Modal', '/modal/create-list');
	}

	get savedLists() {
		return this.savedListService.lists;
	}

	get isValidName() {
		return this.name.trim();
	}

	get selectedListIds() {
		return Object
			.keys(this.savedListIds)
			.filter(listId => this.savedListIds[listId]);
	}

	async createList() {
		await this.shoppingListService.saveNewList(this.name.trim(), this.selectedListIds);

		this.closeModal();
	}

	async closeModal() {
		const modal = await this.modalCtrl.getTop();
		modal && await modal.dismiss();
	}
}
