import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavParams, NavController, ModalController, LoadingController } from '@ionic/angular';
import { recordPageView } from '../../utils';
import { ToastService } from '../../services/toast.service';
import { Key } from 'ts-key-enum';
import { ShoppingListService } from '../../services/shopping-list.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { ShoppingList, EditableBy } from '../../interfaces';

@Component({
	selector: 'app-user-share-modal',
	styleUrls: ['./user-share-modal.page.scss'],
	template: `
		<ion-header>
			<ion-toolbar color="primary">
				<ion-buttons slot="start">
					<ion-button (click)="closeModal()">
						<ion-icon name="close"></ion-icon>
						<span class="ios-only">Close</span>
					</ion-button>
				</ion-buttons>
				<ion-title>
					Share
				</ion-title>
			</ion-toolbar>
		</ion-header>
		<ion-content class="ion-padding" *ngIf="shoppingList | async as list">
			<ion-item>
				<ion-label>Share with</ion-label>
				<ion-select
					class="ion-select-wide"
					placeholder="Select One"
					[(ngModel)]="list!.public"
					(ionChange)="changeShareType($event, list)"
				>
					<ion-select-option [value]="false">Users I Specify</ion-select-option>
					<ion-select-option [value]="true">Link</ion-select-option>
				</ion-select>
			</ion-item>
			<ion-item>
				<ion-label>Editable By</ion-label>
				<ion-select
					*ngIf="list?.public"
					class="ion-select-wide"
					[(ngModel)]="list!.editableBy"
					(ionChange)="changeEditableType($event, list)"
				>
					<ion-select-option [value]="EditableBy.NoOne">No one</ion-select-option>
					<ion-select-option [value]="EditableBy.Anyone">Anyone with a link</ion-select-option>
					<ion-select-option [value]="EditableBy.SpecifiedUsers">Specified users</ion-select-option>
				</ion-select>
				<ion-select
					*ngIf="!list?.public"
					class="ion-select-wide"
					[(ngModel)]="list!.editableBy"
					(ionChange)="changeEditableType($event, list)"
				>
					<ion-select-option [value]="EditableBy.NoOne">No one</ion-select-option>
					<ion-select-option [value]="EditableBy.SpecifiedUsers">Specified users</ion-select-option>
				</ion-select>
			</ion-item>
			<ion-button (click)="share(list)" color="primary" expand="full">
				Share Link to "{{list!.name}}"
			</ion-button>
			<p>
				Selected users will see the list in their view.
			</p>
			<ng-container>
				<p>
					<ion-input
						#input
						clearInput
						type="text"
						autocomplete="on"
						placeholder="Add username to share with"
						[(ngModel)]="newUsername"
						(keyup)="submitUser($event, list)"
					></ion-input>
				</p>
				<ion-button expand="block" (click)="addUser(list)">
					Add user
				</ion-button>
				<ion-list *ngIf="sharedUsers | async as users">
					<ion-item
						*ngFor="let user of users; trackBy: trackData"
						(click)="removeUser(user.id, list)"
					>
						<ion-label>
							{{user.name}}
						</ion-label>
						<ion-icon slot="end" name="close" item-end></ion-icon>
					</ion-item>
					<ion-item *ngIf="!users.length">
						Not currently shared
					</ion-item>
				</ion-list>
			</ng-container>
		</ion-content>
	`,
})
export class UserShareModalPage {
	public shoppingList: Observable<ShoppingList | null>;
	public sharedUsers: Observable<{
		id: string;
		name: any;
	}[]>;
	public newUsername = '';
	public EditableBy = EditableBy;
	@ViewChild('input', {static: false}) inputField: ElementRef | null = null;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private modalCtrl: ModalController,
		private shoppingListService: ShoppingListService,
		private toastService: ToastService,
		private userService: UserService,
		private loadingCtrl: LoadingController,
	) {
		recordPageView('User Share Modal', '/modal/user-share');

		const listId = this.navParams.get('listId');

		this.shoppingList = this.shoppingListService.getList(listId).pipe(
			map(list => {
				if(!list) {
					return list;
				}

				list.public = !!list.public;
				list.editableBy = list.editableBy || EditableBy.NoOne;
				return list;
			}),
		);

		this.sharedUsers = this.shoppingList.pipe(
			map(list => {
				if(!(list && list.shared)) {
					return [];
				}

				const shared = list.shared;

				return Object
					.keys(shared)
					.map(id => ({
						id,
						name: shared[id].username,
					}));
			}),
		);
	}

	ionViewDidEnter() {
		if(this.inputField) {
			setTimeout(() => {
				(this.inputField as any).setFocus();
			}, 175);
		}
	}

	async addUser(list: ShoppingList) {
		if(!(this.newUsername && list.shared)) {
			return;
		}

		const loading = await this.loadingCtrl.create({});
		await loading.present();

		const userDoc = await this.userService.getUserDoc(this.newUsername);

		if(!userDoc.exists) {
			await this.toastService.show(`User "${this.newUsername}" does not exist.`);
		} else {
			const user = userDoc.data();

			if(!user) {
				return;
			}

			list.shared[user.ownerId] = {
				username: user.display,
			};

			this.shoppingListService.save(list);
			this.newUsername = '';
		}
		await loading.dismiss();
	}

	async removeUser(userId: string, list: ShoppingList) {
		list.shared && delete list.shared[userId];

		const loading = await this.loadingCtrl.create({});
		loading.present();
		await this.shoppingListService.save(list);
		await loading.dismiss();
	}

	async changeShareType(e: any, list: ShoppingList) {
		const loading = await this.loadingCtrl.create({});
		loading.present();

		list.public = e.detail.value;

		if(!list.public && list.editableBy === EditableBy.Anyone) {
			list.editableBy = EditableBy.SpecifiedUsers;
		}

		await this.shoppingListService.save(list);
		await loading.dismiss();
	}

	async changeEditableType(e: any, list: ShoppingList) {
		const loading = await this.loadingCtrl.create({});
		loading.present();
		list.editableBy = e.detail.value;
		await this.shoppingListService.save(list);
		await loading.dismiss();
	}

	async submitUser(e: KeyboardEvent, list: ShoppingList) {
		if(e.key === Key.Enter) {
			this.addUser(list);
		}
	}

	async share(list: ShoppingList) {
		const {
			navigator,
		}: any = window;

		if(!this.shoppingList) {
			return;
		}

		if(!navigator.share) {
			await navigator.clipboard.writeText(`${location.origin}/shopping-list/${list.id}`);
			await this.toastService.show(`Copied url to "${list.name}"!`);
		} else {
			await navigator.share({
				title: `ShopLystr List: '${list.name}'`,
				text: 'Check out this shopping list!',
				url: location.href,
			});

			await this.toastService.show(`'${list.name}' shared.`);
		}
	}

	async closeModal() {
		const modal = await this.modalCtrl.getTop();
		modal && modal.dismiss();
	}

	// Perf optimization
	trackData(_: number, item: any): any {
		return item.id;
	}
}
