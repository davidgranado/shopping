import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
	{
		path: 'tabs',
		component: TabsPage,
		children: [
			{
				path: 'home',
				children: [
					{
						path: '',
						loadChildren: async () =>
							(await import('../pages/shopping-lists/shopping-lists.module')).ShoppingListsPageModule,
					},
				],
			},
			{
				path: 'saved-lists',
				children: [
					{
						path: '',
						loadChildren: async () =>
							(await import('../pages/saved-lists/saved-lists.module')).SavedListsPageModule,
					},
				],
			},
			{
				path: 'saved-items',
				children: [
					{
						path: '',
						loadChildren: async () =>
							(await import('../pages/saved-items/saved-items.module')).SavedItemsPageModule,
					},
				],
			},
			{
				path: 'settings',
				children: [
					{
						path: '',
						loadChildren: async () =>
							(await import('../pages/settings/settings.module')).SettingsPageModule,
					},
				],
			},
			{
				path: 'register',
				redirectTo: '/register',
			},
			{
				path: '',
				redirectTo: '/tabs/home',
				pathMatch: 'full',
			},
		],
	},
	{
		path: '',
		redirectTo: '/tabs/home',
		pathMatch: 'full',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TabsPageRoutingModule {}
