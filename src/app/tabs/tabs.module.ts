import { IonicModule } from '@ionic/angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { AboutPageModule } from '../pages/about/about.module';
import { ShoppingListsPageModule } from '../pages/shopping-lists/shopping-lists.module';
import { SavedListsPageModule } from '../pages/saved-lists/saved-lists.module';

@NgModule({
	imports: [
		IonicModule,
		CommonModule,
		FormsModule,
		TabsPageRoutingModule,
		AboutPageModule,
		SavedListsPageModule,
		ShoppingListsPageModule,
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	declarations: [TabsPage],
})
export class TabsPageModule {}
