import { Component } from '@angular/core';
import { InstallBannerService } from '../services/install-banner.service';
import { UserService } from '../services/user.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-tabs',
	styleUrls: ['tabs.page.scss'],
	template: `
		<ion-tabs>

			<ion-tab-bar slot="bottom">

				<ion-tab-button tab="home">
					<ion-icon name="list-outline"></ion-icon>
					<ion-label>Lists</ion-label>
				</ion-tab-button>

				<ion-tab-button tab="saved-lists">
					<ion-icon name="bookmark"></ion-icon>
					<ion-label>Saved Lists</ion-label>
				</ion-tab-button>

				<ion-tab-button tab="saved-items">
					<ion-icon name="file-tray-stacked-outline"></ion-icon>
					<ion-label>Saved Items</ion-label>
				</ion-tab-button>

				<ion-tab-button tab="settings">
					<ion-icon name="settings"></ion-icon>
					<ion-label>Settings</ion-label>
				</ion-tab-button>

				<ion-tab-button [routerLink]="['/register']" *ngIf="hasNoAccount | async">
					<ion-icon name="person"></ion-icon>
					<ion-label>Create Account</ion-label>
				</ion-tab-button>

				<ion-tab-button *ngIf="canInstall" (click)="install()">
					<ion-icon name="download"></ion-icon>
					<ion-label>Install</ion-label>
				</ion-tab-button>

			</ion-tab-bar>

		</ion-tabs>
	`,
})
export class TabsPage {
	public hasNoAccount: Observable<boolean>;

	constructor(
		userService: UserService,
		private installBannerService: InstallBannerService,
	) {
		this.hasNoAccount = userService.user.pipe(
			map(u => !u || u.isAnonymous),
		);
	}

	get canInstall() {
		return this.installBannerService.hasInstallPrompt;
	}

	install() {
		return this.installBannerService.install();
	}
}
