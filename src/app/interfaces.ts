import { firestore } from 'firebase';

export
interface DoneMap {
	[itemId: string]: boolean;
}

export
enum EditableBy {
	SpecifiedUsers = 'users',
	Anyone = 'anyone',
	NoOne = '',
}

export
interface FirebaseConfig {
	isDev: boolean;
	prod: FirebaseConfigProps;
	dev: FirebaseConfigProps;
}

interface FirebaseConfigProps {
	stripe: StripeBaseProps;
}

export
type ProPlan = 'pro-monthly' | 'pro-annual';

export
type Plan = 'basic' | ProPlan;

export
const ProPlanCost = {
	'pro-monthly': 99,
	'pro-annual': 899,
};

export
interface StorePrice {
	id: string;
	storeName: string;
	price: number;
}

export
interface CommonItemProps {
	aisle: string;
	brand: string;
	id?: string;
	name: string;
	notes: string;
	price: number;
	storePrices?: StorePrice[];
}

export
interface SavedItem extends CommonItemProps {
	ownerId: string;
	private: boolean;
	created?: firestore.Timestamp;
}

export
interface ShoppingListItem extends CommonItemProps {
	parentId?: string;
	quantity: number;
	selectedPriceId?: string;
}

export
interface SavedList {
	id?: string;
	created?: firestore.Timestamp;
	itemOrder: string[];
	items: ShoppingListItem[];
	name: string;
	notes: string;
	ownerUsername: string;
	ownerId: string;
	private?: boolean;
}

// Saved lists are public by default and need to be
// explicitly made "private" while shopping lists are
// private by default and need to be explicitly set
// to "public"
type SavedListWithoutPrivate = Omit<SavedList, 'private'>;

export
interface SavedShoppingListItem extends ShoppingListItem {
	private: boolean;
}

export
interface ShoppingList extends SavedListWithoutPrivate {
	finishedBy?: string;
	shared?: SharedUserMap;
	finishedItems: DoneMap;
	public?: boolean;
	editableBy?: 'users' | 'anyone' | '';
}

export
interface SharedUserMap {
	[user: string]: {
		username: string;
	};
}

export
interface ShoppingList extends SavedListWithoutPrivate {
	finishedBy?: string;
	shared?: SharedUserMap;
	finishedItems: DoneMap;
	public?: boolean;
	editableBy?: 'users' | 'anyone' | '';
}

interface StripeBaseProps {
	publishable: string;
	secret: string;
	webhook_secret: string;
}

export
interface SubscriptionDoc {
	plan: Plan;
	stripeCustomerId: string | null;
	subscriptionId: string | null;
	periodEnd: firestore.Timestamp | null;
	cancellationDate?: firestore.Timestamp | null;
}
