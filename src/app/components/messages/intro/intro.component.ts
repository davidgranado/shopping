import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { UserService } from '../../../services/user.service';

const MESSAGE_ID = 'beta-intro-1';

@Component({
	selector: 'app-intro',
	template: '',
})
export class IntroComponent implements OnInit {
	public showIntro = false;

	constructor(
		private alertController: AlertController,
		private userService: UserService,
	) { }

	async ngOnInit() {
		const isLoggedIn = this.userService.isLoggedIn;

		if(isLoggedIn) {
			return;
		}

		this.presentIntro();
	}

	async presentIntro() {
		if(this.showIntro || await this.hasViewedIntro()) {
			return;
		}

		this.showIntro = true;

		setTimeout(async () => {
			const alert = await this.alertController.create({
				backdropDismiss: false,
				header: 'Welcome to ShopLystr!',
				message: /*html*/`
					Thank you for participating in the open beta. This is a
					small project with many updates planned, including enhanced desktop
					support and more ways to consolidate lists.
					<p>
						Be sure to let us know your comments and other feedback under
						<strong>Settings&nbsp;>&nbsp;About<strong>
					</p>
				`,
				buttons: [{
					text: 'Let\'s get started!',
					handler: () => this.markIntroViewed(),
				}],
			});
			await alert.present();
		}, 15000);

	}

	async hasViewedIntro() {
		return (this.userService.isLoggedIn) || !!localStorage.getItem(MESSAGE_ID);
	}

	async markIntroViewed() {
		localStorage.setItem(MESSAGE_ID, 'true');
	}

}
