import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { Router, NavigationEnd } from '@angular/router';
import { ToastService } from '../../services/toast.service';
import { environment } from '../../../environments/environment';

const UPDATE_MSG_KEY = 'update-prompt';

@Component({
	selector: 'app-sw-update',
	styleUrls: ['./sw-update.component.scss'],
	template: '',
})
export class SwUpdateComponent implements OnInit {
	public updateAvailable = false;

	constructor(
		private swupdate: SwUpdate,
		private router: Router,
		private toastService: ToastService,
	) { }

	ngOnInit() {
		const v = localStorage.getItem(UPDATE_MSG_KEY);
		if(v) {
			localStorage.removeItem(UPDATE_MSG_KEY);
			this.toastService.show(`ShopLystr updated from ${v} to ${environment.appVersion}`);
		}

		if(!this.swupdate.isEnabled) {
			return;
		}

		this
			.swupdate
			.available
			.subscribe(() => this.registerReloadOnRoute());
		this.swupdate.checkForUpdate();
		setTimeout(() => this.swupdate.checkForUpdate(), 180000); // 3 Min (3 * 60 * 1000)
	}

	async registerReloadOnRoute() {
		// Todo: Figure out how to properly do this the "RXJS way"
		this.router.events.subscribe((e) => {
			if(e instanceof NavigationEnd) {
				localStorage.setItem(UPDATE_MSG_KEY, environment.appVersion);
				window.location.reload(true);
			}
		});
	}

}
