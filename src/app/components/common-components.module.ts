import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotesEditComponent } from './notes-edit/notes-edit.component';
import { UserShareModalPage } from '../pages/user-share-modal/user-share-modal.page';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { NumberInputComponent } from './number-input/number-input.component';

@NgModule({
	imports: [
		FormsModule,
		CommonModule,
		IonicModule,
		ReactiveFormsModule,
	],
	declarations: [
		NotesEditComponent,
		UserShareModalPage,
		RegistrationFormComponent,
		NumberInputComponent,
	],
	exports: [
		NotesEditComponent,
		RegistrationFormComponent,
		NumberInputComponent,
	],
	entryComponents: [
		UserShareModalPage,
	],
})
export class CommonComponentsModule { }
