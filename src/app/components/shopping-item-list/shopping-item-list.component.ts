import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ShoppingListItem, DoneMap, StorePrice } from '../../interfaces';
import { ActionSheetController, AlertController, IonSelect } from '@ionic/angular';
import { getPrice, pick, dedupe } from '../../utils';

interface StoreLabel {
	id: string;
	storeName: string;
}

export
interface ListReorder {
	from: number;
	to: number;
	complete(arr?: any[]): any;
}

@Component({
	selector: 'app-shopping-item-list',
	styleUrls: ['./shopping-item-list.component.scss'],
	template: `
		<ion-reorder-group disabled="false" (ionItemReorder)="handleReorder($event)">
			<ng-container *ngIf="stores as s">
				<ion-item *ngIf="s.length > 1">
					<ion-label>
						Set Store
					</ion-label>
					<ion-select
						#store
						selectedText=" "
						interface="popover"
						(ionChange)="setStore($event.detail.value)"
					>
						<ion-select-option
							[value]="price.storeName.toLowerCase()"
							*ngFor="let price of s"
						>
							{{price.storeName}}
						</ion-select-option>
					</ion-select>
				</ion-item>
			</ng-container>
			<ion-item-sliding [disabled]="!canEdit" *ngFor="let item of items; trackBy: trackData">
				<ion-item-options
					side="start"
					(ionSwipe)="handleToggleSwipe($event, item)"
				></ion-item-options>
				<ion-item button>
					<ion-checkbox
						*ngIf="canEdit"
						slot="start"
						class="hide-mobile"
						[checked]="itemIsDone(item)"
						(click)="toggleItemDone(item)"
					></ion-checkbox>
					<ion-label
						class="hammerjs-press-scroll-hack"
						(click)="openItemEdit(item)"
						(press)="openActionSheet(item)"
					>
						<strong *ngIf="!itemIsDone(item)">
							{{item.name}}
						</strong>
						<strong *ngIf="itemIsDone(item)">
							<s>
								{{ item.name }}
							</s>
						</strong>
						<p *ngIf="item.brand">
							Brand: {{ item.brand }}
						</p>
						<p *ngIf="getPrice(item) as p">
							{{ item.quantity }} @ {{ (p.price / 100) | currency }}

							<ng-container *ngIf="item.selectedPriceId">
								({{p.storeName}})
							</ng-container>
						</p>
						<p *ngIf="item.aisle">
							Aisle: {{ item.aisle }}
						</p>
						<p *ngIf="item.notes">
							Notes: {{ item.notes }}
						</p>
					</ion-label>
					<ion-reorder *ngIf="canEdit" slot="end"></ion-reorder>
				</ion-item>
			</ion-item-sliding>
		</ion-reorder-group>
	`,
})
export class ShoppingItemListComponent {
	@Input() listId = '';
	@Input() canEdit = true;
	@ViewChild('store', {static: false}) select: IonSelect | null = null;
	@Output() itemToggleDone = new EventEmitter<ShoppingListItem>();
	@Output() itemDelete = new EventEmitter<ShoppingListItem>();
	@Output() itemEdit = new EventEmitter<string>();
	@Output() reorder = new EventEmitter<ListReorder>();
	@Output() updateStores = new EventEmitter<string>();

	public _shoppingItems: ShoppingListItem[] = [];
	public _finshedItems: DoneMap = {};
	getPrice = getPrice;

	constructor(
		private actionSheetCtrl: ActionSheetController,
		private alertCtrl: AlertController,
	) { }

	handleReorder({detail}: any) {
		if(detail.to === detail.from) {
			return;
		}

		this.reorder.emit(detail);
	}

	handleToggleSwipe(ev: any, item: ShoppingListItem) {
		ev.target.nextSibling.style.transform = 'translate3d(100%, 0, 0)';
		ev.target.parentElement.classList.add('removed-item');

		this.toggleItemDone(item);

		setTimeout(() => {
			ev.target.nextSibling.style.transform = 'translate3d(0, 0, 0)';
			ev.target.parentElement.classList.remove('removed-item');
		}, 350);
	}

	@Input('items')
	set items(items: ShoppingListItem[]) {
		if(items) {
			this._shoppingItems = items;
		}
	}

	@Input('finishedItems')
	set finishedItems(finishedItems: DoneMap) {
		if(finishedItems) {
			this._finshedItems = finishedItems;
		}
	}

	get items() {
		return this._shoppingItems;
			// TODO Figure this out
			// .filter(item => !item.done)
			// .sort((a, b) => a.name.localeCompare(b.name))
			// .concat(
			// 	this
			// 		._shoppingItems
			// 		.filter(item => item.done)
			// 		.sort((a, b) => a.name.localeCompare(b.name)),
			// );
	}

	get finishedItems() {
		return this._finshedItems;
	}

	get stores(): StoreLabel[] {
		return dedupe(
			[{
				id: '',
				storeName: 'General',
			}].concat(
				this.items
					.flatMap(i => i.storePrices)
					.filter(price => !!price)
					.map((price) =>
						pick(price as StorePrice, ['id', 'storeName']) as StoreLabel,
					),
			),
			i => i.storeName.toLowerCase(),
		);
	}

	setStore(value: string) {
		if(value === null) {
			return;
		}

		this.select && (this.select.value = null);
		this.updateStores.emit(value);
	}

	toggleItemDone(item: ShoppingListItem) {
		setTimeout(() => {
			this.itemToggleDone.emit(item);
		}, 300);
	}

	deleteItem(item: ShoppingListItem) {
		this.itemDelete.emit(item);
	}

	async openItemEdit(item: ShoppingListItem) {
		if(this.canEdit) {
			this.itemEdit.emit(item.id);
		} else if(item.notes) {
			const alert = await this.alertCtrl.create({
				cssClass: 'big-alert',
				buttons: [
					{
						role: 'close',
						text: 'Close',
					},
				],
				message: item.notes,
				header: `'${item.name}' Notes`,
			});
			await alert.present();
		}
	}

	// Perf optimization
	trackData(_: number, item: ShoppingListItem): any {
		return item.id;
	}

	itemIsDone(item: ShoppingListItem) {
		return !!(item.id && this._finshedItems[item.id]);
	}

	async openActionSheet(item: ShoppingListItem) {
		if(!this.canEdit) {
			return;
		}

		const isDone = this.itemIsDone(item);

		const actionSheet = await this.actionSheetCtrl.create({
			backdropDismiss: false,
			buttons: [{
				handler: () => this.toggleItemDone(item),
				icon: isDone ? 'arrow-undo-outline' : 'checkmark',
				role: '',
				text: isDone ? 'Undo' : 'Done',
			}, {
				handler: () => this.openItemEdit(item),
				icon: 'create',
				role: '',
				text: 'Edit',
			}, {
				handler: () => this.deleteItem(item),
				icon: 'trash',
				role: 'destructive',
				text: 'Delete',
			}, {
				handler() {},
				icon: 'close',
				role: 'cancel',
				text: 'Cancel',
			}],
			header: `'${item.name}' Actions`,
		});
		actionSheet.present();
	}

}
