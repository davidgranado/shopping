import { Component, Input, ElementRef, EventEmitter, Output, ViewChild, OnDestroy } from '@angular/core';
import { AlertController, NavController, ActionSheetController, LoadingController, IonSelect } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { ShoppingListItem, ShoppingList } from '../../interfaces';
import { ListReorder } from '../shopping-item-list/shopping-item-list.component';
import { move, uuid, getPrice } from '../../utils';
import { UserService } from '../../services/user.service';
import { ShoppingListService } from '../../services/shopping-list.service';
import { SavedListService } from '../../services/saved-list.service';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { filter, tap, takeUntil } from 'rxjs/operators';

interface ItemDone {
	id: string;
	done: boolean;
}

const SortFns: any = {
	priceAsc(a: ShoppingListItem, b: ShoppingListItem) {
		return getPrice(a).price - getPrice(b).price;
	},
	priceDsc(a: ShoppingListItem, b: ShoppingListItem) {
		return getPrice(b).price - getPrice(a).price;
	},
	nameAsc(a: ShoppingListItem, b: ShoppingListItem) {
		return (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1;
	},
	nameDsc(a: ShoppingListItem, b: ShoppingListItem) {
		return (a.name.toLowerCase() < b.name.toLowerCase()) ? 1 : -1;
	},
	doneTop(a: ItemDone, b: ItemDone) {
		return b.done ? 1 : -1;
	},
	doneLast(a: ItemDone, b: ItemDone) {
		return a.done ? 1 : -1;
	},
};

@Component({
	selector: 'app-shopping-list-edit-form',
	styleUrls: ['./shopping-list-edit-form.component.scss'],
	template: `
		<div class="app-list-layout-container">
			<ng-container *ngIf="canEdit">
				<div>
					<ion-item
						*ngIf="!editingName"
						(click)="openNameEdit()"
					>
						<h1>{{list.name}}</h1>
					</ion-item>
					<ion-item *ngIf="editingName">
						<ion-input
							#listName
							floating
							autofocus
							type="text"
							placeholder="Shopping List Name"
							[value]="list.name"
						></ion-input>
					</ion-item>
					<div *ngIf="editingName">
						<ion-button
							expand="full"
							icon-right
							(click)="closeNameEdit()"
							color="secondary"
						>
							Cancel
							<ion-icon name="close"></ion-icon>
						</ion-button>
						<ion-button
							expand="full"
							icon-right
							[disabled]="!isValid"
							(click)="updateName()"
						>
							Save
							<ion-icon name="checkmark"></ion-icon>
						</ion-button>
					</div>
					<ion-item button (click)="toggleFinishList(!isFinished)">
						<ion-label>
							<span>Finished</span>
						</ion-label>
						<ion-checkbox slot="start" [checked]="!!isFinished"></ion-checkbox>
					</ion-item>
					<app-notes-edit
						[canEdit]="true"
						name="{{name}}"
						[notes]="notes"
						(update)="handleNoteUpdate($event)"
					></app-notes-edit>
					<ion-grid>
						<ion-row>
							<ion-col size="10">
								<ion-item>
									<ion-label>
										Total: {{totalDoneCost | currency}} / {{totalCost | currency}}
									</ion-label>
								</ion-item>
							</ion-col>
							<ion-col size="2">
								<ion-button expand="full" fill="clear" (click)="openSortSelect()">
									<ion-icon slot="icon-only" name="funnel"></ion-icon>
								</ion-button>
								<ion-item hidden>
									<ion-label>
										Sort
									</ion-label>
									<ion-select
										#sortOption
										okText="Okay"
										interface="action-sheet"
										[formControl]="selectControl"
									>
										<ion-select-option value="priceDsc">Price (High-Low)</ion-select-option>
										<ion-select-option value="priceAsc">Price (Low-High)</ion-select-option>
										<ion-select-option value="nameAsc">Name (A-Z)</ion-select-option>
										<ion-select-option value="nameDsc">Name (Z-A)</ion-select-option>
										<ion-select-option value="doneLast">Finished Items Last</ion-select-option>
										<ion-select-option value="doneTop">Finished Items First</ion-select-option>
									</ion-select>
								</ion-item>
							</ion-col>
						</ion-row>
					</ion-grid>
				</div>
				<ion-content class="app-list-layout-items">
					<ion-list *ngIf="items">
						<app-shopping-item-list
							*ngIf="items"
							(updateStores)="handleUpdateStores($event)"
							[items]="sortedItems"
							[finishedItems]="list.finishedItems"
							(itemToggleDone)="itemDoneToggled($event)"
							(itemDelete)="deleteItem($event)"
							(itemEdit)="openItemEdit($event)"
							(reorder)="updateOrder($event)"
						></app-shopping-item-list>
					</ion-list>
					<!--spacer for fab -->
					<ion-item lines="none"></ion-item>
					<ion-item lines="none"></ion-item>
					<p class="ion-padding empty-list-message" *ngIf="!items?.length">
						<em>Add the first shopping list item</em>
					</p>
				</ion-content>
			</ng-container>
			<ng-container *ngIf="!canEdit">
				<div>
					<ion-item>
						<h1>{{list.name}}</h1>
					</ion-item>
					<ion-item>
						<ion-label>
							<span *ngIf="!isFinished">Not Finished</span>
							<span *ngIf="isFinished">Finished</span>
						</ion-label>
					</ion-item>
					<app-notes-edit
						[canEdit]="false"
						name="{{name}}"
						[notes]="notes"
						(update)="handleNoteUpdate($event)"
					></app-notes-edit>
					<ion-item>
						<ion-label>
							Total: {{totalDoneCost | currency}} / {{totalCost | currency}}
						</ion-label>
					</ion-item>
				</div>
				<ion-content class="app-list-layout-items">
					<ion-list *ngIf="items">
						<app-shopping-item-list
							*ngIf="items"
							(updateStores)="handleUpdateStores($event)"
							[canEdit]="false"
							[items]="sortedItems"
							[finishedItems]="list.finishedItems"
						></app-shopping-item-list>
					</ion-list>
					<!--spacer for fab -->
					<ion-item lines="none"></ion-item>
					<ion-item lines="none"></ion-item>
				</ion-content>
			</ng-container>
			<div>
				<ion-button
					*ngIf="!isOwner"
					expand="full"
					color="secondary"
					(click)="createFromList()"
				>
					<div class="app-ellipsis">
						Copy/Save "{{name}}"
					</div>
				</ion-button>
			</div>
		</div>
	`,
})
export class ShoppingListEditFormComponent implements OnDestroy {
	@ViewChild('sortOption', {static: false}) sortOption: IonSelect | null = null;
	selectControl = new FormControl();
	destroyed = new Subject();

	@Output() save = new EventEmitter<ShoppingList>();
	@ViewChild('listName', {static: false}) input: ElementRef | null = null;
	@Input() public canEdit = false;
	private _list: ShoppingList;
	public editingName = false;
	getPrice = getPrice;

	constructor(
		private alertCtrl: AlertController,
		public navCtrl: NavController,
		private toastService: ToastService,
		private shoppingListService: ShoppingListService,
		private userService: UserService,
		private savedListService: SavedListService,
		private actionSheetCtrl: ActionSheetController,
		private loadingCtrl: LoadingController,
	) {
		this._list = {
			name: '',
			notes: '',
			ownerUsername: '',
			ownerId: '',
			shared: {},
			items: [],
			itemOrder: [],
			finishedItems: {},
		};

		this.selectControl.valueChanges.pipe(
			filter(val => !!val),
			tap(newVal => {
				this.selectControl.setValue('');

				if(['doneTop', 'doneLast'].includes(newVal)) {
					this._list.itemOrder = this._list
						.items
						.map(i => ({
							id: i.id,
							done: !!this._list.finishedItems[i.id as string],
						}))
						.sort(SortFns[newVal])
						.map(i => i.id as string);
				} else {
					this._list.itemOrder = this._list
						.items
						.sort(SortFns[newVal])
						.map(i => i.id as string);
				}
				this.save.emit(this._list);

			}),
			takeUntil(this.destroyed),
		).subscribe();
	}

	ngOnDestroy() {
		this.destroyed.complete();
	}

	get userId() {
		return this.userService.userId;
	}

	get isOwner() {
		return this._list.ownerId === this.userId;
	}

	get isValid() {
		return !!this._list.name;
	}

	get isPublic() {
		return !!this._list.public;
	}

	set isPublic(isPrivate: boolean) {
		if(this._list.public === !isPrivate) {
			return;
		}

		this._list.public = !isPrivate;
		this.save.emit(this._list);
	}

	get sortedItems() {
		const list = this._list;

		return list.itemOrder
			.map(itemId => (
				list.items.find(item => item.id === itemId)
			))
			// In case some logic is introduced that gets these out of sync.
			.filter(item => item) as ShoppingListItem[];
	}

	get isFinished() {
		return !!this._list.finishedBy;
	}

	get items() {
		return this._list.items;
	}

	get name() {
		return this._list.name;
	}

	get notes() {
		return this._list.notes;
	}

	@Input('shopping-list')
	set list(list: ShoppingList) {
		if(list) {
			this._list = list;
		}
	}

	get list() {
		return this._list;
	}

	get totalCost() {
		if(this._list.items) {
			return this._list.items
				.reduce((total, item) =>
					total + (getPrice(item).price * item.quantity),
					0,
				) / 100;
		} else {
			return 0;
		}
	}

	get totalDoneCost() {
		if(this._list.items) {
			return this._list.items
				.filter(item => this.isDone(item))
				.reduce(
					(total, item) => total + (getPrice(item).price * item.quantity),
				0,
			) / 100;
		}
	}

	openSortSelect() {
		this.sortOption && this.sortOption.open();
	}

	updateOrder(newOrder: ListReorder) {
		move(this._list.itemOrder, newOrder.from, newOrder.to);
		this.shoppingListService.save(this._list);
		newOrder.complete();
	}

	handleUpdateStores(storeName: string) {
		this._list.items.forEach(i => {
			if(!(i.storePrices && i.storePrices.length)) {
				return;
			}

			const price = i.storePrices.find(
					p => p.storeName.toLowerCase() === storeName,
				);

			i.selectedPriceId = price ?
				price.id :
				'';
		});

		this.save.emit(this._list);
	}

	handleFinishToggle({target}: any) {
		const finished = target.checked as boolean;
		this.toggleFinishList(finished);
	}

	handleNoteUpdate(newNotes: string) {
		if(this._list.notes === newNotes) {
			return;
		}

		this._list.notes = newNotes;
		this.save.emit(this._list);
	}

	async toggleFinishList(finished: boolean) {
		if(!this.userService.isLoggedIn) {
			const loader = await this.loadingCtrl.create({});
			await loader.present();
			await this.userService.loginAnonymously();
			await loader.dismiss();
		}

		if(finished) {
			const userDoc = await this.userService.userDocPromise;
			this._list.finishedBy = userDoc ? userDoc.username : '';
		} else {
			this._list.finishedBy = '';
		}

		this.save.emit(this._list);
	}

	async promptToMarkFinished() {
		if(this.isFinished) {
			return;
		}

		const alert = await this.alertCtrl.create({
			buttons: [
				{
					role: 'close',
					text: 'No',
				}, {
					handler: () => {
						this.toggleFinishList(true);
					},
					text: 'Yes',
				},
			],
			message: `Last item purchased! Would you like to mark the list as finished?`,
			header: 'Confirm Completion',
		});
		await alert.present();
	}


	deleteItem(item: ShoppingListItem) {
		if(this._list.id && item.id) {
			this.toastService.show(`"${item.name}" deleted`);
			this.shoppingListService.deleteItem(item.id, this._list.id);
		}
	}

	async openItemEdit(id: string) {
		this.navCtrl.navigateForward(`/shopping-list/${this._list.id}/item/shop/${id}`, {
			animated: false,
		});
	}

	openNameEdit() {
		if(!this.canEdit) {
			return;
		}

		this.editingName = true;
	}

	closeNameEdit() {
		this.editingName = false;
	}

	updateName() {
		if(this.input) {
			this._list.name = (this.input as any).value;
			this.save.emit(this._list);
		}
		this.closeNameEdit();
	}
	itemDoneToggled(item: ShoppingListItem) {
		if(!item.id) {
			return;
		}

		this.list.finishedItems[item.id] = !this.isDone(item);
		const currentIndex = this.list.itemOrder.findIndex(itemId => itemId === item.id);
		const targetIndex = this.list.finishedItems[item.id] ?
			this.list.items.length - 1 :
			0;

		if(currentIndex !== targetIndex) {
			move(this.list.itemOrder, currentIndex, targetIndex);
		}

		if(this.list.id) {
			this.shoppingListService.save(this.list);
		}

		const allDone = this.list.items.every(i => !!this.isDone(i));

		if(allDone) {
			this.promptToMarkFinished();
		}
	}

	isDone(item: ShoppingListItem) {
		return !!item.id && this.list.finishedItems[item.id];
	}

	async createFromList() {
		const actionSheet = await this.actionSheetCtrl.create({
			backdropDismiss: false,
			buttons: [{
				handler: async () => {
					const loader = await this.loadingCtrl.create({});
					await loader.present();
					await this.createListFromSavedList();
					await this.navCtrl.navigateBack(`/tabs/home`);
					await loader.dismiss();
				},
				icon: 'create',
				role: '',
				text: 'Duplicate List',
			}, {
				handler: async () => {
					const loader = await this.loadingCtrl.create({});
					await loader.present();
					await this.copySavedListAsSavedList();
					await this.navCtrl.navigateBack(`/tabs/saved-lists`);
					await loader.dismiss();
				},
				icon: 'bookmark',
				role: '',
				text: 'Save List to Library',
			}, {
				handler: async () => {
					const loader = await this.loadingCtrl.create({});
					await loader.present();
					await this.createListFromSavedList();
					await this.copySavedListAsSavedList();
					await this.navCtrl.navigateBack('/tabs/home');
					await loader.dismiss();
				},
				icon: 'bookmarks',
				role: '',
				text: 'Duplicate & Save to Library',
			}, {
				handler() {},
				icon: 'close',
				role: 'cancel',
				text: 'Cancel',
			}],
			header: `Create List from "${this.name}"`,
		});
		actionSheet.present();
	}

	private async createListFromSavedList() {
		// TODO Refactor this copy n' paste from create-list-modal.page
		const items = this._list.items
			.map(item => ({
				...item,
				id: uuid(),
			}));

		await this.shoppingListService.save({
			notes: this._list.notes,
			name: this.name.trim(),
			items,
			itemOrder: items.map(item => item.id),
		});
	}

	private async copySavedListAsSavedList() {
		const {
			id,
			created,
			ownerId,
			// tslint:disable-next-line:trailing-comma
			...newList
		} = this._list;
		await this.savedListService.save(newList);
	}
}
