import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
	selector: 'app-notes-edit',
	styleUrls: ['./notes-edit.component.scss'],
	template: `
		<ion-item *ngIf="!editingNotes">
		<ion-label  (click)="viewNote()">
			<p>
				{{renderedNote}}
			</p>
		</ion-label>
		<ion-button
			*ngIf="_canEdit"
			slot="end"
			(click)="toggleEditNotes()"
		>
			<ion-icon slot="icon-only" name="create"></ion-icon>
		</ion-button>
	</ion-item>
	<ng-container *ngIf="editingNotes">
		<ion-textarea
			autofocus
			auto-grow
			[(ngModel)]="_notes"
			placeholder="Add list notes"></ion-textarea>
		<ion-button
			expand="full"
			(click)="toggleEditNotes()"
		>
			Save
		</ion-button>
	</ng-container>
	`,
})
export class NotesEditComponent implements OnInit {
	@Output() update = new EventEmitter<string>();
	public editingNotes = false;
	public _notes = '';
	private _name = '';
	public _canEdit = false;

	constructor(
		private alertCtrl: AlertController,
	) { }

	ngOnInit() {}

	get renderedNote() {
		return this._notes || 'Add notes';
	}

	@Input('canEdit')
	set canEdit(canEdit: boolean) {
		this._canEdit = !!canEdit;
	}

	@Input('name')
	set name(name: string) {
		if(name) {
			this._name = name;
		}
	}

	@Input('notes')
	set list(notes: string) {
		if(notes) {
			this._notes = notes;
		}
	}

	toggleEditNotes() {
		if(this.editingNotes) {
			this.update.emit(this._notes.trim());
		}

		this.editingNotes = !this.editingNotes;
	}

	async viewNote() {
		if(!this._notes) {
			return;
		}

		const alert = await this.alertCtrl.create({
			cssClass: 'big-alert',
			buttons: [
				{
					role: 'close',
					text: 'Close',
				},
			],
			message: this._notes,
			header: `'${this._name}' Notes`,
		});
		await alert.present();
	}

}
