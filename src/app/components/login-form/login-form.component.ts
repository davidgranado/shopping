import { Component, OnInit } from '@angular/core';
import { ToastService } from '../../services/toast.service';
import { NavController, LoadingController } from '@ionic/angular';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-login-form',
	styleUrls: ['./login-form.component.scss'],
	template: `
		<ion-item>
			<ion-button color="primary" (click)="navToRegister($event)">Create an Account</ion-button>
			<ion-label position="floating">
				Email Address
			</ion-label>
			<ion-input
				type="email"
				(keyup.enter)="login()"
				[(ngModel)]="email"
			></ion-input>
		</ion-item>

		<ion-item>
			<ion-label position="floating">
				Password
			</ion-label>
			<ion-input
				type="password"
				(keyup.enter)="login()"
				[(ngModel)]="password"
			></ion-input>
		</ion-item>

		<p>
			<ion-button
				expand="full"
				color="primary"
				(click)="login()"
				[disabled]="!email.trim()"
			>
				Login
			</ion-button>
		</p>

		<p>
			<ion-button
				expand="full"
				color="secondary"
				(click)="sendPwReset()"
				[disabled]="!email.trim()"
			>
				Forgot Password
			</ion-button>
		</p>
	`,
})
export class LoginFormComponent implements OnInit {
	email = '';
	password = '';

	constructor(
		private loadingCtrl: LoadingController,
		private navCtrl: NavController,
		private toast: ToastService,
		private userService: UserService,
	) { }

	ngOnInit() {
	}

	async login() {
		const email = this.email.trim();
		const loader = await this.loadingCtrl.create({});
		loader.present();

		try {
			await this.userService.login(email, this.password);
		} catch (e) {
			console.error(e);
			this.toast.show(e.message);
		}

		loader.dismiss();
	}

	async sendPwReset() {
		const email = this.email.trim();
		const loader = await this.loadingCtrl.create({});
		loader.present();

		try {
			await this.userService.sendPasswordResetEmail(email);
			this.toast.show(`Password reset email sent to ${email}`);
		} catch (e) {
			console.error(e);
			this.toast.show(e.message);
		}

		loader.dismiss();
	}

	// Using to prevent animation. Possibly a better way
	// to do this through the markup
	navToRegister(e: MouseEvent) {
		e.preventDefault();
		this.navCtrl.navigateRoot('/register');
	}
}
