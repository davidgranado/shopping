import { Component, EventEmitter, ElementRef, Output, ViewChild, Input } from '@angular/core';
import { NavController, ActionSheetController, AlertController, LoadingController, ModalController, IonSelect } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { uuid, move, getPrice, dedupe, pick } from '../../utils';
import { ListReorder } from '../shopping-item-list/shopping-item-list.component';
import { MembershipService } from '../../services/membership.service';
import { ItemEditPage } from '../../pages/item-edit/item-edit.page';
import { ShoppingListItem, SavedList, StorePrice } from '../../interfaces';
import { ShoppingListService } from '../../services/shopping-list.service';
import { SavedListService } from '../../services/saved-list.service';
import { BehaviorSubject, Observable, of, combineLatest } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';

interface StoreLabel {
	id: string;
	storeName: string;
}

@Component({
	selector: 'app-saved-lists-edit-form',
	styleUrls: ['./saved-lists-edit-form.component.scss'],
	template: `
		<div class="app-list-layout-container" *ngIf="list$ | async as list">
			<div>
				<ion-item
					*ngIf="!editingName"
					(click)="openNameEdit()"
				>
					<h1>{{list.name}}</h1>
				</ion-item>
				<ion-item *ngIf="editingName">
					<ion-input
						#listName
						floating
						autofocus
						type="text"
						placeholder="Saved List Name"
						[value]="list.name"
					></ion-input>
				</ion-item>
				<div *ngIf="editingName">
					<ion-button
						expand="full"
						icon-right
						(click)="closeNameEdit()"
						color="secondary"
					>
						Cancel
						<ion-icon name="close"></ion-icon>
					</ion-button>
					<ion-button
						expand="full"
						icon-right
						[disabled]="!isValid"
						(click)="updateName(list)"
					>
						Save
						<ion-icon name="checkmark"></ion-icon>
					</ion-button>
				</div>
				<app-notes-edit
					name="{{list.name}}"
					[notes]="list.notes"
					(update)="handleNoteUpdate($event, list)"
					[canEdit]="canEdit | async"
				></app-notes-edit>
				<ion-item>
					<ion-label>
						Total Cost: {{totalCost | async | currency}}
					</ion-label>
					<div class="can-toggle-private-toggle" slot="end" *ngIf="canEdit | async">
						<ion-label>
							Private
							<ng-container *ngIf="isBasic | async">
								{{privateCount | async}}/5
							</ng-container>
						</ion-label>
						<ion-toggle
							[disabled]="!(canTogglePrivate | async)"
							[(ngModel)]="list.private"
							(click)="saveList(list)"
						></ion-toggle>
					</div>
				</ion-item>
			</div>
			<ion-content class="app-list-layout-items">
				<ion-list>
					<ng-container *ngIf="stores as s">
						<ion-item *ngIf="s.length > 1">
							<ion-label>
								Set Store
							</ion-label>
							<ion-select
								#store
								selectedText=" "
								interface="popover"
								(ionChange)="setStore($event.detail.value)"
							>
								<ion-select-option
									[value]="price.storeName.toLowerCase()"
									*ngFor="let price of s"
								>
									{{price.storeName}}
								</ion-select-option>
							</ion-select>
						</ion-item>
					</ng-container>
					<ng-container *ngIf="canEdit | async">
						<ion-reorder-group disabled="false" (ionItemReorder)="updateOrder($event, list)">
							<ion-item
								button
								*ngFor="let item of sortedItems | async; trackBy: trackData"
								(click)="openItemEdit(item.id, list.id)"
								(press)="openActionSheet(item, list)"
								class="hammerjs-press-scroll-hack"
							>
								<ion-label>
									<strong>
										{{ item.name }}
									</strong>
									<p *ngIf="item.brand">
										Brand: {{ item.brand }}
									</p>
									<p *ngIf="getPrice(item) as p">
										{{ item.quantity }} @ {{ (p.price / 100) | currency }}

										<ng-container *ngIf="item.selectedPriceId">
											({{p.storeName}})
										</ng-container>
									</p>
									<p *ngIf="item.aisle">
										Aisle: {{ item.aisle }}
									</p>
									<p *ngIf="item.notes">
										Notes: {{ item.notes }}
									</p>
								</ion-label>
								<ion-reorder slot="end"></ion-reorder>
							</ion-item>
						</ion-reorder-group>
					</ng-container>
					<ng-container *ngIf="!(canEdit | async)">
						<ion-item
						*ngFor="let item of sortedItems | async; trackBy: trackData"
						(click)="displayNotes(item)"
						class="hammerjs-press-scroll-hack"
					>
						<ion-label>
							<strong>
								{{ item.name }}
							</strong>
							<p *ngIf="item.brand">
								Brand: {{ item.brand }}
							</p>
							<p *ngIf="getPrice(item) as p">
								{{ item.quantity }} @ {{ (p.price / 100) | currency }}

								<ng-container *ngIf="item.selectedPriceId">
									({{p.storeName}})
								</ng-container>
							</p>
							<p *ngIf="item.aisle">
								Aisle: {{ item.aisle }}
							</p>
							<p *ngIf="item.notes">
								Notes: {{ item.notes }}
							</p>
						</ion-label>
					</ion-item>
					</ng-container>
				</ion-list>
				<!--spacer for fab -->
				<ion-item lines="none"></ion-item>
				<ion-item lines="none"></ion-item>
				<p class="ion-padding empty-list-message" *ngIf="!list.items.length">
					<em>Add the first saved list item</em>
				</p>
			</ion-content>

			<ion-button
				expand="full"
				color="secondary"
				*ngIf="!(canEdit | async)"
				(click)="createFromList(list)"
			>
				<div class="app-ellipsis">
					Create List from "{{list.name}}"
				</div>
			</ion-button>
		</div>
	`,
})
export class SavedListsEditFormComponent {
	@Output() save = new EventEmitter<SavedList>();
	@ViewChild('listName', {static: false}) input: ElementRef | null = null;
	@ViewChild('store', {static: false}) select: IonSelect | null = null;
	@Output() updateStores = new EventEmitter<string>();
	public list$: BehaviorSubject<SavedList>;
	public canEdit: Observable<boolean>;
	public canTogglePrivate: Observable<boolean>;
	public isValid: Observable<boolean>;
	public totalCost: Observable<number>;
	public editingName = false;
	public sortedItems: Observable<ShoppingListItem[]>;

	get isBasic() {
		return this.membershipService.isBasic;
	}

	get privateCount() {
		return this.savedListService.privateCount;
	}

	get stores(): StoreLabel[] {
		return dedupe(
			[{
				id: '',
				storeName: 'General',
			}].concat(
				this.list$.getValue().items
					.flatMap(i => i.storePrices)
					.filter(price => !!price)
					.map((price) =>
						pick(price as StorePrice, ['id', 'storeName']) as StoreLabel,
					),
			),
			i => i.storeName.toLowerCase(),
		);
	}

	getPrice = getPrice;

	constructor(
		private alertCtrl: AlertController,
		private navCtrl: NavController,
		private loadingCtrl: LoadingController,
		private shoppingListService: ShoppingListService,
		private toastService: ToastService,
		private actionSheetCtrl: ActionSheetController,
		private savedListService: SavedListService,
		private membershipService: MembershipService,
		private modalCtrl: ModalController,
		route: ActivatedRoute,
	) {
		route.paramMap.pipe(
			tap(params => {
				params.has('itemType') ?
					this.openCreateItem(route.paramMap) :
					this.closeCreateItem();
			}),
		).subscribe();

		this.list$ = new BehaviorSubject(savedListService.createList());
		this.canEdit = this.list$.pipe(
			map(l => savedListService.canEdit(l)),
		);
		this.isValid = this.list$.pipe(
			map(l => !!l.name),
		);
		this.totalCost = this.list$.pipe(
			map(list =>
				list.items.reduce(
					(total, item) => total + (getPrice(item).price * item.quantity),
					0,
				) / 100,
			),
		);
		this.sortedItems = this.list$.pipe(
			map(l =>
				l.itemOrder.map(
					itemId => l.items.find(
						item => item.id === itemId,
					),
				).filter(
					i => !!i,
				) as ShoppingListItem[],
			),
		);
		this.canTogglePrivate = membershipService.subscriptionDoc.pipe(
			switchMap(doc => {
				if(!doc) {
					return of(false);
				}

				return doc.plan === 'basic' ?
					combineLatest([
						this.list$,
						savedListService.privateCount,
					]).pipe(
						map(([list, count]) =>
							count < 5 ||
							!!list.private,
						),
					) :
					of(true);
			}),
		);
	}

	saveList(list: SavedList) {
		setTimeout(() => this.save.emit(list), 100);
	}

	setStore(value: string) {
		if(value === null) {
			return;
		}

		this.select && (this.select.value = null);
		this.updateStores.emit(value);
	}

	async openCreateItem(params: Observable<ParamMap>) {
		const oldModal = await this.modalCtrl.getTop();

		oldModal && oldModal.dismiss();

		const modal = await this.modalCtrl.create({
			backdropDismiss: false,
			component: ItemEditPage,
			componentProps: {
				params: params.pipe(
					map((paramMap: ParamMap) => ({
						id: paramMap.get('id'),
						itemId: paramMap.get('itemId'),
						itemType: paramMap.get('itemType'),
					})),
				),
			},
		});

		await modal.present();
	}

	async closeCreateItem() {
		const modal = await this.modalCtrl.getTop();

		modal && modal.dismiss();
	}

	setPrivate(isPrivate: boolean, list: SavedList) {
		if(list.private === isPrivate) {
			return;
		}

		list.private = isPrivate;
		this.save.emit(list);
	}

	@Input('saved-list')
	set list(list: SavedList) {
		if(list) {
			this.list$.next(list);
		}
	}

	updateOrder(ev: any, list: SavedList) {
		const {
			complete,
			from,
			to,
		}: ListReorder = ev.detail;

		move(list.itemOrder, from, to);
		this.savedListService.save(list);
		complete();
	}

	handleNoteUpdate(newNotes: string, list: SavedList) {
		if(list.notes === newNotes) {
			return;
		}

		list.notes = newNotes;
		this.save.emit(list);
	}

	openNameEdit() {
		if(!this.canEdit) {
			return;
		}

		this.editingName = true;
	}

	closeNameEdit() {
		this.editingName = false;
	}

	async openItemEdit(id: string, listId: string) {
		this.navCtrl.navigateForward(`/saved-list/${listId}/item/saved/${id}`, {
			animated: false,
		});
	}

	deleteItem(item: SavedList, list: SavedList) {
		if(list.id && item.id) {
			this.toastService.show(`"${item.name}" deleted`);
			this.savedListService.deleteItem(item.id, list.id);
		}
	}

	updateName(list: SavedList) {
		if(this.input) {
			list.name = (this.input as any).value;
			this.save.emit(list);
		}
		this.closeNameEdit();
	}

	async openActionSheet(item: SavedList, list: SavedList) {
		const listId = list.id;

		if(!listId) {
			return;
		}

		const actionSheet = await this.actionSheetCtrl.create({
			backdropDismiss: false,
			buttons: [{
				handler: () => this.deleteItem(item, list),
				icon: 'trash',
				role: 'destructive',
				text: 'Delete',
			}, {
				handler: () => {
					item.id && this.openItemEdit(item.id, listId);
				},
				icon: 'create',
				role: '',
				text: 'Edit',
			}, {
				handler() {},
				icon: 'close',
				role: 'cancel',
				text: 'Cancel',
			}],
			header: `'${item.name}' Actions`,
		});
		actionSheet.present();
	}

	async displayNotes(item: SavedList) {
		if(!item.notes) {
			return;
		}

		const alert = await this.alertCtrl.create({
			cssClass: 'big-alert',
			buttons: [
				{
					role: 'close',
					text: 'Close',
				},
			],
			message: item.notes,
			header: `'${item.name}' Notes`,
		});

		await alert.present();
	}

	async createFromList(list: SavedList) {
		const actionSheet = await this.actionSheetCtrl.create({
			backdropDismiss: false,
			buttons: [{
				handler: async () => {
					const loader = await this.loadingCtrl.create({});
					await loader.present();
					await this.createListFromSavedList(list);
					await this.navCtrl.navigateBack(`/tabs/home`);
					await loader.dismiss();
				},
				icon: 'create',
				role: '',
				text: 'Create List',
			}, {
				handler: async () => {
					const loader = await this.loadingCtrl.create({});
					await loader.present();
					await this.copySavedListAsSavedList(list);
					await this.navCtrl.navigateBack(`/tabs/saved-lists`);
					await loader.dismiss();
				},
				icon: 'bookmark',
				role: '',
				text: 'Save List to Library',
			}, {
				handler: async () => {
					const loader = await this.loadingCtrl.create({});
					await loader.present();
					await this.createListFromSavedList(list);
					await this.copySavedListAsSavedList(list);
					await this.navCtrl.navigateBack('/tabs/home');
					await loader.dismiss();
				},
				icon: 'bookmarks',
				role: '',
				text: 'Create & Save List to Library',
			}, {
				handler() {},
				icon: 'close',
				role: 'cancel',
				text: 'Cancel',
			}],
			header: `Create List from "${list.name}"`,
		});
		actionSheet.present();
	}

	private async createListFromSavedList(list: SavedList) {
		// TODO Refactor this copy n' paste from create-list-modal.page
		const items = list.items
			.map(item => ({
				...item,
				id: uuid(),
			}));

		await this.shoppingListService.save({
			notes: list.notes,
			name: list.name.trim(),
			items,
			itemOrder: items.map(item => item.id),
		});
	}

	private async copySavedListAsSavedList(list: SavedList) {
		const {
			id,
			created,
			ownerId,
			// tslint:disable-next-line:trailing-comma
			...newList
		} = list;
		await this.savedListService.save(newList);
	}

	// Perf optimization
	trackData(_: number, item: ShoppingListItem): any {
		return item.id;
	}
}
