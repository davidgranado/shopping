import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ToastService } from '../../services/toast.service';
import { LoadingController, NavController } from '@ionic/angular';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-registration-form',
	styleUrls: ['./registration-form.component.scss'],
	template: `
		<ion-item>
			<ion-button
				*ngIf="!hideLogin"
				color="primary"
				(click)="navToLogin($event)"
			>
				I already have a ShopLystr Account
			</ion-button>
			<ion-label position="floating">
				Email Address
			</ion-label>
			<ion-input
				type="email"
				(keyup.enter)="register()"
				[(ngModel)]="email"
			></ion-input>
		</ion-item>

		<ion-item>
			<ion-label position="floating">
				Password
			</ion-label>
			<ion-input
				type="password"
				(keyup.enter)="register()"
				[(ngModel)]="password"
			></ion-input>
		</ion-item>

		<p>
			<ion-button
				expand="full"
				color="secondary"
				(click)="register()"
				[disabled]="!hasAgreed"
			>
				Register
			</ion-button>
		</p>
		<hr/>
		<ion-grid>
			<ion-row>
				<ion-col size="1" class="sl-agreement">
					<span>
						<ion-checkbox slot="start" [(ngModel)]="agreedTosPrivacy"></ion-checkbox>
					</span>
				</ion-col>
				<ion-col class="ion-align-self-start">
					<div>
						I agree to the ShopLystr
						<a href="https://shoplystr.com/privacy-policy.html" target="__blank">Privacy Policy</a>
						and <a href="https://shoplystr.com/tos.html" target="__blank">Terms of Service Policy</a>.
					</div>
				</ion-col>
			</ion-row>
			<ion-row>
				<ion-col size="1" class="sl-agreement">
					<span>
						<ion-checkbox slot="start" [(ngModel)]="agreedGdpr"></ion-checkbox>
					</span>
				</ion-col>
				<ion-col class="ion-align-self-start">
					<div>
						I am not a resident or citizen of a country subject to
						<a href="https://shoplystr.com/gdpr.html" target="__blank">GDPR</a>.
					</div>
				</ion-col>
			</ion-row>
		</ion-grid>
	`,
})
export class RegistrationFormComponent implements OnInit {
	@Output() registered = new EventEmitter();
	_hideLogin = false;
	email = '';
	password = '';
	agreedTosPrivacy = false;
	agreedGdpr = false;

	constructor(
		private loadingCtrl: LoadingController,
		private navCtrl: NavController,
		private toast: ToastService,
		private userService: UserService,
	) { }


	ngOnInit() {
	}

	@Input('hideLogin')
	set hideLogin(hideLogin: boolean) {
		this._hideLogin = !!hideLogin;
	}

	get hideLogin() {
		return this._hideLogin;
	}

	get hasAgreed() {
		return this.agreedGdpr && this.agreedTosPrivacy;
	}

	async register() {
		if(!this.hasAgreed) {
			return;
		}

		(await this.loadingCtrl.create({})).present();

		try {
			await this.userService.register(this.email, this.password);
		} catch (e) {
			console.error(e);
			this.toast.show(e.message);
		}

		await this.loadingCtrl.dismiss();
		this.registered.emit();
	}

	// Using to prevent animation. Possibly a better way
	// to do this through the markup
	navToLogin(e: MouseEvent) {
		e.preventDefault();
		this.navCtrl.navigateRoot('/login');
	}
}
