import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { PaymentService } from '../../services/payment.service';
import { PaymentParams } from '../../services/api.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { ToastService } from '../../services/toast.service';
import { UserService } from '../../services/user.service';
import { ProPlan } from '../../interfaces';

@Component({
	selector: 'app-checkout-form',
	styleUrls: ['./checkout-form.component.scss'],
	template: `
		<div *ngIf="step === 0">
			<h2>Create an Account</h2>
			<app-registration-form
				[hideLogin]="true"
				(registered)="nextStep()"
			></app-registration-form>
		</div>
		<div *ngIf="step === 1">
			<h2>Subscriptions</h2>
			<ion-list>
				<ion-radio-group [(ngModel)]="selectedPlan">
					<ion-item>
						<ion-label>Annual @ $8.99 (save 25%)</ion-label>
						<ion-radio slot="start" value="pro-annual"></ion-radio>
					</ion-item>
					<ion-item>
						<ion-label>Monthly @ $.99</ion-label>
						<ion-radio slot="start" value="pro-monthly"></ion-radio>
					</ion-item>
				</ion-radio-group>
			</ion-list>
			<ion-button (click)="nextStep()">Next</ion-button>
		</div>
		<div [hidden]="step !== 2">
			<div class="checkout-info">
				<div class="loader" *ngIf="!(loaded | async)">
					<ion-spinner></ion-spinner>
				</div>
				<ng-container *ngIf="loaded | async">
					<h2>Cart</h2>
					<ng-container *ngIf="selectedPlan === 'pro-annual'">
						<p>
							ShopLystr Pro - Annual: $8.99
						</p>
						<p>
							--------
						</p>
						<p>
							Total: $8.99
						</p>
					</ng-container>
					<ng-container *ngIf="selectedPlan === 'pro-monthly'">
						<p>
							ShopLystr Pro - Monthly: $0.99
						</p>
						<p>
							--------
						</p>
						<p>
							Total: $0.99
						</p>
					</ng-container>
				</ng-container>
			</div>
			<p [hidden]="canUsePaymentReq">
				<ion-card>
					<ion-card-content>
						<div #paymentEl></div>
						<p id="card-errors" role="alert">{{error}}</p>
					</ion-card-content>
				</ion-card>
			</p>
			<ion-grid>
				<ion-row>
					<ion-col>
						<p>
							<ion-button
								expand="block"
								color="secondary"
								(click)="prevStep()"
							>
								Back
							</ion-button>
						</p>
					</ion-col>
					<ion-col>
						<p #payReqBtn></p>
						<p [hidden]="canUsePaymentReq">
							<ion-button
								[hidden]="canUsePaymentReq"
								expand="block"
								color="primary"
								(click)="submit()"
							>
								Pay Now
							</ion-button>
						</p>
					</ion-col>
				</ion-row>
			</ion-grid>
		</div>
	`,
})
export class CheckoutFormComponent implements OnInit {
	@ViewChild('payReqBtn', {static: false}) payReqBtn: ElementRef | null = null;
	@ViewChild('paymentEl', {static: false}) paymentEl: ElementRef | null = null;
	email = '';
	error = '';
	canUsePaymentReq = false;
	step = 1;
	selectedPlan: ProPlan = 'pro-annual';
	loaded = this.paymentService.loaded;

	foo: any = null;

	constructor(
		private paymentService: PaymentService,
		private userService: UserService,
		private loadingCtrl: LoadingController,
		private modalCtrl: ModalController,
		private toastService: ToastService,
	) {
	}

	async ngOnInit() {
		const userDoc = await this.userService.userDocPromise;
		if(!(userDoc && userDoc.email)) {
			this.step = 0;
		} else {
			this.email = userDoc.email;
		}
	}

	async displayCheckout() {
		this.loadingCtrl.create({});
		const elements = this.paymentService.elementsInstance;
		const paymentRequest = await this.paymentService.getPaymentRequest(this.selectedPlan);
		this.canUsePaymentReq = !!(await paymentRequest.canMakePayment());
		let prButton;

		if(this.canUsePaymentReq && this.payReqBtn) {
			prButton = elements.create('paymentRequestButton', {
				paymentRequest,
			});
			prButton.mount(this.payReqBtn.nativeElement);

			paymentRequest.on('source', async (ev: any) => {
				const loading =  await this.loadingCtrl.create({
					message: 'Processing payment...',
				});
				await loading.present();
				await this.payment(ev.source.id, ev.complete);
				await loading.dismiss();
			});
		} else if(this.paymentEl) {
			// Create an instance of the card Element.
			const card = elements.create('card');
			this.foo = card;
			card.mount(this.paymentEl.nativeElement, {
				style: {
					base: {
						fontSize: '16px',
						color: '#32325d',
					},
				},
			});
			card.addEventListener('change', (e: any) => {
				this.error = e.error ?
					e.error.message :
					'';
			});
		}
	}

	private async payment(sourceId: string, complete?: (message: 'success' | 'fail') => void) {
		try {
			const data: PaymentParams = {
				source: sourceId,
				plan: this.selectedPlan,
				email: (this.email) || '',
			};

			await this.paymentService.payment(data);
			complete && complete('success');
			await this.toastService.show('Payment successfully completed');
			await this.modalCtrl.dismiss();
		} catch {
			complete && complete('fail');
			await this.toastService.show('There was a problem processing your payment.');
		}
	}

	async submit() {
		const loading =  await this.loadingCtrl.create({
			message: 'Processing payment...',
		});
		await loading.present();
		const source = await this.paymentService.getSource(this.foo);
		await this.payment(source.id);
		await loading.dismiss();
	}

	async nextStep() {
		this.step++;

		if(this.step === 2) {
			await this.displayCheckout();
		}
	}

	async prevStep() {
		this.step--;
	}
}
