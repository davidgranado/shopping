import { ShoppingListItem, SavedItem, StorePrice } from '../../interfaces';
import { Key } from 'ts-key-enum';
import { NavController, ModalController, AlertController } from '@ionic/angular';
import { Observable, fromEvent, combineLatest, race, BehaviorSubject, of, Subject } from 'rxjs';
import {
	Component,
	OnInit,
	Input,
	EventEmitter,
	Output,
	ViewChild,
	ElementRef,
	OnDestroy,
} from '@angular/core';
import {
	debounceTime,
	map,
	distinctUntilChanged,
	startWith,
	filter,
	shareReplay,
	repeat,
	take,
	takeUntil,
	endWith,
	switchMap,
	tap,
	catchError,
} from 'rxjs/operators';
import { ShoppingListService } from '../../services/shopping-list.service';
import { SavedItemService } from '../../services/saved-item.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { pick, minTrimmedLengthValidator, uuid, defer, getPrice, dedupe } from '../../utils';

function savedItemHasChanged(parentItem: SavedItem, newItem: SavedItem) {
	// TODO clean this nasty
	const currentPrices = parentItem.storePrices || [];
	const newPrices = newItem.storePrices || [];
	return !(
		(parentItem.aisle === newItem.aisle) &&
		(parentItem.brand === newItem.brand) &&
		(parentItem.name === newItem.name) &&
		(parentItem.notes === newItem.notes) &&
		(parentItem.price === newItem.price) &&
		newPrices.every(np =>
			currentPrices.some(cp =>
				cp.storeName.toLowerCase() === np.storeName.toLowerCase() &&
				cp.price === np.price,
			),
		)
	);
}

function combinePrices(parentPrices: StorePrice[], newPrices: StorePrice[]) {
	const pP = parentPrices || [];
	const nP = newPrices || [];

	return dedupe(
			nP.concat(pP),
			price => price.storeName.toLowerCase(),
		)
		.sort(
			(a, b) => (a.storeName.toLowerCase() > b.storeName.toLowerCase()) ? 1 : -1,
		);
}

@Component({
	selector: 'app-shopping-list-item-edit-form',
	styleUrls: ['./shopping-list-item-edit-form.component.scss'],
	template: `
		<ion-grid fixed class="ion-no-padding">
			<ion-row>
				<ion-col class="ion-no-padding">
					<div [formGroup]="itemFG">
						<ion-item class="item-count ion-item-inline-control">
							<ion-button slot="start" (click)="decQuantity()">
								<ion-icon slot="icon-only" name="remove"></ion-icon>
							</ion-button>
							<ion-input
								type="number"
								formControlName="quantity"
								(keyup)="handleKey($event)"
							></ion-input>
							<ion-button slot="end" (click)="incQuantity()">
								<ion-icon slot="icon-only" name="add"></ion-icon>
							</ion-button>
						</ion-item>
						<ion-item>
							<ion-label position="stacked">Name</ion-label>
							<ion-input
								#nameInput
								type="text"
								formControlName="name"
								(keyup)="handleKey($event)"
							></ion-input>
						</ion-item>

						<ng-container *ngIf="!disableAutocomplete">
							<ion-list class="saved-items" *ngIf="showFilter | async">
								<ng-container *ngIf="searchList | async as list">
									<ion-list-header>
										Saved Items ({{(list)?.length}})
									</ion-list-header>
									<ion-item button *ngFor="let item of list" (click)="setItem(item)">
										<ion-label>
											{{item.name}}
										</ion-label>
									</ion-item>
								</ng-container>
							</ion-list>
						</ng-container>

						<ion-item>
							<ion-label position="stacked">Brand</ion-label>
							<ion-input
								type="text"
								formControlName="brand"
								(keyup)="handleKey($event)"
							></ion-input>
						</ion-item>
						<ion-item
							[class.ion-item-inline-control-wide-empty]="!hasStorePrices"
							class="ion-item-inline-control-wide ion-item-inline-control"
						>
							<ion-button
								slot="start"
								*ngIf="hasSelectedStorePrice"
								(click)="removeStorePrice()"
							>
								<ion-icon slot="icon-only" name="remove"></ion-icon>
							</ion-button>
							<ion-label hidden>Store Prices</ion-label>
							<ion-select
								*ngIf="hasStorePrices"
								formControlName="selectedPriceId"
								interface="popover"
								[selectedText]="(price | async)?.storeName"
							>
								<ion-select-option [value]="''">
									{{'$'}}{{item.price}} - General
								</ion-select-option>
								<ion-select-option [value]="price.id" *ngFor="let price of item.storePrices">
									{{'$'}}{{price.price}} - {{price.storeName}}
								</ion-select-option>
							</ion-select>
							<ion-button slot="end" (click)="addStorePrice()">
								<ion-icon slot="icon-only" name="add"></ion-icon>
							</ion-button>
						</ion-item>
						<app-number-input
							*ngIf="price | async as p"
							[precision]="2"
							[amount]="p.price.toString()"
							(amountEntered)="updatePrice($event)"
						></app-number-input>

						<ion-item>
							<ion-label position="stacked">Aisle</ion-label>
							<ion-input
								type="text"
								formControlName="aisle"
								(keyup)="handleKey($event)"
							></ion-input>
						</ion-item>

						<ion-item>
							<ion-label position="stacked">Notes</ion-label>
							<ion-textarea
								auto-grow
								spellcheck
								formControlName="notes"
							></ion-textarea>
						</ion-item>
					</div>

					<ng-container *ngIf="isOwner">
						<div class="parent-item-link">
							<a
								*ngIf="showParentLink | async"
								(click)="openParentItem()"
							>
								Edit Parent Item
							</a>
						</div>

						<ion-button
							*ngIf="canSaveToLibrary | async"
							expand="full"
							color="secondary"
							(click)="saveToLibrary()"
						>
							<div class="app-ellipsis" >
								Save to Library
							</div>
						</ion-button>
					</ng-container>
				</ion-col>
			</ion-row>
		</ion-grid>

		<ion-fab vertical="bottom" horizontal="end" slot="fixed">
			<ion-fab-button
				color="secondary"
				[disabled]="itemFG.invalid || itemFG.pristine"
				(click)="handleSave()"
			>
				<ion-icon name="checkmark"></ion-icon>
			</ion-fab-button>
		</ion-fab>
	`,
})
export class ShoppingListItemEditFormComponent implements OnInit, OnDestroy {
	public _item: BehaviorSubject<ShoppingListItem>;
	public itemFG: FormGroup;
	private parentItem: Observable<SavedItem | null>;
	public searchList: Observable<SavedItem[]> | null = null;
	public showFilter: Observable<boolean> | null = null;
	public showParentLink: Observable<boolean> | null = null;
	public canSaveToLibrary: Observable<boolean> | null = null;
	public price: Observable<StorePrice>;
	private destroy = new Subject();

	@ViewChild('nameInput', {static: true, read: ElementRef}) nameInput: ElementRef<HTMLIonInputElement> | null = null;
	@Output() save = new EventEmitter<ShoppingListItem>();
	@Output() saveAndFinish = new EventEmitter<ShoppingListItem>();
	@Output() close = new EventEmitter<ShoppingListItem>();
	@Input() disableAutocomplete = false;
	@Input() showCreateParentLink = false;
	@Input() isSavedItem = true;
	@Input() isOwner = false;

	get savedItems() {
		return this.savedItemsService.items;
	}

	@Input('item')
	set item(item: ShoppingListItem) {
		if(item) {
			this._item.next({
				...item,
				storePrices: item.storePrices ? [...item.storePrices] : [],
			});
			defer(() => this.setFG(item));
		}
	}

	get item() {
		return this._item.getValue();
	}

	get hasSelectedStorePrice() {
		const control = this.itemFG.get('selectedPriceId');

		if(!control) {
			return false;
		}

		return !!control.value;
	}

	get hasStorePrices() {
		const { storePrices = [] } = this.item;
		return !!storePrices.length;
	}

	updatePrice(price: string) {
		const item = this.item;
		const {
			storePrices = [],
		} = item;
		const {
			selectedPriceId = '',
		} = this.itemFG.value;
		const updatePrice = getPrice(item, selectedPriceId);

		if(updatePrice && updatePrice.id) {
			updatePrice.price = +price;
			this._item.next({
				...item,
				storePrices,
			});
		} else {
			this._item.next({
				...item,
				price: +price,
			});
		}
		this.itemFG.markAsDirty();
	}

	constructor(
		private alertCtrl: AlertController,
		private shoppingListService: ShoppingListService,
		private savedItemsService: SavedItemService,
		private navCtrl: NavController,
		private modalCtrl: ModalController,
	) {
		this._item = new BehaviorSubject(this.shoppingListService.createItem());
		this.itemFG = new FormGroup({
			name: new FormControl('', [
				Validators.required,
				minTrimmedLengthValidator(1),
			]),
			brand: new FormControl('', [
				Validators.maxLength(50),
			]),
			quantity: new FormControl(0, [
				Validators.min(1),
			]),
			aisle: new FormControl(''),
			notes: new FormControl('', [
				Validators.maxLength(1000),
			]),
			selectedPriceId: new FormControl('a'),
		});
		this.price = combineLatest([
			this._item,
			this.itemFG.valueChanges,
		]).pipe(
			map(([
				item,
				formGroup,
			]): StorePrice => {
				const defaultPrice = {
					id: '',
					price: item.price,
					storeName: 'General',
				};

				if(!item.storePrices) {
					return defaultPrice;
				}

				const price = getPrice(item, formGroup.selectedPriceId);

				return price || defaultPrice;
			}),
			takeUntil(this.destroy),
		);
		this.price.subscribe();
		this.parentItem = this._item.pipe(
			switchMap(
				item => (this.isOwner && item.parentId) ?
					this.savedItemsService.getItem(item.parentId).pipe(
						catchError(() => of(null)),
					) :
					of(null),
			),
		);
	}

	ngOnDestroy() {
		this.destroy.complete();
	}

	setItem(item: ShoppingListItem) {
		this._item.next({
			...item,
			parentId: item.id,
		});
		this.setFG({
			...this.itemFG.getRawValue(),
			...item,
		});
	}

	setFG(item: ShoppingListItem) {
		const partial = pick(item, [
			'name',
			'brand',
			'aisle',
			'notes',
			'quantity',
			'selectedPriceId',
		]);
		partial.selectedPriceId = partial.selectedPriceId || '';
		this.itemFG.setValue(partial);
	}

	ngOnInit() {
		if(!this.nameInput) {
			return;
		}

		const focus = fromEvent<any>(this.nameInput.nativeElement, 'ionFocus');
		const blur = fromEvent<any>(this.nameInput.nativeElement, 'ionBlur')
			.pipe(debounceTime(750));
		const disabled = this._item.pipe(
			map(i => !!i.id),
			filter(d => d),
			take(1),
		);
		const isFocused = race(
				focus,
				blur,
			).pipe(
				take(1),
				repeat(),
				map(({type}) => type === 'ionFocus'),
			);
		const term = fromEvent<any>(this.nameInput.nativeElement, 'keyup')
			.pipe(
				map(event => event.target.value as string),
				map(t => t.trim().toLowerCase()),
				debounceTime(300),
				distinctUntilChanged(),
				startWith(''),
			);

		this.searchList = term
			.pipe(
				switchMap(t =>
					(t.length > 2) ?
						this.savedItemsService.searchTerm(t) :
						of([]),
				),
				shareReplay(1),
			);

		this.showFilter = combineLatest([
				isFocused,
				this.searchList,
			]).pipe(
				map(([
					focused,
					list,
				]) => (
					focused &&
					!!list.length
				)),
				takeUntil(disabled),
				endWith(false),
			);
		this.showParentLink = this.parentItem.pipe(
			map(parent => !!parent),
		);

		this.canSaveToLibrary = combineLatest([
			this._item,
			this.parentItem,
		]).pipe(
			map(([item, parent]) => !!(
				!this.isSavedItem &&
				item.id &&
				!parent
			)),
		);

		setTimeout(() =>
			this.nameInput && this.nameInput.nativeElement.setFocus(),
			350,
		);
	}

	async saveToLibrary() {
		const currentItem = {
			...this.item,
			...this.itemFG.getRawValue(),
		};

		const newParentItem = this.savedItemsService.toSavedItemContent(currentItem);

		newParentItem.id = this.savedItemsService.createId();

		this._item.next({
			...this.item,
			parentId: newParentItem.id,
		});

		this.emitSave();

		await this.savedItemsService.save(newParentItem, true);
	}

	async openParentItem() {
		const modal = await this.modalCtrl.getTop();

		if(modal) {
			await modal.dismiss();
		}
		this.navCtrl.navigateForward(`/item/${this.item.parentId}`);
	}

	public incQuantity() {
		const q = this.itemFG.get('quantity');
		this.itemFG.patchValue({
			quantity: q ? q.value + 1 : 0,
		});
	}

	public decQuantity() {
		const q = this.itemFG.get('quantity');
		this.itemFG.patchValue({
			quantity: q ? q.value - 1 : 0,
		});
	}

	public removeStorePrice() {
		const item = this._item.getValue();
		const { storePrices = [] } = item;
		const { selectedPriceId } = this.itemFG.value;

		if(!storePrices.length) {
			return;
		}

		this.itemFG.patchValue({
			selectedPriceId: '',
		});
		this.itemFG.markAsDirty();
		this._item.next({
			...item,
			storePrices: storePrices.filter(p => p.id !== selectedPriceId),
		});
	}

	public async addStorePrice() {
		const alert = await this.alertCtrl.create({
			header: 'Add Store Price',
			inputs: [
				{
					name: 'price',
					type: 'number',
					placeholder: 'Price',
				}, {
					name: 'storeName',
					placeholder: 'Name',
				},
			],
			buttons: [
				{
					text: 'Cancel',
				},
				{
					text: 'Save',
					handler: data => {
						const newPrice: StorePrice = {
							id: uuid(),
							price: data.price,
							storeName: data.storeName.trim(),
						};

						const item = this.item;

						this.itemFG.patchValue({
							selectedPriceId: newPrice.id,
						});
						this.itemFG.markAsDirty();
						const currentPrices = item.storePrices || [];
						const existingItem = currentPrices.find(p =>
							p.storeName.toLowerCase() === newPrice.storeName.toLowerCase(),
						);

						if(existingItem) {
							existingItem.price = newPrice.price;
						} else {
							currentPrices.push(newPrice);
							currentPrices.sort(
								(a, b) => (a.storeName.toLowerCase() > b.storeName.toLowerCase()) ? 1 : -1,
							);
						}

						this._item.next({
							...item,
							storePrices: [
								...currentPrices,
							],
						});
					},
				},
			],
		});
		alert.present();
	}

	public async handleSave(keepOpen?: boolean) {
		if(!this.itemFG.valid) {
			return;
		}

		let finish = true;

		if(keepOpen) {
			finish = false;
		} else {
			finish = !!this.item.id;
		}

		this._item.next({
			...this.item,
			...this.itemFG.getRawValue(),
		});

		this.parentItem.pipe(
			tap(async parentItem => {
				const newSavedItemContent = this.savedItemsService.toSavedItemContent(this.item);

				if(parentItem && savedItemHasChanged(parentItem, newSavedItemContent)) {
					const alert = await this.alertCtrl.create({
						header: 'Update Parent',
						message: 'Would you like to apply your changes to your library?',
						buttons: [
							{
								text: 'No',
								role: 'cancel',
								handler: () => this.emitSave(finish),
							}, {
								text: 'Yes',
								handler: async () => {
									this.savedItemsService.save({
										...parentItem,
										...newSavedItemContent,
										storePrices: [
											...combinePrices(parentItem.storePrices, newSavedItemContent.storePrices),
										],
									});
								},
							},
						],
					});
					await alert.present();
				} else {
					this.emitSave(finish);
				}
			}),
			take(1),
		).subscribe();
	}

	emitSave(finish = false) {

		if(finish) {
			this.saveAndFinish.emit(this.item);
		} else {
			this.save.emit(this.item);
		}
	}

	async handleKey(e: any) {
		if(e.key === Key.Enter) {
			await this.handleSave();

			this.nameInput && this.nameInput.nativeElement.setFocus();
		} else if(e.key === Key.Escape) {
			this.close.emit();
		}

	}

}
