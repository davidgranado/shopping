import { Component } from '@angular/core';

import { ModalController, ActionSheetController } from '@ionic/angular';
import { fromEvent } from 'rxjs';

@Component({
	selector: 'app-root',
	template: `
		<ion-app>
			<app-sw-update></app-sw-update>
			<app-intro></app-intro>
			<ion-router-outlet></ion-router-outlet>
		</ion-app>
	`,
})
export class AppComponent {
	constructor(
		private modalCtrl: ModalController,
		private actionSheetCtrl: ActionSheetController,
	) {
		this.initializeApp();
	}

	initializeApp() {
		// Hack to work around bug:
		// https://github.com/ionic-team/ionic/issues/10168
		fromEvent(window, 'popstate').subscribe(async () => {
			const [
				actionSheet,
				modal,
			] = await Promise.all([
				this.actionSheetCtrl.getTop(),
				this.modalCtrl.getTop(),
			]);

			if(modal) {
				modal.dismiss();
			}

			if(actionSheet) {
				actionSheet.dismiss();
			}
		});
	}
}
