import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { LoggedInWithAccountGuardService } from './services/logged-in-with-account-guard.service';
import { SwUpdateComponent } from './components/sw-update/sw-update.component';
import { IntroComponent } from './components/messages/intro/intro.component';
import { DelayedPreloadStrategy } from './core/delayed-preload.strategy';

const routes: Routes = [
	{
		path: '',
		loadChildren: async () =>
			(await import('./tabs/tabs.module')).TabsPageModule,
		canActivate: [],
	},
	{
		path: 'login',
		loadChildren: async () =>
			(await import('./pages/login/login.module')).LoginPageModule,
		// canActivate: [LoggedInWithAccountGuardService],
	},
	{
		path: 'register',
		loadChildren: async () =>
			(await import('./pages/registration/registration.module')).RegistrationPageModule,
		// canActivate: [LoggedInWithAccountGuardService],
	},
	{
		path: 'user',
		loadChildren: async () =>
			(await import('./pages/user/user.module')).UserPageModule,
	},
	{
		path: 'about',
		loadChildren: async () =>
			(await import('./pages/about/about.module')).AboutPageModule,
	},
	{
		path: 'membership',
		loadChildren: async () =>
			(await import('./pages/membership/membership.module')).MembershipPageModule,
	},
	{
		path: 'shopping-list/',
		loadChildren: async () =>
			(await import('./pages/shopping-list-edit/shopping-list-edit.module')).ShoppingListEditPageModule,
		canActivate: [],
	},
	{
		path: 'shopping-list/:id',
		loadChildren: async () =>
			(await import('./pages/shopping-list-edit/shopping-list-edit.module')).ShoppingListEditPageModule,
		canActivate: [],
	},
	{
		path: 'shopping-list/:id/item/:itemType',
		loadChildren: async () =>
			(await import('./pages/shopping-list-edit/shopping-list-edit.module')).ShoppingListEditPageModule,
		canActivate: [],
	},
	{
		path: 'shopping-list/:id/item/:itemType/:itemId',
		loadChildren: async () =>
			(await import('./pages/shopping-list-edit/shopping-list-edit.module')).ShoppingListEditPageModule,
		canActivate: [],
	},
	{
		path: 'saved-list/',
		loadChildren: async () =>
			(await import('./pages/saved-list-edit/saved-list-edit.module')).SavedListEditPageModule,
		canActivate: [],
	},
	{
		path: 'saved-list/:id',
		loadChildren: async () =>
			(await import('./pages/saved-list-edit/saved-list-edit.module')).SavedListEditPageModule,
		canActivate: [],
	},
	{
		path: 'saved-list/:id/item/:itemType',
		loadChildren: async () =>
			(await import('./pages/saved-list-edit/saved-list-edit.module')).SavedListEditPageModule,
		canActivate: [],
	},
	{
		path: 'saved-list/:id/item/:itemType/:itemId',
		loadChildren: async () =>
			(await import('./pages/saved-list-edit/saved-list-edit.module')).SavedListEditPageModule,
		canActivate: [],
	},
	{
		path: 'item',
		loadChildren: async () =>
			(await import('./pages/saved-item-edit/saved-item-edit.module')).SavedItemEditPageModule,
	},
	{
		path: 'item/:id',
		loadChildren: async () =>
			(await import('./pages/saved-item-edit/saved-item-edit.module')).SavedItemEditPageModule,
	},

	{
		path: 'shopping-lists/edit',
		redirectTo: 'shopping-list/',
	},
	{
		path: 'shopping-lists/edit/:id',
		redirectTo: 'shopping-list/:id',
	},
	{
		path: 'saved-lists/edit',
		redirectTo: 'saved-list/',
	},
	{
		path: 'saved-lists/edit/:id',
		redirectTo: 'saved-list/:id',
	},
	{
		path: ':listType/edit/:listId/item/',
		redirectTo: ':listType/:listId/item/',
	},
	{
		path: ':listType/edit/:listId/item/:id',
		redirectTo: ':listType/:listId/item/:id',
	},
	{
		path: 'index.html',
		redirectTo: '',
		pathMatch: 'full',
	},
];
@NgModule({
	providers: [DelayedPreloadStrategy],
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: DelayedPreloadStrategy }),
	],
	exports: [
		RouterModule,
		SwUpdateComponent,
		IntroComponent,
	],
	declarations: [
		SwUpdateComponent,
		IntroComponent,
	],
})
export class AppRoutingModule {}
