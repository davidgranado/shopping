import { firestore } from 'firebase-admin';
import { object, string } from 'joi';
import { Express } from 'express';
import { UserToken } from '../interfaces/user-token';

const schema = object({
	feedback: string()
		.min(1)
		.max(20000),
});

interface Schema {
	feedback: string;
}

export
function addFeedback(app: Express) {
	app.post('/feedback', async (req, res) => {
		// @ts-ignore
		const user: UserToken | null = req.user;
		let userId = null;

		if(user) {
			userId = user.uid;
		}

		try {
			const result: Schema = await schema.validate(req.body);
			const fs = firestore();
			await fs.collection('/feedback').add({
				feedback: result.feedback,
				userId,
				submitted: new Date(),
			});
			res.status(200).send({
				ok: true,
			});
		} catch (e) {
			console.log('Feedback Submission Error:' , e.details || e.message);
			res.status(400).send(e.details);
		}
	});
}
