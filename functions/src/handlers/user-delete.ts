import { firestore } from 'firebase-admin';
import { auth } from 'firebase-functions';
import * as Stripe from 'stripe';
import { Subscription } from '../interfaces/subscription.interface';

interface User {
	created: firestore.Timestamp;
	email: string;
	username?: string;
	id: string;
}

export
function initDeleteUser(stripe: Stripe) {
	return auth
		.user()
		.onDelete(async (change, context) => {
			const userId = change.uid;

			const fs = firestore();

			const userDoc = await fs.doc(`/users/${userId}`).get();
			const user = userDoc.data() as User;

			if(!user) {
				throw new Error(`Could not find user doc at "/users/${userId}"`);
			}

			const username = user.username;

			if(!username) {
				throw new Error(`Username not found for user "${userId}".`);
			}

			const subscriptionDoc = await fs.doc(`/subscriptions/${userId}`).get();
			const subscription = subscriptionDoc.data() as Subscription;

			if(subscription.subscriptionId) {
				console.log(`Deleting user "${userId} subscription: `, subscription.subscriptionId);
				await stripe.subscriptions.del(subscription.subscriptionId);
			}

			const batch = fs.batch();

			batch.delete(fs.doc(`/users/${userId}`));
			batch.delete(fs.doc(`/usernames/${username.toLowerCase()}`));
			batch.delete(fs.doc(`/subscriptions/${userId}`));

			console.log('Deleting user', userId, username);
			let savedLists;
			let shoppingLists;

			try {
				[
					savedLists,
					shoppingLists,
				] = await Promise.all([
					fs.collection('saved-lists').where('ownerId', '==', userId).get(),
					fs.collection('shopping-lists').where('ownerId', '==', userId).get(),
				]);
			} catch(e) {
				console.log('Username Delete Error: error at step 1', e);
				return;
			}

			const updateList = (listId: string, type: 'shopping-lists' | 'saved-lists') => {
				batch.delete(fs.doc(`${type}/${listId}`));
			};

			shoppingLists.docs.forEach(l => updateList(l.id, 'shopping-lists'));
			savedLists.docs.forEach(l => updateList(l.id, 'saved-lists'));

			const sharedShoppingLists = await fs.collection('shopping-lists').where(`shared.${userId}.username`, '==', username).get();
			sharedShoppingLists.forEach((sharedList) => {
				batch.update(fs.doc(`shopping-lists/${sharedList.id}`), {
					[`shared.${userId}`]: firestore.FieldValue.delete(),
				});
			});

			try {
				console.log(`Initiating user data deletion`, userId, username);
				await batch.commit();
			} catch(e) {
				console.log('Username Delete Error: ', e);
			}
		});
}
