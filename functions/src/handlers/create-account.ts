import { firestore } from 'firebase-admin';
import { auth } from 'firebase-functions';

export
function initCreateAccount() {
	return auth
		.user()
		.onCreate(async (user) => {
			console.log(`Creating user '${user.uid}'.`);

			const fs = firestore();

			let username;

			do {
				// Find unused username
				// tslint:disable-next-line: no-bitwise
				username = `ShopLystr${(10000 * Math.random()) | 0}`;
				const existingUsername = await fs.doc(`usernames/${username}`).get();

				if(existingUsername.exists) {
					username = '';
				}
			} while(!username);

			const batch = fs.batch();

			batch.set(fs.doc(`/users/${user.uid}`), {
				created: new Date(),
				email: user.email || null,
				id: user.uid,
				username,
			});
			batch.set(fs.doc(`/subscriptions/${user.uid}`), {
				userId: user.uid,
				periodEnd: null,
				plan: 'basic',
				stripeCustomerId: null,
				subscriptionId: null,
			});
			batch.set(fs.doc(`usernames/${username.toLowerCase()}`), {
				display: username,
				ownerId: user.uid,
			});

			await batch.commit();
		});
}
