import { firestore } from 'firebase-admin';
import { Express } from 'express';
import { object, string, number } from 'joi';
import { Subscription } from '../interfaces/subscription.interface';
import { Plan } from '../interfaces/plan.interface';

const schema = object({
	type: string().valid('customer.subscription.updated').required(),
	data: object({
		object: object({
			id: string().required(),
			customer: string().required(),
			current_period_end: number().required(),
			plan: object({
				id: string().required(),
			}).unknown(true),
		}).unknown(true),
	}).unknown(true),
}).unknown(true);

interface Foo {
	type: 'customer.subscription.updated';
	data: {
		object: {
			id: string;
			customer: string;
			current_period_end: number;
			cancel_at_period_end: boolean;
			canceled_at: number;
			plan: {
				id: string;
			};
		};
	};
}

export
function addHooks(app: Express) {
	app.post('/hook', async (req, res) => {
		try {
			const fs = firestore();
			const result: Foo = await schema.validate(req.body);

			const c = await fs
				.collection('subscriptions')
				.where('subscriptionId', '==', result.data.object.id)
				.limit(1)
				.get();

			const doc = c.docs.pop();

			if(!doc) {
				console.log(`Stripe Hook Error: Could not find subscription id "${result.data.object.id}"`);
				res.status(400).send({
					ok: false,
				});
				return;
			}

			const sub = doc.data() as Subscription;
			const updatedSubData = result.data.object;

			sub.periodEnd = new Date(updatedSubData.current_period_end  * 1000);
			sub.plan = updatedSubData.plan.id as Plan;

			if(updatedSubData.canceled_at) {
				sub.cancellationDate = new Date(updatedSubData.canceled_at * 1000);
			}

			await fs.doc(`/subscriptions/${doc.id}`).set(sub);

			res.status(200).send({
				ok: true,
			});
		} catch (e) {
			console.log('Stripe Hook Error:' , e.details || e.message);
			res.status(400).send(e.details);
		}
	});
}
