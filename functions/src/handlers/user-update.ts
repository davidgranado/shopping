import { firestore as fa } from 'firebase-admin';
import { firestore as ff } from 'firebase-functions';

interface Username {
	display: string;
	ownerId: string;
}

interface User {
	created: fa.Timestamp;
	email: string;
	username?: string;
	id: string;
}

// interface List {
// 	ownerId: string;
// 	ownerUsername: string;
// }

export
function initUpdateUser() {
	return ff
		.document('users/{userId}')
		.onUpdate(async (change, context) => {
			const userId = context.params.userId;
			const prev = change.before.data() as User;
			const after = change.after.data() as User;
			const afterUsername = after.username || '';
			const prevUsername = prev.username || '';

			if(!(prev && after)) {
				return;
			}

			if(prev.username !== after.username) {
				const f = fa();

				try {
					const existingUsername = await f.doc(`usernames/${afterUsername.toLowerCase()}`).get();
					const existingUsernameData = existingUsername.data() as Username;

					if(existingUsername.exists) {
						if(existingUsernameData.ownerId !== after.id) {
							await f.doc(`users/${after.id}`).update({
								username: prev.username,
							});
							console.log(`Username Update Error: Username "${after.username}" exists. Reverted to "${prev.username}"`);
						}

						return;
					}
				} catch(e) {
					console.log('Username Update Error: Username Check Error', e);
					return;
				}

				console.log('Updating usernames', prev, after);
				let savedLists;
				let shoppingLists;

				const batch = f.batch();

				try {
					[
						savedLists,
						shoppingLists,
					] = await Promise.all([
						f.collection('saved-lists').where('ownerId', '==', after.id).get(),
						f.collection('shopping-lists').where('ownerId', '==', after.id).get(),
					]);
				} catch(e) {
					console.log('Username Update Error: error at step 1', e);
					return;
				}

				batch.set(f.doc(`usernames/${afterUsername.toLowerCase()}`), {
					display: after.username,
					ownerId: after.id,
				});

				const updateList = (listId: string, type: 'shopping-lists' | 'saved-lists') => {
					batch.update(f.doc(`${type}/${listId}`), {
						ownerUsername: after.username,
					});
				};

				shoppingLists.docs.forEach(l => updateList(l.id, 'shopping-lists'));
				savedLists.docs.forEach(l => updateList(l.id, 'saved-lists'));

				if(prevUsername) {
					batch.delete(f.doc(`usernames/${prevUsername.toLowerCase()}`));

					const sharedShoppingLists = await f.collection('shopping-lists').where(`shared.${userId}.username`, '==', prevUsername).get();
					sharedShoppingLists.forEach((sharedList) => {
						batch.update(f.doc(`shopping-lists/${sharedList.id}`), {
							[`shared.${userId}`]: {
								username: after.username,
							},
						});
					});
				}

				try {
					console.log(`Writing batch change: user ${after.id} username "${prev.username}" => ${after.username}`);
					await batch.commit();
				} catch(e) {
					console.log('Username Update Error: Error ', e);
				}
			}
		});
}
