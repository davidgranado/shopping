import { firestore } from 'firebase-admin';
import { object, only, string } from 'joi';
import * as Stripe from 'stripe';
import { Express } from 'express';
import { UserToken } from '../interfaces/user-token';
import { Plan } from '../interfaces/plan.interface';
import { Subscription } from '../interfaces/subscription.interface';

interface PaymentPostBody {
	email?: string;
	plan: Plan;
	source: string;
}

const PLANS = [
	'basic',
	`pro-monthly`,
	`pro-annual`,
];

const paymentPostSchema = object({
	email: string()
		.trim()
		.email(),
	plan: only(PLANS)
		.required(),
	source: string()
		.required(),
});

export
function addMembershipPosts(app: Express, stripe: Stripe) {
	app.post('/membership/unsubscribe', async (req, res) => {
		const fs = firestore();
		// @ts-ignore
		const user: UserToken = req.user;

		const sub = (await fs.doc(`/subscriptions/${user.uid}`).get()).data() as Subscription;

		if(sub && sub.subscriptionId) {
			await stripe.subscriptions.update(sub.subscriptionId, {
				cancel_at_period_end: true,
			});

			sub.cancellationDate = new Date();

			await fs.doc(`/subscriptions/${user.uid}`).set(sub);
		}

		res.status(200).send({
			ok: true,
		});
	});

	app.post('/membership/pro', async (req, res) => {
		const fs = firestore();
		console.log('pro registration...');
		// @ts-ignore
		const user: UserToken = req.user;
		let result: PaymentPostBody;

		try {
			console.log('Validating Schema');
			result = await paymentPostSchema.validate({
				email: user.email,
				plan: req.body.plan,
				source: req.body.source,
			});
		} catch({details}) {
			console.log('Signup error:' , details);
			res.status(400).send(details);
			return;
		}

		const {
			email,
			plan,
			source,
		} = result;

		try {
			const sub = (await fs.doc(`/subscriptions/${user.uid}`).get()).data() as Subscription;
			let customerId: string;

			if(sub && sub.stripeCustomerId) {
				customerId = sub.stripeCustomerId;
			} else {
				const createUserParams = {
					email,
					source,
				};

				console.log('Registering user with Stripe:', createUserParams);
				const newCustomer = await stripe.customers.create(createUserParams);

				customerId = newCustomer.id;
			}

			const createSubParams = {
				customer: customerId,
				items: [{
					plan,
				}],
			};

			console.log('Creating Stripe subscription:', createSubParams);
			const newSub = await stripe.subscriptions.create(createSubParams);

			console.log('Saving new subscription', newSub);
			await fs.doc(`/subscriptions/${user.uid}`)
				.set({
					plan,
					stripeCustomerId: customerId,
					subscriptionId: newSub.id,
					periodEnd: new Date(newSub.current_period_end * 1000),
				}, {
					merge: true,
				});

			res.status(200).send({
				ok: true,
			});
		} catch(e) {
			console.log(e);
			res.sendStatus(500);
		}
	});
}
