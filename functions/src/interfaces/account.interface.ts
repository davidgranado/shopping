export
interface Account {
	email: string;
	id: string;
	created: Date;
}
