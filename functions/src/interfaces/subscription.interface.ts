import { Plan } from './plan.interface';

export
interface Subscription {
	plan: Plan;
	stripeCustomerId: string;
	subscriptionId: string;
	periodEnd: Date;
	cancellationDate?: Date;
}
