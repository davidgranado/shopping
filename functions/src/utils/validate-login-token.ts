import { RequestHandler } from 'express';
import { auth } from 'firebase-admin';

interface RouteMap {
	[key: string]: boolean;
}

// source:
// https://github.com/firebase/functions-samples/blob/7d83e9ae2107a3345fec234f758c8d115353cb2a/authorized-https-endpoint/functions/index.js
export
function tokenValidationGenerator(routeMap: RouteMap): RequestHandler {
	return async (req, res, next) => {
		// false = parse token, but don't require
		// true = require token
		// undefined = ignore token
		const requireAuth = routeMap[req.path];

		if(typeof requireAuth === undefined) {
			return next();
		}

		console.log('Check if request is authorized with Firebase ID token');

		if (
			requireAuth &&
			(!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
			!(req.cookies && req.cookies.__session)) {
			console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
				'Make sure you authorize your request by providing the following HTTP header:',
				'Authorization: Bearer <Firebase ID Token>',
				'or by passing a "__session" cookie.');
			res.status(403).send('Unauthorized');
			return;
		}

		let idToken;

		if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
			console.log('Found "Authorization" header');
			// Read the ID Token from the Authorization header.
			idToken = req.headers.authorization.split('Bearer ')[1];
		} else if(req.cookies) {
			console.log('Found "__session" cookie');
			// Read the ID Token from cookie.
			idToken = req.cookies.__session;
		} else if(requireAuth) {
			// No cookie
			res.status(403).send('Unauthorized');
			return;
		}

		try {
			const decodedIdToken = await auth().verifyIdToken(idToken);

			console.log('ID Token correctly decoded', decodedIdToken);
			// @ts-ignore
			req.user = decodedIdToken;
			return next();
		} catch(error) {
			console.error('Error while verifying Firebase ID token:', error);
			if(requireAuth) {
				res.status(403).send('Unauthorized');
			} else {
				return next();
			}
		}
	};
}
