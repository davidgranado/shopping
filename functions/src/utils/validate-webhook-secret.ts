import { RequestHandler } from 'express';
import * as Stripe from 'stripe';

interface RouteMap {
	[key: string]: boolean;
}

export
function webhookValidationGenerator(endpointSecret: string, stripe: Stripe, routeMap: RouteMap): RequestHandler {
	return async (req, res, next) => {

		if(!routeMap[req.path]) {
			return next();
		}

		const sig = req.headers['stripe-signature'];

		if (sig) {

			try {
				stripe.webhooks.constructEvent((req as any).rawBody, sig, endpointSecret);
				return next();
			} catch (err) {
				console.error(`Invalid Webhook Signature: ${sig}`, err);
			}
		}

		console.error('Failed Webhook Authorization');
		res.status(200);
		return;
	};
}
