interface StripeBaseProps {
	publishable: string;
	secret: string;
	webhook_secret: string;
}

interface FirebaseConfigProps {
	stripe: StripeBaseProps;
}

export
interface FirebaseConfig {
	isDev: boolean;
	prod: FirebaseConfigProps;
	dev: FirebaseConfigProps;
}
