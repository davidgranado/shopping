import { initializeApp } from 'firebase-admin';
import { config as functionsConfig, https } from 'firebase-functions';
import * as Stripe from 'stripe';
import { FirebaseConfig } from './firebase-config.model';
import * as express from 'express';
import * as cors from 'cors';
import * as cookieParser from 'cookie-parser';
import { tokenValidationGenerator } from './utils/validate-login-token';
import { webhookValidationGenerator } from './utils/validate-webhook-secret';
import { addMembershipPosts } from './handlers/membership-post';
import { initCreateAccount } from './handlers/create-account';
import { addHooks } from './handlers/hooks';
import { initUpdateUser } from './handlers/user-update';
import { initDeleteUser } from './handlers/user-delete';
import { addFeedback } from './handlers/feedback';

initializeApp();
const allConfig = functionsConfig() as FirebaseConfig;
const config = allConfig.isDev ? allConfig.dev : allConfig.prod;
const stripe = new Stripe(config.stripe.secret);
const app = express();

// only use the raw bodyParser for webhooks
app.use((req, res, next) => {
	if (req.originalUrl === '/hook' || req.originalUrl === '/hook/') {
		next();
	} else {
		express.json()(req, res, next);
	}
});
app.use(express.raw());
app.use(cors({origin: true}));
app.use(cookieParser());
app.use(tokenValidationGenerator({
	'/membership/pro': true,
	'/membership/pro/': true,
	'/membership/unsubscribe': true,
	'/membership/unsubscribe/': true,
	'/user': true,
	'/user/': true,
	'/feedback': false,
}));
app.use(webhookValidationGenerator(config.stripe.webhook_secret, stripe, {
	'/hook': true,
	'/hook/': true,
}));

addMembershipPosts(app, stripe);
addHooks(app);
addFeedback(app);

export
const api = https.onRequest(app);

export
const createAccount = initCreateAccount();

export
const updateUser = initUpdateUser();

export
const deleteUser = initDeleteUser(stripe);

// export
// const handleEmailUpdate = firestore
// 	.document('users/{userId}')
// 	.onUpdate(async (change, context) => {
// 		const prevValue = change.before.data() as Account;
// 		const newValue = change.after.data() as Account;

// 		if(prevValue.email === newValue.email) {
// 			return;
// 		}

// 		console.log(`Email updated from ${prevValue.email} to ${newValue.email} for user ${context.params.userId}`);

// 		firestoreAdmin().doc(`/users/${context.params.userId}`).set({
// 			created: new Date(),
// 			email: newValue.email || null,
// 			id: user.uid,
// 		}),
// 	});

// 		const prevValue = change.before.data() as Account;
// 		const newValue = change.after.data() as Account;

// 		console.log(`Registering stripe customer id '${customer.id}' for user '${prevValue.id}'.`);

// 		if(!prevValue.email) {
// 			const customer = await stripe.customers.create({
// 				email: newValue.email,
// 			});

// 			console.log(`Registering stripe customer id '${prevValue.id}' for user '${prevValue.id}'.`);

// 			await admin.firestore().doc(`/users/${prevValue.id}`).update({
// 				stripeCustomerId: customer.id,
// 			} as Partial<Account>,);
// 		}
// 	});
